<%-- 
    Document   : newjsp
    Created on : Jul 24, 2022, 10:47:38 AM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Ezuni - Online Education System</title>
        <meta charset="UTF-8">
        <meta name="description" content="WebUni Education Template">
        <meta name="keywords" content="webuni, education, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="${path}/asset/img/logo/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="${path}/asset/img/logo/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="${path}/asset/img/logo/favicon-16x16.png" />
        <link rel="manifest" href="${path}/asset/img/logo/site.webmanifest" />

        <!-- Google Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300" rel="stylesheet" type="text/css">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="${path}/assetHomeP/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/font-awesome.min.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/owl.carousel.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/style.css" />

        <style>
            .body {
                font-family: "Open Sans", sans-serif;
            }
            .courseDescription {
                display: block;
                width: 100%;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }
            .subjectDescription {
                display: block;
                width: 100%;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }
            .notify-badge{
                position: absolute;
                right:-20px;
                top:10px;
                background:red;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:white;
                padding:5px 10px;
                font-size:20px;
                margin-right: 40px;
                margin-top: 5px;
            }
        </style>
    </head>

    <body>

        <!--Header-->
        <!--Page Preloder--> 
        <div id="preloder">
            <div class="loader"></div>
        </div> 

        <!-- Header section -->
        <div>
            <header class="header-section fixed-top" style="position: absolute;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <a href="${path}/home2" class="site-logo">
                                <img src="${path}/asset/img/logo/ezuni_5.png" alt="">
                            </a>
                            <div class="nav-switch">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-2">
                            <!--<a href="" class="site-btn header-btn">Login</a>-->

                            <nav class="main-menu">
                                <ul>
                                    <li><a href="${path}/home2">Home</a></li>
                                    <li><a href="${path}/home2">About us</a></li>
                                    <li><a href="${path}/subject">Courses</a></li>
                                    <li><a href="BlogList">Blog</a></li>
                                    <li><a href="${path}/home2">Contact</a></li>
                                        <c:if test="${user != null}">
                                        <li><a href="${path}/myregister">My Course</a></li>
                                        <li><a href="${path}/practicelist">My Practice</a></li>
                                        <li><a href="${path}/EditProfileController">Profile</a></li>
                                        <li><a href="${path}/logout">Logout</a></li>

                                    </c:if>
                                    <c:if test="${user == null}">
                                        <a href="${path}/accountcontroller">
                                            <button class=" site-btn header-btn btn btn-primary">Login</button>
                                        </a>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- Header section end -->
        <!--Header end-->

        <!-- Page info -->
        <div class="page-info-section set-bg" data-setbg="${path}/assetHomeP/img/page-bg/3.jpg">
            <div class="container">
                <div class="site-breadcrumb">
                    <a href="#">Home</a>
                    <span>All course</span>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <!-- search section -->
        <section class="search-section ss-other-page">
            <div class="container">
                <div class="search-warp">
                    <div class="section-title text-white">
                        <h2><span>Search your course</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <!-- search form -->
                            <form class="course-search-form">
                                <input type="text" placeholder="Course">
                                <input type="text" class="last-m" placeholder="Category">
                                <button class="site-btn btn-dark">Search Couse</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- search section end -->

        <!-- course section -->
        <section class="course-section">
            <div class="course-warp">
                <ul class="course-filter controls">
                    <li class="control active" data-filter="all">All</li>
                        <c:forEach items="${cats}" var="c">
                        <li class="control" data-filter=".${fn:replace(c.name, ' ', '')}">${c.name}</li>
                        </c:forEach>
                </ul>
                <div class="row course-items-area">
                    <c:set value="0" var="numSub"></c:set>
                    <c:forEach items="${cats}" var="c">
                        <c:forEach items="${c.types}" var="t">
                            <c:forEach items="${t.subjects}" var="s" varStatus="countSub">
                                <c:set value="${numSub+1}" var="numSub" ></c:set>
                                <c:if test="${numSub < 8}">
                                    <!-- course -->
                                    <a href="${path}/home2" class="mix col-lg-3 col-md-4 col-sm-6 ${fn:replace(c.name, ' ', '')}">
                                        <div class="course-item">
                                            <div class="course-thumb set-bg" data-setbg="${s.image}">
                                                <c:if test="${s.discount eq 0}" >
                                                    <div class="price">$${s.price}</div>
                                                </c:if>
                                                <c:if test="${s.discount ne 0}" >
                                                    <c:set value="${(s.price - (s.price * s.discount) /100)}" var="afterPrice" ></c:set>
                                                    <span class="notify-badge">Sale ${s.discount}%</span>
                                                    <div class="price"> <s style="color: #23527c">$${s.price}</s> <span>$<fmt:formatNumber value="${afterPrice}" maxFractionDigits="0"/></span></div>
                                                        </c:if>
                                            </div>
                                            <div class="course-info">
                                                <div class="course-text">
                                                    <h5 class="subjectDescription"> ${countSub.count}: ${s.name}</h5>
                                                    <p class="subjectDescription">${s.description}</p>
                                                    <div class="students">${s.num_regis_sub} Students</div>
                                                </div>
                                                <div class="course-author">
                                                    <div class="ca-pic set-bg" data-setbg="${s.instructor_avatar}"></div>
                                                    <p>${s.instructor_first_name} ${s.instructor_last_name}, <span style="text-transform: capitalize;">${s.instructor_role}</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </c:if>
                                <c:if test="${numSub > 8}">
                                    <!-- course -->
                                    <a style="display: none" href="${path}/home2" class="mix col-lg-3 col-md-4 col-sm-6 ${fn:replace(c.name, ' ', '')}">
                                        <div class="course-item">
                                            <div class="course-thumb set-bg" data-setbg="${s.image}">
                                                <c:if test="${s.discount eq 0}" >
                                                    <div class="price">$${s.price}</div>
                                                </c:if>
                                                <c:if test="${s.discount ne 0}" >
                                                    <c:set value="${(s.price - (s.price * s.discount) /100)}" var="afterPrice" ></c:set>
                                                    <span class="notify-badge">Sale ${s.discount}%</span>
                                                    <div class="price"> <s style="color: #23527c">$${s.price}</s> <span>$<fmt:formatNumber value="${afterPrice}" maxFractionDigits="0"/></span></div>
                                                        </c:if>
                                            </div>
                                            <div class="course-info">
                                                <div class="course-text">
                                                    <h5 class="subjectDescription"> ${countSub.count}: ${s.name}</h5>
                                                    <p class="subjectDescription">${s.description}</p>
                                                    <div class="students">${s.num_regis_sub} Students</div>
                                                </div>
                                                <div class="course-author">
                                                    <div class="ca-pic set-bg" data-setbg="${s.instructor_avatar}"></div>
                                                    <p>${s.instructor_first_name} ${s.instructor_last_name}, <span style="text-transform: capitalize;">${s.instructor_role}</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </c:forEach>
                </div>
                <h3>Most registrations course</h3>
                <!--course list-->
                <div class="featured-courses">
                    <c:forEach items="${regisSubs}" var="rs" varStatus="status" begin="0" end="0">
                        <%--<c:if test="${status.count % 2 eq 0}">--%>
                        <div class="featured-course course-item">
                            <div class="course-thumb set-bg" data-setbg="${rs.image}">
                                <c:if test="${rs.discount eq 0}" >
                                    <div class="price">$${rs.price}</div>
                                </c:if>
                                <c:if test="${rs.discount ne 0}" >
                                    <c:set value="${(rs.price - (rs.price * rs.discount) /100)}" var="afterPrice" ></c:set>
                                    <span class="notify-badge">Sale ${rs.discount}%</span>
                                    <div class="price"> <s style="color: #23527c">$${rs.price}</s> <span>$<fmt:formatNumber value="${afterPrice}" maxFractionDigits="0"/></span></div>
                                        </c:if>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 offset-lg-6 pl-0">
                                    <div class="course-info">
                                        <div class="course-text">
                                            <div class="fet-note">${rs.cat_name}</div>
                                            <h5>${rs.name}</h5>
                                            <p>${rs.description}</p>
                                            <div class="students">${rs.num_regis_sub} Students</div>
                                        </div>
                                        <div class="course-author">
                                            <div class="ca-pic set-bg" data-setbg="${rs.instructor_avatar}"></div>
                                            <p>${rs.instructor_first_name} ${rs.instructor_last_name}, <span style="text-transform: capitalize;"> ${rs.instructor_role}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--</c:if>--%>
                    </c:forEach>

                </div>
                <!--end course list-->
            </div>
        </section>
        <!-- course section end -->

        <!-- banner section -->
        <section class="banner-section">
            <div class="container">
                <div class="section-title mb-0 pb-2">
                    <h2>Join Our Community Now!</h2>
                    <p></p>
                </div>
                <div class="text-center">
                    <a href="#" class="site-btn">Register Now</a>
                </div>
            </div>
        </section>
        <!-- banner section end -->

        <!-- banner section -->
        <section class="banner-section">
            <div class="container">
                <div class="section-title mb-0 pb-2">
                    <h2>View more our blog</h2>
                    <p></p>
                </div>
                <div class="text-center" style="padding-bottom: 10px">
                    <a href="${path}/blog" class="site-btn">Blog</a>
                </div>
            </div>
        </section>
        <!-- banner section end -->

        <!-- footer section -->
        <!-- footer section -->
        <footer class="footer-section spad pb-0">
            <div class="footer-top">
                <div class="footer-warp">
                    <div class="row">
                        <div class="widget-item">
                            <h4>Contact Info</h4>
                            <ul class="contact-list">
                                <li>1481 Creekside Lane <br>Avila Beach, CA 931</li>
                                <li>+53 345 7953 32453</li>
                                <li>yourmail@gmail.com</li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Engeneering</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Graphic Design</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Development</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Newsletter</h4>
                            <form class="footer-newslatter">
                                <input type="email" placeholder="E-mail">
                                <button class="site-btn">Subscribe</button>
                                <p>*We don’t spam</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-warp">
                    <ul class="footer-menu">
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Register</a></li>
                        <li><a href="#">Privacy</a></li>
                    </ul>
                    <div class="copyright">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer section end -->
        <!-- footer section end -->

        <!--====== Javascripts & Jquery ======-->
        <script src="${path}/assetHomeP/js/jquery-3.2.1.min.js"></script>
        <script src="${path}/assetHomeP/js/bootstrap.min.js"></script>
        <script src="${path}/assetHomeP/js/mixitup.min.js"></script>
        <script src="${path}/assetHomeP/js/circle-progress.min.js"></script>
        <script src="${path}/assetHomeP/js/owl.carousel.min.js"></script>
        <script src="${path}/assetHomeP/js/main.js"></script>

    </body>

</html>
