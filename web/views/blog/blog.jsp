<%-- 
    Document   : newjsp
    Created on : Jul 24, 2022, 10:47:38 AM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Ezuni - Online Education System</title>
        <meta charset="UTF-8">
        <meta name="description" content="WebUni Education Template">
        <meta name="keywords" content="webuni, education, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="${path}/asset/img/logo/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="${path}/asset/img/logo/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="${path}/asset/img/logo/favicon-16x16.png" />
        <link rel="manifest" href="${path}/asset/img/logo/site.webmanifest" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="${path}/assetHomeP/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/font-awesome.min.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/owl.carousel.css" />
        <link rel="stylesheet" href="${path}/assetHomeP/css/style.css" />
    </head>

    <body>
        <!--Header-->
        <!--Page Preloder--> 
        <div id="preloder">
            <div class="loader"></div>
        </div> 

        <!-- Header section -->
        <div>
            <header class="header-section fixed-top" style="position: absolute;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <a href="${path}/home2" class="site-logo">
                                <img src="${path}/asset/img/logo/ezuni_5.png" alt="">
                            </a>
                            <div class="nav-switch">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-2">
                            <!--<a href="" class="site-btn header-btn">Login</a>-->

                            <nav class="main-menu">
                                <ul>
                                    <li><a href="${path}/home2">Home</a></li>
                                    <li><a href="${path}/home2">About us</a></li>
                                    <li><a href="${path}/subject">Courses</a></li>
                                    <li><a href="BlogList">Blog</a></li>
                                    <li><a href="${path}/home2">Contact</a></li>
                                        <c:if test="${user != null}">
                                        <li><a href="${path}/myregister">My Course</a></li>
                                        <li><a href="${path}/practicelist">My Practice</a></li>
                                        <li><a href="${path}/EditProfileController">Profile</a></li>
                                        <li><a href="${path}/logout">Logout</a></li>

                                    </c:if>
                                    <c:if test="${user == null}">
                                        <a href="${path}/accountcontroller">
                                            <button class=" site-btn header-btn btn btn-primary">Login</button>
                                        </a>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- Header section end -->
        <!--Header end-->

        <!-- Page info -->
        <div class="page-info-section set-bg" data-setbg="${path}/assetHomeP/img/page-bg/3.jpg">
            <div class="container">
                <div class="site-breadcrumb">
                    <a href="#">Home</a>
                    <span>Blog</span>
                </div>
            </div>
        </div>
        <!-- Page info end -->

        <!-- search section -->
        <section class="search-section ss-other-page">
            <div class="container">
                <div class="search-warp">
                    <div class="section-title text-white">
                        <h2><span>Search your course</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <!-- search form -->
                            <form class="course-search-form">
                                <input type="text" placeholder="Course">
                                <input type="text" class="last-m" placeholder="Category">
                                <button class="site-btn btn-dark">Search Couse</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- search section end -->
        <!-- Page  -->
        <section class="blog-page spad pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <!-- blog post -->
                        <div class="blog-post">
                            <img src="${path}/assetHomeP/img/blog/1.jpg" alt="">
                            <h3>How to create the perfect resume</h3>
                            <div class="blog-metas">
                                <div class="blog-meta author">
                                    <div class="post-author set-bg" data-setbg="${path}/assetHomeP/img/authors/1.jpg"></div>
                                    <a href="#">James Smith</a>
                                </div>
                                <div class="blog-meta">
                                    <a href="#">Development</a>
                                </div>
                                <div class="blog-meta">
                                    <a href="#">June 12, 2018</a>
                                </div>
                                <div class="blog-meta">
                                    <a href="#">2 Comments</a>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur. Phasellus sollicitudin et nunc eu efficitur. Sed ligula nulla, molestie quis ligula in, eleifend rhoncus ipsum. Donec ultrices, sem vel efficitur molestie, massa nisl posuere ipsum, ut vulputate mauris ligula a metus. Aenean vel congue diam, sed bibendum ipsum. Nunc vulputate aliquet tristique. Integer et pellentesque urna. </p>
                            <a href="#" class="site-btn readmore">Read More</a>
                        </div>
                        <!-- end blog -->
                        <c:forEach items="blogs" var="b" >
                            <!-- blog post -->
                            <div class="blog-post">
                                <img src="${path}/assetHomeP/img/blog/1.jpg" alt="">
                                <h3>How to create the perfect resume</h3>
                                <div class="blog-metas">
                                    <div class="blog-meta author">
                                        <div class="post-author set-bg" data-setbg="${path}/assetHomeP/img/authors/1.jpg"></div>
                                        <a href="#">James Smith</a>
                                    </div>
                                    <div class="blog-meta">
                                        <a href="#">Development</a>
                                    </div>
                                    <div class="blog-meta">
                                        <a href="#">June 12, 2018</a>
                                    </div>
                                    <div class="blog-meta">
                                        <a href="#">2 Comments</a>
                                    </div>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur. Phasellus sollicitudin et nunc eu efficitur. Sed ligula nulla, molestie quis ligula in, eleifend rhoncus ipsum. Donec ultrices, sem vel efficitur molestie, massa nisl posuere ipsum, ut vulputate mauris ligula a metus. Aenean vel congue diam, sed bibendum ipsum. Nunc vulputate aliquet tristique. Integer et pellentesque urna. </p>
                                <a href="#" class="site-btn readmore">Read More</a>
                            </div>
                            <!-- end blog -->
                        </c:forEach>

                        <div class="site-pagination">
                            <span class="active">01.</span>
                            <a href="#">02.</a>
                            <a href="#">03</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-9 sidebar">
                        <div class="sb-widget-item">
                            <form class="search-widget">
                                <input type="text" placeholder="Search">
                                <button><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <div class="sb-widget-item">
                            <h4 class="sb-w-title">Categories</h4>
                            <ul>
                                <li><a href="#">Developement</a></li>
                                <li><a href="#">Social Media</a></li>
                                <li><a href="#">Press</a></li>
                                <li><a href="#">Events & Lifestyle</a></li>
                                <li><a href="#">Uncategorizes</a></li>
                            </ul>
                        </div>
                        <div class="sb-widget-item">
                            <h4 class="sb-w-title">Archives</h4>
                            <ul>
                                <li><a href="#">February 2018</a></li>
                                <li><a href="#">March 2018</a></li>
                                <li><a href="#">April 2018</a></li>
                                <li><a href="#">May 2018</a></li>
                                <li><a href="#">June 2018</a></li>
                            </ul>
                        </div>
                        <div class="sb-widget-item">
                            <h4 class="sb-w-title">Archives</h4>
                            <div class="tags">
                                <a href="#">education</a>
                                <a href="#">courses</a>
                                <a href="#">development</a>
                                <a href="#">design</a>
                                <a href="#">on line courses</a>
                                <a href="#">wp</a>
                                <a href="#">html5</a>
                                <a href="#">music</a>
                            </div>
                        </div>
                        <div class="sb-widget-item">
                            <div class="add">
                                <a href="#"><img src="${path}/assetHomeP/img/add.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Page end -->
        <!-- banner section -->
        <section class="banner-section spad">
            <div class="container">
                <div class="section-title mb-0 pb-2">
                    <h2>Join Our Community Now!</h2>
                    <p></p>
                </div>
                <div class="text-center pt-5">
                    <a href="#" class="site-btn">Register Now</a>
                </div>
            </div>
        </section>
        <!-- banner section end -->

        <!-- footer section -->
        <footer class="footer-section spad pb-0">
            <div class="footer-top">
                <div class="footer-warp">
                    <div class="row">
                        <div class="widget-item">
                            <h4>Contact Info</h4>
                            <ul class="contact-list">
                                <li>1481 Creekside Lane <br>Avila Beach, CA 931</li>
                                <li>+53 345 7953 32453</li>
                                <li>yourmail@gmail.com</li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Engeneering</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Graphic Design</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Development</h4>
                            <ul>
                                <li><a href="">Applied Studies</a></li>
                                <li><a href="">Computer Engeneering</a></li>
                                <li><a href="">Software Engeneering</a></li>
                                <li><a href="">Informational Engeneering</a></li>
                                <li><a href="">System Engeneering</a></li>
                            </ul>
                        </div>
                        <div class="widget-item">
                            <h4>Newsletter</h4>
                            <form class="footer-newslatter">
                                <input type="email" placeholder="E-mail">
                                <button class="site-btn">Subscribe</button>
                                <p>*We don’t spam</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-warp">
                    <ul class="footer-menu">
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Register</a></li>
                        <li><a href="#">Privacy</a></li>
                    </ul>
                    <div class="copyright">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer section end -->

        <!--====== Javascripts & Jquery ======-->
        <script src="${path}/assetHomeP/js/jquery-3.2.1.min.js"></script>
        <script src="${path}/assetHomeP/js/bootstrap.min.js"></script>
        <script src="${path}/assetHomeP/js/mixitup.min.js"></script>
        <script src="${path}/assetHomeP/js/circle-progress.min.js"></script>
        <script src="${path}/assetHomeP/js/owl.carousel.min.js"></script>
        <script src="${path}/assetHomeP/js/main.js"></script>

    </body>

</html>
