<%-- 
    Document   : slider
    Created on : Jun 27, 2022, 2:00:51 PM
    Author     : Phamb
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <meta content="" name="description">
        <meta content="" name="keywords">        <!-- Favicons -->
        <link href="${path}/assetSlider/img/favicon.png" rel="icon">
        <link href="assetSlider/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="${path}/assetSlider/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/aos/aos.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="${path}/assetSlider/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="${path}/assetSlider/css/style.css" rel="stylesheet">
    </head>
    <body>
        <!-- ======= Hero Section ======= -->
        <section id="hero">
            <div class="hero-container">
                <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">
                    <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active" style="background-image: url('${path}/assetSlider/img/slide/slide-3.jpg');">
                            <div class="carousel-container">
                                <div class="carousel-content container">
                                    <h2 class="animate__animated animate__fadeInDown">Welcome to <span>Ezuni</span></h2>
                                    <p class="animate__animated animate__fadeInUp">Online learning platform with thousands of courses and exercises</p>
                                    <a href="home" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More About Us</a>
                                </div>
                            </div>
                        </div>
                        <c:forEach var="s" items="${slides}">
                            <div class="carousel-item " style="background-image: url('${s.sl_image}');">
                                <div class="carousel-container">
                                    <div class="carousel-content container">
                                        <h2 class="animate__animated animate__fadeInDown">${s.sl_heading}<span> Ezuni</span></h2>
                                        <p class="animate__animated animate__fadeInUp">${s.sl_content}</p>
                                        <c:if test="${s.sl_show eq 1}">
                                            <fmt:parseDate value="${s.sl_begin}" pattern="yyyy-MM-dd HH:mm" var="beginBP" type="both" />
                                            <fmt:formatDate pattern="dd MMM, yyyy" value="${beginBP}" var="begin" />
                                            <fmt:parseDate value="${s.sl_end}" pattern="yyyy-MM-dd HH:mm" var="endBP" type="both" />
                                            <fmt:formatDate pattern="dd MMM, yyyy" value="${endBP}" var="end" />
                                            <h2 class="animate__animated animate__fadeInDown" style="font-size: 150%">
                                                <p>Sale from 
                                                    <strong style="font-size: 200%"> ${begin} </strong>
                                                     to 
                                                    <strong style="font-size: 200%"> ${end} </strong>
                                                </p>
                                            </h2>
                                        </c:if>
                                        <a href="${s.sl_link}" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                    </a>
                    <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                        <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </section>
        <!-- End Hero -->
        <!-- Vendor JS Files -->
        <script src="${path}/assetSlider/vendor/purecounter/purecounter.js"></script>
        <script src="${path}/assetSlider/vendor/aos/aos.js"></script>
        <script src="${path}/assetSlider/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="${path}/assetSlider/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="${path}/assetSlider/vendor/isotope-layout/isotope.pkgd.min.js"></script>
        <script src="${path}/assetSlider/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="${path}/assetSlider/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="${path}/assetSlider/js/main.js"></script>
        <script type="text/javascript">

        </script>
    </body>
</html>
