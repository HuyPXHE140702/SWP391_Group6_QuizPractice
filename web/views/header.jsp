<%-- 
    Document   : header
    Created on : Jul 25, 2022, 12:40:02 AM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--Header-->
        <!--Page Preloder--> 
        <div id="preloder">
            <div class="loader"></div>
        </div> 

        <!-- Header section -->
        <div>
            <header class="header-section fixed-top" style="position: absolute;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <a href="${path}/home2" class="site-logo">
                                <img src="${path}/asset/img/logo/ezuni_5.png" alt="">
                            </a>
                            <div class="nav-switch">
                                <i class="fa fa-bars"></i>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-2">
                            <!--<a href="" class="site-btn header-btn">Login</a>-->
                            <button class=" site-btn header-btn btn btn-primary">Login</button>
                            <nav class="main-menu">
                                <ul>
                                    <li><a href="${path}/home2">Home</a></li>
                                    <li><a href="${path}/home2">About us</a></li>
                                    <li><a href="${path}/subject">Courses</a></li>
                                    <li><a href="${path}/blog">Blog</a></li>
                                    <li><a href="${path}/home2">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- Header section end -->
        <!--Header end-->
    </body>
</html>
