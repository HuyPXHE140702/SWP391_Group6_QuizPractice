<%-- 
    Document   : queslist_new
    Created on : Jun 19, 2022, 9:48:49 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Quiz" ></c:set>
        <c:set var="subTitle" scope="session" value="Question" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">
        
        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
       <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--tham khảo: https://datatables.net --> 
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1>
                            </div>

                            <!-- Content Row -->
                            <div class="row">
                                
                                   <!-- Main -->
                <div class="container">
                    <div class="text-center">
                        <h2>Question Manager</h2>
                    </div>
                    <div class="admin_ques">
                        <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal"
                            data-bs-target="#addModal">
                            Add new question
                        </button>
                        <table class="display nowrap table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Lesson</th>
                                    <th>Question</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <c:set var="hard" value="h" />
                            <c:set var="medium" value="m" />
                            <tbody>
                                <c:forEach items="${quesList}" var="q">
                                    <tr>
                                        <td>${q.getId()}</td>
                                        <td>${q.getLessonName()}</td>
                                        <td>${q.getContent()}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary list_edit"
                                                data-bs-toggle="modal" data-bs-target="#editModal_${q.getId()}">
                                                <i class="fas fa-edit"></i>
                                            </button>

                                        </td>
                                        <!--Edit Question -->
                                        <div class="modal fade" id="editModal_${q.getId()}" tabindex="-1"
                                            aria-labelledby="editModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center" id="exampleModalLabel">
                                                            Edit question</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="editExam_${q.getId()}">
                                                            <input id="id_${q.getId()}" type="text" name="id_${q}"
                                                                value="${q.getId()}" hidden>
                                                            <input type="text" name="type_${q}" value="edit" hidden>
                                                            <div class="form-group">
                                                                <label>Lesson</label>
                                                                <select id="lessonId_${q.getId()}" name="lessonId_${q}"
                                                                    class="form-select"
                                                                    aria-label="Default select example">
                                                                    <c:forEach items="${lessonList}" var="l">
                                                                        <c:if test="${l.getL_id() eq q.getLessonId()}">
                                                                            <option value="${l.getL_id()}"
                                                                                selected="selected">${l.getName()}
                                                                            </option>
                                                                        </c:if>
                                                                        <c:if test="${l.getL_id() ne q.getLessonId()}">
                                                                            <option value="${l.getL_id()}">
                                                                                ${l.getName()}</option>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Question</label>
                                                                <input id="content_${q.getId()}" type="text"
                                                                    class="form-control" value="${q.getContent()}"
                                                                    name="content_${q}" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Answer</label>
                                                                <input id="answer_${q.getId()}" type="text"
                                                                    class="form-control" value="${q.getAnswer()}"
                                                                    name="answer_${q}" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Other choice options (doesn't match the
                                                                    answer)</label>
                                                                <c:forEach items="${q.getOptions()}" var="opt"
                                                                    varStatus="loop">
                                                                    <c:if test="${opt eq q.getAnswer()}">
                                                                        <div class="ques_option">
                                                                            <input name="ot${loop.index}_${q}"
                                                                                type="text" class="form-control"
                                                                                value="${opt}" hidden>
                                                                        </div>
                                                                    </c:if>
                                                                    <c:if test="${opt ne q.getAnswer()}">
                                                                        <div class="ques_option">
                                                                            <i class="fas fa-circle opt_dot"></i><input
                                                                                name="ot${loop.index}_${q}" type="text"
                                                                                class="form-control" value="${opt}">
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>

                                                            </div>
                                                            <div class="form-group">
                                                                <label>Level</label>
                                                                <select id="type_${q.getId()}" name="level_${q}"
                                                                    class="form-select" value="${q.getType()}"
                                                                    aria-label="Default select example" required>
                                                                    <option value="e">Easy</option>
                                                                    <option value="m">Medium</option>
                                                                    <option value="h">Hard</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Description</label>
                                                                <textarea id="description_${q.getId()}" rows="5"
                                                                    cols="50" value="${q.getDescription()}"
                                                                    name="description_${q}"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Image</label>
                                                                <input id="image_${q.getId()}" type="text"
                                                                    class="form-control" value="${q.getImage()}"
                                                                    name="image_${q}">
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">

                                                        <input type="button" class="btn btn-default"
                                                            data-bs-dismiss="modal" value="Cancel">
                                                        <button type="button" value="${q}"
                                                            onclick="UpdateQuestion(this.value)"
                                                            class="btn btn-primary">Edit</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Add Question -->
                <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="QuestionManageController" method="post">
                                <div class="modal-header">
                                    <h5 class="modal-title text-center" id="addModalLabel">Add a new question</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" name="type" value="add" hidden>
                                    <input type="text" name="id" value="${quesList.get(0).getId()+1}" hidden>
                                    <div class="form-group">
                                        <label>Lesson</label>
                                        <select name="lessonId" class="form-select" aria-label="Default select example">
                                            <c:forEach items="${lessonList}" var="l">
                                                <option value="${l.getL_id()}">${l.getName()}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Question</label>
                                        <input type="text" class="form-control" name="content" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Answer</label>
                                        <input type="text" class="form-control" name="answer" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Other choice options (doesn't match the answer) </label>
                                        <div class="ques_option">
                                            <i class="fas fa-circle opt_dot"></i><input type="text" class="form-control"
                                                name="ot1">
                                        </div>
                                        <div class="ques_option">
                                            <i class="fas fa-circle opt_dot"></i><input type="text" class="form-control"
                                                name="ot2">
                                        </div>
                                        <div class="ques_option">
                                            <i class="fas fa-circle opt_dot"></i><input type="text" class="form-control"
                                                name="ot3">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select name="level" class="form-select" aria-label="Default select example"
                                            required>
                                            <option value="e">Easy</option>
                                            <option value="m">Medium</option>
                                            <option value="h">Hard</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea rows="5" cols="50" name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="text" class="form-control" name="image">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="button" class="btn btn-default" data-bs-dismiss="modal" value="Cancel">
                                    <input type="submit" class="btn btn-success" value="Add">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                                
                            </div>
                        </div>
                        <!-- /.container-fluid -->

                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/chart-area-demo.js"></script>
        <script src="${path}/assetAdmin/js/demo/chart-pie-demo.js"></script>

                <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>
    </body>

</html>