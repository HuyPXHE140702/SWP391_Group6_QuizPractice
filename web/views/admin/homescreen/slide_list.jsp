<%-- 
    Document   : slide_list
    Created on : Jun 27, 2022, 2:07:14 PM
    Author     : Phamb
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Pages" ></c:set>
        <c:set var="subTitle" scope="session" value="Slide List" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="${path}/assetSlider/css/style.css" rel="stylesheet">
        <script src="https://js.upload.io/upload-js/v1"></script>
        <style>
            body:not(.modal-open){
                padding-right: 0px !important;
            }
            .center {
                color: white;
                font-size: 120%;
                display: flex;
                padding-top: 15%;
                padding-left: 10%;
                height: 150px;
                align-items: center;
                z-index: 2;
            }
            .upImage-input img {
                width:100%;
                display:none;
                margin-bottom:30px;
            }
            .upImage-input input {
                display:none;
            }

            .upImage-input label {
                display:block;
                width:100%;
                height:100%;
                line-height:30px;
                text-align:center;
                background:#1172c2;
                color:#fff;
                font-size:15px;
                font-family:"Open Sans",sans-serif;
            }
        </style>
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">
                                ${title} - ${subTitle}
                                <button class="btn btn-primary" data-toggle="modal" data-target="#addNewSlide">New Slide</button>
                            </h1>
                            <!--Begin modal-->
                            <div>
                                <div class="modal fade" id="addNewSlide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form>
                                                <div class="modal-header">
                                                    <h5 class="modal-title"  >New Slide</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        <input type="text" id="newSlide" value="newSlide" hidden class="form-control">
                                                    </button>
                                                </div>
                                                <!--Begin modal-body-->
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Heading</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="newSlideHeading" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Content</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="newSlideContent" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Image</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="newSlideImage" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Time</label>
                                                        <div class="input-group mb-3 col-sm-10">
                                                            <label class="input-group-text" >Begin</label>
                                                            <input type="datetime-local" id="newSlideBegin" class="form-control"">
                                                            <label class="input-group-text" >End</label>
                                                            <input type="datetime-local" id="newSlideEnd" class="form-control"">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Link</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="newSlideLink" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End modal-body-->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick="addNewSlide()">Add</button>
                                                    <script>
                                                        function addNewSlide() {
                                                            var ns = $('#newSlide').val();
                                                            var nsheading = $('#newSlideHeading').val();
                                                            var nscontent = $('#newSlideContent').val();
                                                            var nsimage = $('#newSlideImage').val();
                                                            var nsbegin = $('#newSlideBegin').val();
                                                            var nsend = $('#newSlideEnd').val();
                                                            var nslink = $('#newSlideLink').val();
                                                            $.ajax({
                                                                url: '${path}/Admin/Slide',
                                                                method: 'POST',
                                                                data: {
                                                                    ns: ns,
                                                                    nsheading: nsheading,
                                                                    nscontent: nscontent,
                                                                    nsimage: nsimage,
                                                                    nsbegin: nsbegin,
                                                                    nsend: nsend,
                                                                    nslink: nslink
                                                                },
                                                                success: function () {
                                                                    location.replace("${path}/Admin/Slide");
                                                                }
                                                            });
                                                        }
                                                    </script>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End modal--> 
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <c:forEach items="${slideData}" var="slides" >

                                <div class="col-xl-6 col-lg-6">
                                    <!-- Slide Area -->
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">-->
                                            <link href="${path}/assetAdmin/css/component-custom-switch.css" rel="stylesheet">
                                            <link href="${path}/assetAdmin/css/boostrap.css" rel="stylesheet">
                                            <h6 style="font-size: 180%" class="m-0 font-weight-bold text-primary custom-switch custom-switch-label-onoff custom-switch-xs pl-0">
                                                ${slides.sl_heading}/
                                                <c:if test="${slides.sl_status eq 1}" >
                                                    <input class="custom-switch-input" id="status_of_${slides.sl_id}" type="checkbox"
                                                           value="0"checked onclick="updateSlideStatus('${slides.sl_id}', '0', 'upSta')">
                                                </c:if>
                                                <c:if test="${slides.sl_status ne 1}" >
                                                    <input class="custom-switch-input" id="status_of_${slides.sl_id}" type="checkbox"
                                                           value="1" onclick="updateSlideStatus('${slides.sl_id}', '1', 'upSta')">
                                                </c:if>
                                                <label class="custom-switch-btn" for="status_of_${slides.sl_id}"></label>
                                            </h6>
                                            <script>
                                                function updateSlideStatus(id, status, upSta) {
                                                    $.ajax({
                                                        url: '${path}/Admin/Slide',
                                                        method: 'POST',
                                                        data: {
                                                            sid: id,
                                                            sstatus: status,
                                                            supSta: upSta
                                                        }
                                                    });
                                                }
                                            </script>
                                        </div>
                                        <div class="card-body" >
                                            <div class="chart-area " id="mybackgr_${slides.sl_id}" style="background-image: url('${slides.sl_image}');
                                                 background-position: center;
                                                 background-repeat: no-repeat;
                                                 background-size: cover;
                                                 background-color: black;
                                                 /*filter: brightness(50%);*/
                                                 position: relative;">
                                                <div class="carousel-container center">
                                                    <div class="carousel-content container">
                                                        <h2 class="animate__animated animate__fadeInDown">${slides.sl_heading}<span> Ezuni</span></h2>
                                                        <p class="animate__animated animate__fadeInUp">${slides.sl_content}</p>
                                                        <c:if test="${slides.sl_show eq 1}">
                                                            <fmt:parseDate value="${slides.sl_begin}" pattern="yyyy-MM-dd HH:mm" var="beginBP" type="both" />
                                                            <fmt:formatDate pattern="dd MMM, yyyy" value="${beginBP}" var="begin" />
                                                            <fmt:parseDate value="${slides.sl_end}" pattern="yyyy-MM-dd HH:mm" var="endBP" type="both" />
                                                            <fmt:formatDate pattern="dd MMM, yyyy" value="${endBP}" var="end" />
                                                            <h2 class="animate__animated animate__fadeInDown" style="font-size: 100%">
                                                                <p>Sale from 
                                                                    <strong style="font-size: 150%"> ${begin} </strong>
                                                                    to 
                                                                    <strong style="font-size: 150%"> ${end} </strong>
                                                                </p>
                                                            </h2>
                                                        </c:if>
                                                        <button type="button" class="btn btn-primary" href="${slides.sl_link}">Read More</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <!--<form id="form_slide" >-->
                                            <div class="col-sm-12">
                                                <div class="input-group col-sm-11">
                                                    <div class="input-group">
                                                        <span class="input-group-text" >ID</span>
                                                        <input name="id_${slides.sl_id}" type="text" value="${slides.sl_id}" readonly class="form-control">
                                                    </div>
                                                    <span class="input-group-text" >Heading</span>
                                                    <input name="id_${slides.sl_id}" type="text" value="${slides.sl_heading}" class="form-control">
                                                </div>
                                                <div class="input-group col-sm-11">
                                                    <span class="input-group-text" >Content</span>
                                                    <input name="id_${slides.sl_id}" type="text" value="${slides.sl_content}" class="form-control">
                                                </div>
                                                <div class="input-group col-sm-11">
                                                    <span class="input-group-text" >Image</span>
                                                    <input name="id_${slides.sl_id}" type="text" value="${slides.sl_image}" class="form-control">
                                                    <input type="file" accept="image/*" onclick="show(event, '${slides.sl_id}')" onchange="show(event, '${slides.sl_id}')">
                                                    show
                                                </div>
                                                <div class="input-group col-sm-11">
                                                    <span class="input-group-text" >Begin</span>
                                                    <input name="id_${slides.sl_id}" type="datetime-local" value="${slides.sl_begin}" class="form-control">
                                                    <span class="input-group-text" >End</span>
                                                    <input name="id_${slides.sl_id}" type="datetime-local" value="${slides.sl_end}" class="form-control">
                                                    <h6 class="custom-switch custom-switch-label-onoff custom-switch-sm pl-0">
                                                        <c:if test="${slides.sl_show eq 1}" >
                                                            <input class="custom-switch-input" id="show_of_${slides.sl_id}" type="checkbox"
                                                                   value="0"checked onclick="updateSlideShow('${slides.sl_id}', 0, 'upSho')">
                                                        </c:if>
                                                        <c:if test="${slides.sl_show ne 1}" >
                                                            <input class="custom-switch-input" id="show_of_${slides.sl_id}" type="checkbox"
                                                                   value="1" onclick="updateSlideShow('${slides.sl_id}', 1, 'upSho')">
                                                        </c:if>
                                                        <label class="custom-switch-btn" for="show_of_${slides.sl_id}"></label>
                                                    </h6>
                                                </div>
                                                <div class="input-group col-sm-11">
                                                    <span class="input-group-text" >Redirect Link</span>
                                                    <input name="id_${slides.sl_id}" type="text" value="${slides.sl_link}" class="form-control">
                                                    <button type="button" class="btn btn-primary" value="${slides}" onclick="updateSlideDetail(this.value, '${slides.sl_id}')"> Save</button>
                                                </div>
                                            </div>
                                            <!--</form>-->
                                            <script>
                                                function updateSlideDetail(Slide, id) {
                                                    var inputs = document.getElementsByName('id_' + id);
                                                    $.ajax({
                                                        url: '${path}/Admin/Slide',
                                                        method: 'POST',
                                                        data: {
                                                            sid: inputs[0].value,
                                                            sheading: inputs[1].value,
                                                            scontent: inputs[2].value,
                                                            simage: inputs[3].value,
                                                            sbegin: inputs[4].value,
                                                            send: inputs[5].value,
                                                            slink: inputs[6].value
                                                        },
                                                        success: function (data, textStatus, jqXHR) {
                                                            alert("OK");
                                                            //location.replace("${path}/Admin/Slide")
                                                        }
                                                    });
                                                }
                                                function updateSlideShow(id, show, upSho) {
                                                    $.ajax({
                                                        url: '${path}/Admin/Slide',
                                                        method: 'POST',
                                                        data: {
                                                            sid: id,
                                                            sshow: show,
                                                            supSho: upSho
                                                        }
                                                    });
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                        </div>
                        <!-- /.container-fluid -->

                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                    <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                        <!-- End of Footer -->

                    </div>
                    <!-- End of Content Wrapper -->

                </div>
                <!-- End of Page Wrapper -->

                <!-- Bootstrap core JavaScript-->
                <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
            <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>

    </body>

</html>

