<%-- 
    Document   : buttons
    Created on : Jun 17, 2022, 4:42:21 PM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>
        <!--        
                Note:
                    Ko thay đổi ${path}/assetAdmin/ nhé
        -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Buttons</title>

        <!-- Custom fonts for this template-->
        <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">

    </head>

    <body>

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="${path}/Admin">
                <div class="sidebar-brand-icon rotate-n-15">
                    <img class="sidebar_img" src="${path}/asset/img/logo/favicon-32x32.png" alt="Ezuni_logo" />
                </div>
                <div class="sidebar-brand-text mx-3">Ezuni</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="${path}/Admin">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>
            
            <!-- Nav Item - Marketing Collapse Menu -->
            <c:set var="Marketing" scope="session" value="Marketing" />
            <li class="${(Marketing eq title) ? 'nav-item active' : 'nav-item'}">
                <a class="${(Marketing eq title) ? 'nav-link' : 'nav-link collapsed'}" 
                   href="#" data-toggle="collapse" data-target="#collapseMarketing"
                   aria-expanded="true" aria-controls="collapseMarketing">
                    <i class="fas fa-fw fa-table"></i>
                    <span>${Marketing}</span>
                </a>
                <div id="collapseMarketing" class="${(Marketing eq title) ? 'collapse show' : 'collapse'}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom ${Marketing}:</h6>
                        <c:set var="SaleCourse" scope="session" value="Sale Course" />
                        <a class="${(SaleCourse eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/Admin/SaleCourse">${SaleCourse}</a>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Subjects Collapse Menu -->
            <c:set var="Subjects" scope="session" value="Subjects" />
            <li class="${(Subjects eq title) ? 'nav-item active' : 'nav-item'}">
                <a class="${(Subjects eq title) ? 'nav-link' : 'nav-link collapsed'}" 
                   href="#" data-toggle="collapse" data-target="#collapseSubjects"
                   aria-expanded="true" aria-controls="collapseSubjects">
                    <i class="fas fa-fw fa-play"></i>
                    <span>${Subjects}</span>
                </a>
                <div id="collapseSubjects" class="${(Subjects eq title) ? 'collapse show' : 'collapse'}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom ${Subjects}:</h6>
                        <c:set var="SubjectList" scope="session" value="Subject List" />
                        <a class="${(SubjectList eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/Admin/Subject">${SubjectList}</a>
                    </div>
                </div>
            </li>
            <!--end Subjects Collapse Menu-->

            <!-- Nav Item - Quiz Collapse Menu -->
            <c:set var="Quiz" scope="session" value="Quiz" />
            <li class="${(Quiz eq title) ? 'nav-item active' : 'nav-item'}">
                <a class="${(Quiz eq title) ? 'nav-link' : 'nav-link collapsed'}" href="#" data-toggle="collapse" data-target="#collapseQuiz"
                   aria-expanded="true" aria-controls="collapseQuiz">
                    <i class="fas fa-fw fa-pager"></i>
                    <span>${Quiz}</span>
                </a>
                <div id="collapseQuiz" class="${(Quiz eq title) ? 'collapse show' : 'collapse'}" aria-labelledby="headingUtilities"
                     data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom ${Quiz}:</h6>

                        <c:set var="Examlist" scope="session" value="Examlist" />
                        <a class="${(Examlist eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href="${path}/examslist">${Examlist}</a>
                        <c:set var="Question" scope="session" value="Question" />
                        <a class="${(Question eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href = "${path}/questionslist">${Question}</a>
                    </div>
                </div>
            </li>
            <!--end Quiz Collapse Menu-->

            <!-- Nav Item - Users Collapse Menu -->
            <c:set var="Users" scope="session" value="Users" />
            <li class="${(Users eq title) ? 'nav-item active' : 'nav-item'}">
                <a class="${(Users eq title) ? 'nav-link' : 'nav-link collapsed'}" href="#" data-toggle="collapse" data-target="#collapseUser"
                   aria-expanded="true" aria-controls="collapseUser">
                    <i class="fas fa-fw fa-user"></i>
                    <span>${Users}</span>
                </a>
                <div id="collapseUser" class="${(Users eq title) ? 'collapse show' : 'collapse'}" aria-labelledby="headingUtilities"
                     data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom ${Users}:</h6>
                        <c:set var="UserList" scope="session" value="User List" />
                        <a class="${(UserList eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href="${path}/Admin/User">${UserList}</a>
                        <c:set var="LoginHistory" scope="session" value="Login History" />
                        <a class="${(LoginHistory eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href="utilities-color.html">${LoginHistory}</a>
                        <c:set var="Blogs" scope="session" value="Blogs" />
                        <a class="${(Blogs eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href="${path}/Admin/Blog">${Blogs}</a>
                        <c:set var="Registrations" scope="session" value="Registrations" />
                        <a class="${(Registrations eq subTitle) ? 'collapse-item active' : 'collapse-item'}" 
                           href="RegistrationListController">${Registrations}</a>
                    </div>
                </div>
            </li>
            <!--end Users Collapse Menu-->

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Addons
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <c:set var="Pages" scope="session" value="Pages" />
            <li class="${(Pages eq title) ? 'nav-item active' : 'nav-item'}">
                <a class="${(Pages eq title) ? 'nav-link' : 'nav-link collapsed'}" href="#" data-toggle="collapse" data-target="#collapsePages"
                   aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>${Pages}</span>
                </a>
                <div id="collapsePages" class="${(Pages eq title) ? 'collapse show' : 'collapse'}" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <c:set var="Login" scope="session" value="Login" />
                        <a class="${(Login eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/views/admin/loginscreen/login.jsp">Login</a>
                        <c:set var="Register" scope="session" value="Register" />
                        <a class="${(Register eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/views/admin/loginscreen/register.jsp">Register</a>
                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Home Screens:</h6>
                        <c:set var="Slide" scope="session" value="Slide List" />
                        <a class="${(Slide eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/Admin/Slide">Slide</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <c:set var="p404" scope="session" value="404 Page" />
                        <a class="${(p404 eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/views/admin/otherpage/404.jsp">404 Page</a>
                        <c:set var="BlankPage" scope="session" value="Blank Page" />
                        <a class="${(BlankPage eq subTitle) ? 'collapse-item active' : 'collapse-item'}" href="${path}/views/admin/template.jsp">Blank Page</a>
                    </div>
                </div>
            </li>
<!--
             Nav Item - Charts 
            <li class="nav-item">
                <a class="nav-link" href="charts.html">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Charts</span></a>
            </li>

             Nav Item - Tables 
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tables</span></a>
            </li>-->

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

            <!-- Sidebar Message -->
            <div class="sidebar-card d-none d-lg-flex">
                <img class="sidebar-card-illustration mb-2" src="${path}/assetAdmin/img/undraw_rocket.svg" alt="...">
                <p class="text-center mb-2"><strong>Notification</strong> !</p>
                <a class="btn btn-success btn-sm" href="">Read more!</a>
            </div>

        </ul>
        <!-- End of Sidebar -->
        <!-- Bootstrap core JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>

        <!--         Page level plugins 
                <script src="${path}/assetAdmin/vendor/chart.js/Chart.min.js"></script>
        
                 Page level custom scripts 
                <script src="${path}/assetAdmin/js/demo/chart-area-demo.js"></script>
                <script src="${path}/assetAdmin/js/demo/chart-pie-demo.js"></script>-->
        <!--MyJS-->
    <!--    <script src="${path}/assetAdmin/js/admin/DashBoardController.js"></script>
        <script src="${path}/assetAdmin/js/admin/QuizzController.js"></script>
        <script src="${path}/assetAdmin/js/connection.js"></script>
         main js  
        <script>
                RenderDashBroad('Overview');
        </script>-->

    </body>

</html>