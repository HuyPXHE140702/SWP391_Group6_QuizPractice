<%-- 
    Document   : user_list
    Created on : Jun 28, 2022, 9:05:40 AM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Users" ></c:set>
        <c:set var="subTitle" scope="session" value="User List" ></c:set>
            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template -->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">

        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!--tham khảo: https://datatables.net --> 
        <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
        <style>
            body:not(.modal-open){
                padding-right: 0px !important;
            }
        </style>
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <h1 class="h3 mb-2 text-gray-800">${subTitle}</h1>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>User</th>
                                                <th>Full name</th>
                                                <th>Status</th>
                                                <th>Email</th>
                                                <th>Created</th>
                                                <th>...</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>User</th>
                                                <th>Full name</th>
                                                <th>Status</th>
                                                <th>Email</th>
                                                <th>Created</th>
                                                <th>...</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${userData}" var="users" varStatus="x">
                                                <tr>
                                                    <td>
                                                        <img style="  width: 100px; height: 100px; object-fit: contain;" 
                                                             src="${users.avatar}" alt="">

                                                        <a style="font-size: 170%;" href="#" class="user-link">${users.username}</a>/ 
                                                        <span class="user-subhead">${users.role}</span>
                                                    </td>
                                                    <td >${users.firstname} ${users.lastname}</td>
                                                    <td id="td_status">
                                                        <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">-->
                                                        <link href="${path}/assetAdmin/css/boostrap.css" rel="stylesheet">
                                                        <link href="${path}/assetAdmin/css/component-custom-switch.css" rel="stylesheet">
                                                        <div class="custom-switch custom-switch-label-status pl-0">

                                                            <c:if test="${users.status eq 1}" >
                                                                <input class="custom-switch-input" id="status_of_${users.id}" type="checkbox"
                                                                       value="0"checked onclick="updateUserStatus('${users.id}', '0')">
                                                            </c:if>
                                                            <c:if test="${users.status ne 1}" >
                                                                <input class="custom-switch-input" id="status_of_${users.id}" type="checkbox"
                                                                       value="1" onclick="updateUserStatus('${users.id}', '1')">
                                                            </c:if>
                                                            <label class="custom-switch-btn" for="status_of_${users.id}"></label>

                                                        </div>
                                                        <script>
                                                            function updateUserStatus(id, status) {
                                                                $.ajax({
                                                                    url: '${path}/Admin/User',
                                                                    method: 'POST',
                                                                    data: {
                                                                        uid: id,
                                                                        ustatus: status
                                                                    }
                                                                });
                                                            }
                                                        </script>
                                                    </td>
                                                    <td ><a href="#">${users.email}</a></td>
                                                    <td ><a>${users.createTime}</a></td>
                                                    <td>
                                                        <div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCenter_user_${users.id}">
                                                                Edit
                                                            </button>

                                                            <!-- Modal -->
                                                            <div class="modal fade bd-example-modal-xl" id="modalCenter_user_${users.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalCenterTitle">Full information of ${users.username}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form id="form_user_${users.id}">
                                                                                <div class="row" >
                                                                                    <div class="col-12">
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text" >ID</span>
                                                                                            <input type="text" value="${users.id}" id="thisId" readonly class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                                                                                            <span class="input-group-text">Username</span>
                                                                                            <input type="text" value="${users.username}" id="thisUsername" readonly class="form-control" placeholder="Account" aria-label="Username" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <label class="input-group-text" for="thisStatus">Status</label>
                                                                                            <input type="text" value="${users.status}" id="thisStatus" readonly class="form-control" placeholder="Account" aria-label="Username" aria-describedby="basic-addon1">

                                                                                            <span class="input-group-text">Role</span>
                                                                                            <input type="text" value="${users.role}" id="thisRole" readonly class="form-control" placeholder="Role" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Password</span>
                                                                                            <input type="text" value="${users.pass}" id="thisPass" readonly class="form-control" placeholder="Password"  aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <img  style="  width: 100px; height: 100px; object-fit: contain;" 
                                                                                                  src="${users.avatar}" alt=""> <br>
                                                                                            <input type="text" value="${users.avatar}" id="thisAvatar"  readonly class="form-control" placeholder="Avatar"  aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Created time</span>
                                                                                            <input type="text" value="${users.createTime}" id="thisCreated" readonly class="form-control" placeholder="Account"  aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Email</span>
                                                                                            <input type="text" value="${users.email}" id="thisEmail" readonly class="form-control" placeholder="Email" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Description</span>
                                                                                            <textarea class="form-control" id="thisDes" aria-label="With textarea" readonly style="height : 15vh">${users.description}</textarea>
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">First and last name</span>
                                                                                            <input type="text" value="${users.firstname}" aria-label="First name " id="thisFname" readonly placeholder="First Name" class="form-control">
                                                                                            <input type="text" value="${users.lastname}" aria-label="Last name" id="thisLname" readonly placeholder="Last Name" class="form-control">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">DOB</span>
                                                                                            <input type="date" value="${users.birthdate}" id="thisBirth" readonly class="form-control p-2" aria-label="Username" aria-describedby="basic-addon1">
                                                                                            <div class="input-group-text ms-auto">
                                                                                                <c:if test="${users.gender eq 'male'}" >
                                                                                                    <div class="form-check form-check-inline">
                                                                                                        <input class="form-check-input" type="radio" id="thisGender" readonly value="male" ${users.gender=='male' ? 'checked' : ''}>
                                                                                                        <label class="form-check-label" for="thisGender">Male</label>
                                                                                                    </div>
                                                                                                    <div class="form-check form-check-inline">
                                                                                                        <input class="form-check-input" type="radio" id="Gender_female" readonly value="female" ${users.gender=='female' ? 'checked' : ''}>
                                                                                                        <label class="form-check-label" for="Gender_female">Female</label>
                                                                                                    </div>
                                                                                                </c:if>
                                                                                                <c:if test="${users.gender eq 'female'}" >
                                                                                                    <div class="form-check form-check-inline">
                                                                                                        <input class="form-check-input" type="radio" id="Gender_male" readonly value="male" ${users.gender=='male' ? 'checked' : ''}>
                                                                                                        <label class="form-check-label" for="Gender_male">Male</label>
                                                                                                    </div>
                                                                                                    <div class="form-check form-check-inline">
                                                                                                        <input class="form-check-input" type="radio" id="thisGender" readonly value="female" ${users.gender=='female' ? 'checked' : ''}>
                                                                                                        <label class="form-check-label" for="thisGender">Female</label>
                                                                                                    </div>
                                                                                                </c:if>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Phone Number</span>
                                                                                            <input type="text" value="${users.phonenum}" id="thisPhone" readonly class="form-control" placeholder="Phone Number"  aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Address</span>
                                                                                            <input type="text" value="${users.address}" id="thisAddress" readonly class="form-control" placeholder="Address" aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                        <div class="input-group mb-3">
                                                                                            <span class="input-group-text">Company</span>
                                                                                            <input type="text" value="${users.company}" id="thisCompany" readonly class="form-control" placeholder="Company"  aria-describedby="basic-addon1">
                                                                                            <span class="input-group-text">School</span>
                                                                                            <input type="text" value="${users.school}" id="thisSchool" readonly class="form-control" placeholder="School"  aria-describedby="basic-addon1">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>

        <style>
            img{
                position: relative;
            }
            img:before {
                content: ' ';

                position: absolute;
                width: 100px;
                height: 100px;

                background-color: white;
                background-image:url(${path}/asset/img/logo/favicon-32x32.png);

            }
        </style>


    </body>

</html>
