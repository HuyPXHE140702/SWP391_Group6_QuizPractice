<%-- 
    Document   : allTopicQuestion
    Created on : Jul 19, 2022, 9:59:58 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Topic" ></c:set>
        <c:set var="subTitle" scope="session" value="Question List" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!--tham khảo: https://datatables.net --> 

        <script src="https://js.upload.io/upload-js/v1"></script>

    </head>

    <body id="page-top" >
        <!--data-new-gr-c-s-check-loaded="14.1017.0" data-gr-ext-installed class="modal-open" style="padding-right:10px;" cái này bị ghi đè khi click modal, ko hiểu tại sao @@--> 
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1>

                        </div>


                        <a href="${path}/Admin/Subject/View?s_id=${requestScope.topic.s_id}"> <button class="btn btn-danger">Back</button></a>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="row">
                                    <div class="col-md-6"><h6 class="m-0 font-weight-bold text-primary"> ${subTitle}</h6></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2">
                                        <button class="btn btn-secondary" data-toggle="modal" data-target="#addQuestion" >Add Question</button>

                                    </div>
                                </div>


                            </div>

                            <!--Begin modal - Add Question-->              
                            <div>
                                <div class="modal fade" id="addQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form onsubmit="addQuestion('${requestScope.topic.topic_id}');return false;">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"  >Question Topic: ${requestScope.topic.name}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <!--Begin modal-body-->
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Question</label>
                                                        <div class="col-sm-10">
                                                            <input type="hidden" name="question_add_" value="add" class="form-control d-none">
                                                            <input type="text" name="question_add_"  required class="form-control" >
                                                        </div>


                                                        <label class="col-sm-2 col-form-label">Question Result</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="question_add_"  required class="form-control" >
                                                        </div>

                                                        <label class="col-sm-2 col-form-label">Question Option</label>
                                                        <div class="col-sm-10">

                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="question_add_"  required class="form-control" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="question_add_"  required class="form-control" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" name="question_add_"  required class="form-control" >
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <label class="col-sm-2 col-form-label">Question Description</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="question_add_"  required class="form-control" >
                                                        </div>

                                                    </div>



                                                </div>
                                                <!--End modal-body-->
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" onclick="" >Add</button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end modal-->

                            <script>

                                function CheckValueAdd() {
                                    var inputs = document.getElementsByName('question_add_');


                                    if (inputs[2].value === inputs[3].value
                                            || inputs[2].value === inputs[4].value
                                            || inputs[2].value === inputs[5].value
                                            || inputs[3].value === inputs[4].value
                                            || inputs[3].value === inputs[5].value
                                            || inputs[4].value === inputs[5].value
                                            ) {
                                        alert('Your result and all option must the same!! ');
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }

                                function addQuestion(topic_id) {
                                    var inputs = document.getElementsByName('question_add_');
                                    $.ajax({
                                        url: '${path}/Admin/Subject/Topic/Question?topic_id=' + topic_id,
                                        method: 'POST',
                                        data: {
                                            topic_id: topic_id,
                                            action: inputs[0].value,
                                            question: inputs[1].value,
                                            question_result: inputs[2].value,
                                            question_option1: inputs[3].value,
                                            question_option2: inputs[4].value,
                                            question_option3: inputs[5].value,
                                            question_description: inputs[6].value


                                        },
                                        beforeSend: function (xhr) {
                                            return CheckValueAdd();
                                        }
                                        ,
                                        success: function (data, textStatus, jqXHR) {
                                            alert("Your question has Added");

                                            location.reload(true);
                                        }
                                    });

                                    return false;
                                }


                            </script>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th style="width: 350px">Question</th>
                                                <th>List Option</th>
                                                <th>Description</th>
                                                <th style="width: 100px">Action</th>

                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Question</th>
                                                <th>List Option</th>
                                                <th>Description</th>

                                                <th>

                                                </th>
                                            </tr>

                                        </tfoot>
                                        <tbody>
                                            <%int i = 0;%>
                                            <c:forEach var="question" items="${requestScope.topic.questions}">
                                                <tr>
                                                    <%i = i + 1;%>
                                                    <td><%=i%></td>
                                                    <td>${question.question}
                                                        <div>
                                                            <button class="btn btn-danger" onclick="deleteQuestion(${question.id})" >Delete</button>
                                                        </div>
                                                        <script>

                                                            function deleteQuestion(question_id) {
                                                                var inputs = document.getElementsByName('question_delete_' + question_id);
                                                                $.ajax({
                                                                    url: '${path}/Admin/Subject/Topic/Question?',
                                                                    method: 'POST',
                                                                    data: {
                                                                        question_id: question_id,
                                                                        action: 'delete'
                                                                    },
                                                                    beforeSend: function (xhr) {
                                                                        return confirm('Do you want to delete this question');
                                                                    }
                                                                    , success: function (data, textStatus, jqXHR) {
                                                                        alert("Delete question success");

                                                                        location.reload(true);
                                                                    }
                                                                });

                                                                return false;
                                                            }
                                                        </script>
                                                    </td>
                                                    <td>
                                                        <ol>
                                                            <li style=" color: green; font-size: 15px; font-weight: bold">
                                                                ${question.result}
                                                            </li> 
                                                            <c:forEach var="option" items="${question.questionChoices}">
                                                                <c:if test="${question.result ne option.option}">
                                                                    <li>${option.option}</li> 
                                                                    </c:if>

                                                            </c:forEach>
                                                        </ol>
                                                    </td>
                                                    <td>
                                                        ${question.description}

                                                    </td>
                                                    <td>

                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#editQuestion${question.id}" style="font-size:15px">Edit Question</button>

                                                    </td>

                                                </tr>
                                                <!--Begin modal - Add Question-->              
                                            <div>
                                                <div class="modal fade" id="editQuestion${question.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <form onsubmit=" editQuestion('${question.id}');
                                                                    return false;
                                                                  ">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"  >Question Topic: ${requestScope.topic.name}</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <!--Begin modal-body-->
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Question</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="hidden" name="question_edit_${question.id}" value="edit" class="form-control d-none">
                                                                            <input type="text" name="question_edit_${question.id}"  required class="form-control" value="${question.question}">
                                                                        </div>


                                                                        <label class="col-sm-2 col-form-label">Question Result</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" name="question_edit_${question.id}"  required class="form-control" value="${question.result}">
                                                                        </div>

                                                                        <label class="col-sm-2 col-form-label">Question Option</label>
                                                                        <div class="col-sm-10">

                                                                            <div class="row">

                                                                                <c:forEach var="option" items="${question.questionChoices}">
                                                                                    <c:if test="${question.result ne option.option}">
                                                                                        <div class="col-sm-4">
                                                                                            <input type="text" name="question_edit_${question.id}"  value="${option.option}" required class="form-control" 


                                                                                                   >
                                                                                        </div>
                                                                                    </c:if>

                                                                                </c:forEach>

                                                                            </div>

                                                                        </div>


                                                                        <label class="col-sm-2 col-form-label">Question Description</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" name="question_edit_${question.id}"  required class="form-control" value="${question.description}" >
                                                                        </div>

                                                                    </div>



                                                                </div>
                                                                <!--End modal-body-->
                                                                <div class="modal-footer">
                                                                    <button class="btn btn-secondary" data-dismiss="modal" onclose="location.reload();">Close</button>
                                                                    <button type="submit" class="btn btn-primary" onclick="" >Edit</button>

                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end modal-->

                                            <script>

                                                function CheckValueEdit(question_id) {
                                                    var inputs = document.getElementsByName('question_edit_' + question_id);
//                                                     alert(inputs[2].value+ "///"+inputs[3].value+"///"+inputs[4].value+"///"+inputs[5].value);

                                                    if (inputs[2].value === inputs[3].value
                                                            || inputs[2].value === inputs[4].value
                                                            || inputs[2].value === inputs[5].value
                                                            || inputs[3].value === inputs[4].value
                                                            || inputs[3].value === inputs[5].value
                                                            || inputs[4].value === inputs[5].value
                                                            ) {
                                                        alert('Your result and all option must the same!! ');
                                                        return false;

                                                    } else {
                                                        return true;

                                                    }
                                                }

                                                function editQuestion(question_id) {
                                                    var inputs = document.getElementsByName('question_edit_' + question_id);


                                                    $.ajax({
                                                        url: '${path}/Admin/Subject/Topic/Question',
                                                        method: 'POST',
                                                        data: {
                                                            question_id: question_id,
                                                            action: inputs[0].value,
                                                            question: inputs[1].value,
                                                            question_result: inputs[2].value,
                                                            question_option1: inputs[3].value,
                                                            question_option2: inputs[4].value,
                                                            question_option3: inputs[5].value,
                                                            question_description: inputs[6].value

                                                        },
                                                        beforeSend: function (xhr) {
                                                            return CheckValueEdit(question_id);
                                                        }
                                                        ,
                                                        success: function (data, textStatus, jqXHR) {
                                                            alert("Your question has Edited");

                                                        }
                                                    });
//                                                    location.reload(true);

                                                    return false;

                                                }


                                            </script>
                                            <!--end modal-->

                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->
                <input id="uppload" value ="" style="display: none" >

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>

        <script>
                                                var upload = new Upload({apiKey: "public_kW15asz9FBowcqDRRtfmv8s8sfST"});
                                                var uploadImage = upload.createFileInputHandler({

                                                    // show process of uploading image
                                                    onProgress: ({ bytesSent, bytesTotal }) => {
                                                        let loading = document.getElementsByName("processImgSub");

                                                        for (var i in loading) {
                                                            i.max = bytesTotal;
                                                            i.value = bytesSent;
                                                    }

//                        document.getElementsByName("processImgSub").value = bytesSent;

                                                    },
                                                    // upload image successfully
                                                    onUploaded: ({ fileUrl, fileId}) => {
                                                        console.log(fileUrl);
//                        document.getElementById("processImgSub").style.visibility = 'hidden';
                                                        Url_Upload = fileUrl;
//                        alert(fileUrl);
                                                        document.getElementById('uppload').value = fileUrl;
                                                    }
                                                });
        </script>

    </body>

</html>


