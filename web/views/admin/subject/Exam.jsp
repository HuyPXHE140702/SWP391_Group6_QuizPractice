<%-- 
    Document   : Exam
    Created on : Jul 19, 2022, 10:41:04 PM
    Author     : ADMIN
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Subjects" ></c:set>
        <c:set var="subTitle" scope="session" value="Subject List" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!--tham khảo: https://datatables.net --> 

    </head>

    <body id="page-top" >
        <!--data-new-gr-c-s-check-loaded="14.1017.0" data-gr-ext-installed class="modal-open" style="padding-right:10px;" cái này bị ghi đè khi click modal, ko hiểu tại sao @@--> 
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1>
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Earnings ${sessionScope.totalMoney.getTimeMoney()}</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$${sessionScope.totalMoney.getTotalMoney()}</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    ...</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$...</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">....
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">....</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar"
                                                                 style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                                 aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Pending Requests Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    ....</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">....</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Subject List</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Organization</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Organization</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${subjectData}" var="subject">
                                                <tr>
                                                    <td>${subject.getS_id()}</td>
                                                    <td>${subject.getName()}</td>
                                                    <td>
                                                        <c:forEach items="${types}" var="type">
                                                            <c:if test="${type.id eq subject.type_id}" >
                                                                ${type.name}
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                    <td>${subject.getOrganization()}</td>
                                                    <td>
                                                        <div>
                                                            <link href="${path}/assetAdmin/css/component-custom-switch.css" rel="stylesheet">
                                                            <div class="custom-switch custom-switch-label-status pl-0">
                                                                <c:if test="${subject.getStatus() eq 1}">
                                                                    <input class="custom-switch-input" id="status_${subject.getS_id()}" type="checkbox" 
                                                                           checked value="0" onclick="updateSubjectStatus('${subject.getS_id()}', 0, 'upSta')">
                                                                </c:if>
                                                                <c:if test="${subject.getStatus() ne 1}">
                                                                    <input class="custom-switch-input" id="status_${subject.getS_id()}" type="checkbox" 
                                                                           value="1" onclick="updateSubjectStatus('${subject.getS_id()}', 1, 'upSta')">
                                                                </c:if>
                                                                <label class="custom-switch-btn" for="status_${subject.getS_id()}"></label>
                                                            </div>
                                                        </div>
                                                        <script>
                                                            function updateSubjectStatus(id, status, upSta) {
                                                                $.ajax({
                                                                    url: '${path}/Admin/Subject',
                                                                    method: 'POST',
                                                                    data: {
                                                                        sjid: id,
                                                                        sjstatus: status,
                                                                        sjupSta: upSta
                                                                    }
                                                                });
                                                            }
                                                        </script>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success"> <a href="${path}/Admin/Subject/View?s_id=${subject.s_id}">View</a></button>
                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#editSubject_${subject.getS_id()}">Edit</button>
                                                        <!--Begin modal-->
                                                        <div>
                                                            <div class="modal fade" id="editSubject_${subject.getS_id()}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <form>
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"  >${subject.getName()}</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <!--Begin modal-body-->
                                                                            <div class="modal-body">
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Name</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input type="text" name="id_${subject.getS_id()}" value="${subject.getS_id()}" class="form-control d-none">
                                                                                        <input type="text" name="id_${subject.getS_id()}" required class="form-control" value="${subject.getName()}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Category</label>
                                                                                    <div class="col-sm-10">
                                                                                        <select required name="id_${subject.getS_id()}" class="form-control"  placeholder="Subject Type">
                                                                                            <c:forEach items="${types}" var="types">
                                                                                                <c:if test="${types.id eq subject.type_id}" >
                                                                                                    <option value="${types.id}" selected>${types.name}</option>
                                                                                                </c:if>
                                                                                                <c:if test="${types.id ne subject.type_id}" >
                                                                                                    <option value="${types.id}" >${types.name}</option>
                                                                                                </c:if>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Subject Description</label>
                                                                                    <div class="col-sm-10">
                                                                                        <textarea style='height: 200px;' type="text" name="id_${subject.getS_id()}" value="${subject.getDescription()}" 
                                                                                                  required class="form-control" id="subjectDes" placeholder="Subject Description">${subject.getDescription()}
                                                                                        </textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Organization</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input name="id_${subject.getS_id()}" type="text" required class="form-control" value="${subject.getOrganization()}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Price</label>
                                                                                    <div class="col-sm-10">
                                                                                        <c:forEach items="${prices}" var="prices">
                                                                                            <c:if test="${prices.getS_id() eq subject.getS_id() and prices.getStatus() eq 1}">
                                                                                                <input type="number" name="id_${subject.getS_id()}" value="${prices.getPrice_id()}" class="form-control d-none">
                                                                                                <input name="id_${subject.getS_id()}" type="number" required class="form-control" value="${prices.getPrice()}">
                                                                                            </c:if>
                                                                                        </c:forEach>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--End modal-body-->
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary" value="${subject}" onclick="updateSubjectDeatil(this.value, '${subject.getS_id()}')">Save</button>
                                                                                <script>
                                                                                    function updateSubjectDeatil(Subject, id) {
                                                                                        var inputs = document.getElementsByName('id_' + id);
                                                                                        $.ajax({
                                                                                            url: '${path}/Admin/Subject',
                                                                                            method: 'POST',
                                                                                            data: {
                                                                                                sjid: inputs[0].value,
                                                                                                sjname: inputs[1].value,
                                                                                                sjtype: inputs[2].value,
                                                                                                sjdes: inputs[3].value,
                                                                                                sjorga: inputs[4].value,
                                                                                                prid: inputs[5].value,
                                                                                                prprice: inputs[6].value
                                                                                            },
                                                                                            success: function (data, textStatus, jqXHR) {
                                                                                                alert("OK");
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                </script>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End modal-->                                   
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>

    </body>

</html>


