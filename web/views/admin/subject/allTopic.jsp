<%-- 
    Document   : allTopic
    Created on : Jul 18, 2022, 11:15:37 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Subjects" ></c:set>
        <c:set var="subTitle" scope="session" value="${subject.name}" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <link rel="stylesheet" href="./Font-Awesomecss/css/font-awesome.min.css">
        <!--tham khảo: https://datatables.net --> 
        <script src="https://js.upload.io/upload-js/v1"></script>
    </head>

    <body id="page-top" >
        <!--data-new-gr-c-s-check-loaded="14.1017.0" data-gr-ext-installed class="modal-open" style="padding-right:10px;" cái này bị ghi đè khi click modal, ko hiểu tại sao @@--> 
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1>
                        </div>

                        <!-- Content Row -->

                        <!-- Content Row -->
                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Earnings ${requestScope.totalMoney.getTimeMoney()}</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$${requestScope.totalMoney.getTotalMoney()}</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    ...</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">...</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">....
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">....</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar"
                                                                 style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                                 aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Pending Requests Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    ....</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">....</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="${path}/Admin/Subject"> <button class="btn btn-danger">Back</button></a>
                        <br/>
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 class="m-0 font-weight-bold text-primary " >Topic List</h6> 
                                    </div>                                  
                                    <div class="col-md-4"></div>

                                    <div class="col-md-2"><button class="btn btn-secondary "  data-toggle="modal" data-target="#addTopic" >Add Topic</button></div>

                                </div>
                            </div>

                            <!--Begin modal - Add Topic-->
                            <div>
                                <div class="modal fade" id="addTopic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form>
                                                <div class="modal-header">
                                                    <h5 class="modal-title"  >New Topic</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <!--Begin modal-body-->
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 col-form-label">Topic Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="hidden" name="topic_add_" value="add" class="form-control d-none">
                                                            <input type="text" name="topic_add_"  required class="form-control" >
                                                        </div>
                                                        <label class="col-sm-3 col-form-label">Topic Description</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="topic_add_"  required class="form-control" >
                                                        </div>
                                                        <label class="col-sm-3 col-form-label">Topic purpose</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="topic_add_"  required class="form-control" >
                                                        </div>
                                                    </div>



                                                </div>
                                                <!--End modal-body-->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary"  onclick="addTopic('${subject.s_id}')">Add</button>
                                                    <script>
                                                        function addTopic(s_id) {
                                                            var inputs = document.getElementsByName('topic_add_');
                                                            $.ajax({
                                                                url: '${path}/Admin/Subject/View',
                                                                method: 'POST',
                                                                data: {
                                                                    s_id: s_id,
                                                                    action: inputs[0].value,
                                                                    topic_name: inputs[1].value,
                                                                    topic_description: inputs[2].value,
                                                                    topic_purpose: inputs[3].value
                                                                },
                                                                success: function (data, textStatus, jqXHR) {
                                                                    alert("Add " + textStatus);
                                                                },
                                                                complete: function (jqXHR, textStatus) {
                                                                    location.reload();
                                                                }
                                                            });
                                                        }
                                                    </script>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end modal-->

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Topic Name</th>
                                                <th>All Lesson</th>
                                                <!--<th></th>-->

                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <!--<th></th>-->

                                            </tr>
                                        </tfoot>

                                        <tbody>

                                            <%int i = 1;%>
                                            <c:forEach items="${requestScope.subject.topics}" var="topic">
                                                <tr>

                                                    <td><%=i%></td>
                                                    <%i = i + 1;%>
                                                    <td> 

                                                        <h5>${topic.name}</h5>
                                                        <div class="row">
                                                            <div class="col-xm-4 Edit_Status">
                                                                <div>
                                                                    <link href="${path}/assetAdmin/css/component-custom-switch.css" rel="stylesheet">
                                                                    <div class="custom-switch custom-switch-label-status pl-0">



                                                                        <c:if test="${topic.status eq 1}">
                                                                            <input class="custom-switch-input" id="status_${topic.topic_id}" type="checkbox" 
                                                                                   checked value="Off" onclick="updateTopicStatus('${topic.topic_id}', 'Off', 'status')">
                                                                        </c:if>
                                                                        <c:if test="${topic.status ne 1}">
                                                                            <input class="custom-switch-input" id="status_${topic.topic_id}" type="checkbox" 
                                                                                   value="ON" onclick="updateTopicStatus('${topic.topic_id}', 'On', 'status')">
                                                                        </c:if>
                                                                        <label class="custom-switch-btn" for="status_${topic.topic_id}"></label>
                                                                    </div>
                                                                </div>
                                                                <script>
                                                                    function updateTopicStatus(id, status, action) {
                                                                        $.ajax({
                                                                            url: '${path}/Admin/Subject/View',
                                                                            method: 'POST',
                                                                            data: {
                                                                                topic_id: id,
                                                                                status: status,
                                                                                action: action
                                                                            },
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                alert("Done");
                                                                            }
                                                                        });
                                                                    }
                                                                </script>

                                                            </div>

                                                            <div class="col-xm-4">

                                                                <button class="btn btn-primary " data-toggle="modal" data-target="#editTopic_${topic.topic_id}">View</button>

                                                                <!--Begin modal-->
                                                                <div>
                                                                    <div class="modal fade" id="editTopic_${topic.topic_id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form onsubmit="return updateTopicDeatil(this.value, '${topic.topic_id}');">
                                                                                    <div class="modal-header">
                                                                                        <h5 class="modal-title"  >${subject.name}</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>

                                                                                    <!--Begin modal-body-->
                                                                                    <div class="modal-body">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-sm-3 col-form-label">Name</label>
                                                                                            <div class="col-sm-9">
                                                                                                <input type="hidden" name="id_${topic.topic_id}" value="edit" class="form-control d-none">
                                                                                                <input type="text" name="id_${topic.topic_id}" value="${topic.s_id}" class="form-control d-none">
                                                                                                <input type="text" name="id_${topic.topic_id}" required class="form-control" value="${topic.name}">
                                                                                            </div>
                                                                                            <label class="col-sm-3 col-form-label">Topic Description</label>
                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" name="id_${topic.topic_id}" required class="form-control" value="${topic.description}">
                                                                                            </div>
                                                                                            <label class="col-sm-3 col-form-label">Topic Purpose</label>
                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" name="id_${topic.topic_id}" required class="form-control" value="${topic.purpose}">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                                                    <!--End modal-body-->
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                                                                                        <button type="submit" class="btn btn-primary" value="${topic}" onclick="">Save</button>
                                                                                        <script>
                                                                                            function updateTopicDeatil(Subject, id) {
                                                                                                var inputs = document.getElementsByName('id_' + id);
                                                                                                $.ajax({
                                                                                                    url: '${path}/Admin/Subject/View',
                                                                                                    method: 'POST',
                                                                                                    data: {
                                                                                                        action: inputs[0].value,
                                                                                                        topic_id: inputs[1].value,
                                                                                                        topic_name: inputs[2].value,
                                                                                                        topic_description: inputs[3].value,
                                                                                                        topic_purpose: inputs[4].value
                                                                                                    },
                                                                                                    success: function (data, textStatus, jqXHR) {
                                                                                                        alert("Edited");
                                                                                                    }
                                                                                                });
                                                                                                return false;
                                                                                            }
                                                                                        </script>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--End modal--> 
                                                            </div>

                                                            <div class="col-xm-4">
                                                                <a href="${path}/Admin/Subject/Topic/Question?topic_id=${topic.topic_id}"><button class="btn btn-success">View Question</button></a>


                                                            </div>
                                                        </div>







                                                    </td>
                                                    <td>
                                                        <ol>
                                                            <c:forEach items="${topic.lessons}" var="lesson">
                                                                <li>
                                                                    ${lesson.name}  -  
                                                                    <input type="checkbox" name="" value="ON" style="appearance: checkbox;"  
                                                                           <c:if test="${lesson.status eq 1}">
                                                                               checked
                                                                           </c:if>
                                                                           onclick="updateLessionStatus('${lesson.l_id}', '${lesson.status}', 'status')" /> 
                                                                    Active   -  <button data-toggle="modal" data-target="#editLesson_${lesson.l_id}" style="color: whitesmoke;background-color: #1da1f2; border-style: none; border-radius: 10px;margin: 3px">View</button>
                                                                </li>

                                                                <!--Begin modal - Edit lesson-->
                                                                <div>
                                                                    <div class="modal fade" id="editLesson_${lesson.l_id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                                <form onsubmit=" return updateLessonDetail(this.value, '${lesson.l_id}');">
                                                                                    <div class="modal-header">
                                                                                        <h5 class="modal-title"  >Topic: ${topic.name}</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <!--Begin modal-body-->
                                                                                    <div class="modal-body">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-sm-2 col-form-label">Lesson Name</label>
                                                                                            <div class="col-sm-10">
                                                                                                <input type="hidden" name="l_id_${lesson.l_id}" value="edit" class="form-control d-none">
                                                                                                <input type="text" name="l_id_${lesson.l_id}" value="${lesson.l_id}" class="form-control d-none">
                                                                                                <input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.name}">
                                                                                            </div>
                                                                                            <label class="col-sm-2 col-form-label">No</label>
                                                                                            <div class="col-sm-10"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.no}"></div>

                                                                                            <label class="col-sm-2 col-form-label">Video</label>
                                                                                            <div class="col-sm-8"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.video}" onchange="show('vid', 4,${lesson.l_id})">
                                                                                                <iframe class="embed-responsive-item" id="lesson_vid_${lesson.l_id}" name="Lession video" src="${lesson.video}" allowfullscreen  width="500px" height="300px"  ></iframe>
                                                                                            </div>
                                                                                            <div class="col-sm-2">

                                                                                            </div>

                                                                                            <label class="col-sm-2 col-form-label">Image</label>
                                                                                            <div class="col-sm-8"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.image}" onchange="show('img', 5,${lesson.l_id})">
                                                                                                <img id="lesson_img_${lesson.l_id}" width="500px" height="300px"  src="${lesson.image}"   />
                                                                                                <!--<progress class="col-sm-12" name="processImgSub" style="visibility: visible" value=0 max=100></progress>-->
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="file" id="inputFileSubImg${lesson.l_id}"  name="upload"  style="display: none;"  onchange="uppload(event, 'img', 5,${lesson.l_id}, 'edit')">
                                                                                                <label for="inputFileSubImg${lesson.l_id}" class="btn btn-success">Upload</label>
                                                                                            </div>

                                                                                            <label class="col-sm-2 col-form-label">Content</label>
                                                                                            <div class="col-sm-10"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.content}"></div>

                                                                                            <label class="col-sm-2 col-form-label">Description</label>
                                                                                            <div class="col-sm-10"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.description}"></div>

                                                                                            <label class="col-sm-2 col-form-label">References</label>
                                                                                            <div class="col-sm-10"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.references}"></div>


                                                                                            <label class="col-sm-2 col-form-label">Documents</label>
                                                                                            <div class="col-sm-10"><input type="text" name="l_id_${lesson.l_id}" required class="form-control" value="${lesson.documents}"></div>




                                                                                        </div>



                                                                                    </div>
                                                                                    <!--End modal-body-->
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary" value="${topic}" onclick="">Edit</button>
                                                                                        <script>
                                                                                            function uppload(event, object, index, id, action) {
                                                                                                var show = document.getElementById('lesson_' + object + '_' + id);
//                                                                                                var src = URL.createObjectURL(event.target.files[0]);
                                                                                                var inputs = document.getElementsByName('l_id_' + id);

                                                                                                var colo = inputs[index].style.color;

                                                                                                inputs[index].value = 'Loading...';
                                                                                                inputs[index].style.color = 'red';

                                                                                                if (object === 'img' || object === 'vid') {
                                                                                                    show.src = '';
                                                                                                }

                                                                                                uploadImage(event);

                                                                                                //document.getElementById('lesson_' + object + '_' + id) = src;


//                                                                                                document.getElementById("processImgSub").style.visibility = 'visible';

                                                                                                var timmer = setInterval(function () {
                                                                                                    var upload = document.getElementById('uppload').value;
                                                                                                    if (upload !== '') {
                                                                                                        inputs[index].value = upload;
                                                                                                        inputs[index].style.color = colo;


                                                                                                        if (object === 'img' || object === 'vid') {
                                                                                                            show.src = upload;
                                                                                                        }
//                                                                                                        alert(upload);
                                                                                                        upload = '';
//                                                                                                        upload.clear;
//                                                                                                        upload.clear();


                                                                                                        clearInterval(timmer);
                                                                                                    }
                                                                                                }, 30);

                                                                                            }






                                                                                            function show(object, index, id) {
                                                                                                var inputs = document.getElementsByName('l_id_' + id);
                                                                                                var show = document.getElementById('lesson_' + object + '_' + id);
                                                                                                show.src = inputs[index].value;
//                                                                                                if(index === '4'){
//                                                                                                    show.src = inputs[4].value;
//                                                                                                    
//                                                                                                }else{
//                                                                                                    
//                                                                                                    show.src = inputs[5].value;
//                                                                                                }
//                                                                                               


                                                                                            }
                                                                                            function updateLessonDetail(Subject, id) {
                                                                                                var inputs = document.getElementsByName('l_id_' + id);
                                                                                                $.ajax({
                                                                                                    url: '${path}/Admin/Subject/Topic/Lesson',
                                                                                                    method: 'POST',
                                                                                                    data: {
                                                                                                        action: inputs[0].value,
                                                                                                        lesson_id: inputs[1].value,
                                                                                                        lesson_name: inputs[2].value,
                                                                                                        lesson_no: inputs[3].value,
                                                                                                        lesson_video: inputs[4].value,
                                                                                                        lesson_image: inputs[5].value,
                                                                                                        lesson_content: inputs[6].value,
                                                                                                        lesson_description: inputs[7].value,
                                                                                                        lesson_references: inputs[8].value,
                                                                                                        lesson_documents: inputs[9].value
                                                                                                    }
                                                                                                    ,
                                                                                                    success: function (data, textStatus, jqXHR) {
//                                                                                                        for (var i = 0, max = inputs.length; i < max; i++) {
//                                                                                                            alert(inputs.length);
//                                                                                                        }

                                                                                                        alert("Edited");
                                                                                                        location.reload(true);
                                                                                                    }
                                                                                                });
                                                                                                return false;
                                                                                            }


                                                                                        </script>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--Begin modal-->
                                                            </c:forEach>

                                                        </ol>

                                                        <button class="btn btn-secondary" data-toggle="modal" data-target="#addLesson_${topic.topic_id}">Add Lesson</button>

                                                        <!--Begin modal - Edit lesson-->
                                                        <div>
                                                            <div class="modal fade" id="addLesson_${topic.topic_id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <form onsubmit="return addLessonDetail(${topic.topic_id});">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"  >Topic: ${topic.name}</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <!--Begin modal-body-->
                                                                            <div class="modal-body">
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Lesson Name</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input type="hidden" name="lesson_add_${topic.topic_id}" value="add" class="form-control d-none">
                                                                                        <input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value="">
                                                                                    </div>
                                                                                    <label class="col-sm-2 col-form-label">No</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" ></div>

                                                                                    <label class="col-sm-2 col-form-label">Video</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value="" onchange="showAdd('vid', 3,${topic.topic_id})">
                                                                                        <iframe class="embed-responsive-item" id="lesson_vid_add_${topic.topic_id}" name="Lession video" src="${lesson.video}" allowfullscreen  width="500px" height="300px"  ></iframe>

                                                                                    </div>

                                                                                    <label class="col-sm-2 col-form-label">Image</label>
                                                                                    <div class="col-sm-8"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value="" onchange="showAdd('img', 4,${topic.topic_id})">
                                                                                        <img id="lesson_img_add_${topic.topic_id}" width="500px" height="300px"  src=""   />
                                                                                        <!--<progress class="col-sm-12" name="processImgSub" style="visibility: visible" value=0 max=100></progress>-->
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="file" id="inputFileSubImgAdd${topic.topic_id}"  name="upload"  style="display: none;"  onchange="uppload_Add(event, 'lesson_add_', ${topic.topic_id}, 4)">
                                                                                        <label for="inputFileSubImgAdd${topic.topic_id}" class="btn btn-success">Upload</label>
                                                                                    </div>



                                                                                    <label class="col-sm-2 col-form-label">Content</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value=""></div>

                                                                                    <label class="col-sm-2 col-form-label">Description</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value=""></div>

                                                                                    <label class="col-sm-2 col-form-label">References</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value=""></div>


                                                                                    <label class="col-sm-2 col-form-label">Documents</label>
                                                                                    <div class="col-sm-10"><input type="text" name="lesson_add_${topic.topic_id}" required class="form-control" value=""></div>


                                                                                </div>


                                                                            </div>
                                                                            <!--End modal-body-->
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-primary" value="${topic.topic_id}" >Add</button>
                                                                                <script>

                                                                                    function showAdd(object, index, id) {
                                                                                        var inputs = document.getElementsByName('lesson_add_' + id);
                                                                                        var show = document.getElementById('lesson_' + object + '_add_' + id);
                                                                                        show.src = inputs[index].value;

                                                                                    }

                                                                                    function addLessonDetail(Topic) {
                                                                                        var inputs = document.getElementsByName('lesson_add_' + Topic);
                                                                                        $.ajax({
                                                                                            url: '${path}/Admin/Subject/Topic/Lesson',
                                                                                            method: 'POST',
                                                                                            data: {
                                                                                                topic_id: Topic,
                                                                                                action: inputs[0].value,
                                                                                                lesson_name: inputs[1].value,
                                                                                                lesson_no: inputs[2].value,
                                                                                                lesson_video: inputs[3].value,
                                                                                                lesson_image: inputs[4].value,
                                                                                                lesson_content: inputs[5].value,
                                                                                                lesson_description: inputs[6].value,
                                                                                                lesson_references: inputs[7].value,
                                                                                                lesson_documents: inputs[8].value
                                                                                            },
                                                                                            success: function (data, textStatus, jqXHR) {
//                                                                                                alert("action: " + inputs[0].value + "lessname: " + inputs[1].value + "lessno: " + inputs[2].value + "dcccscas   " + Topic);
                                                                                                alert("Edited");
                                                                                                location.reload(true);
                                                                                            }
                                                                                        });
                                                                                        return false;
                                                                                    }

                                                                                    function uppload_Add(event, name, id, index) {
                                                                                        var show = document.getElementById('lesson_img_add_' + id);
//                                                                                                var src = URL.createObjectURL(event.target.files[0]);
                                                                                        var inputs = document.getElementsByName(name + id);

                                                                                        var colo = inputs[index].style.color;

                                                                                        inputs[index].value = 'Loading...';
                                                                                        inputs[index].style.color = 'red';

                                                                                        show.src = '';


                                                                                        uploadImage(event);

                                                                                        //document.getElementById('lesson_' + object + '_' + id) = src;


//                                                                                                document.getElementById("processImgSub").style.visibility = 'visible';
                                                                                        var upload = document.getElementById('uppload');
                                                                                        var timeer = setInterval(function () {

                                                                                            if (upload.value !== '') {
                                                                                                inputs[index].value = upload.value;
                                                                                                inputs[index].style.color = colo;

//                                                                                                alert(upload.value);
                                                                                                show.src = upload.value;
                                                                                                upload.value = '';
                                                                                                
                                                                                                
                                                                                                clearInterval(timeer);
                                                                                            }
                                                                                        }, 30);

                                                                                    }







                                                                                </script>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--Begin modal-->

                                                        <!--<button onclick="createLession()">Add Lesson</button>-->
                                                        <script>

                                                            function updateLessionStatus(l_id, status, action) {
                                                                $.ajax({
                                                                    url: '${path}/Admin/Subject/Topic/Lesson',
                                                                    method: 'POST',
                                                                    data: {
                                                                        lesson_id: l_id,
                                                                        status: status,
                                                                        action: action
                                                                    },
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        alert("Done");
                                                                    }
                                                                });
                                                            }

                                                        </script>                
                                                    </td>

                                                    <!--<td>{subject.getOrganization()}</td>-->





                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

          

            <input id="uppload" value ="" style="display: none" >
            <script>




                var Url_Upload;
                var upload = new Upload({apiKey: "public_kW15asz9FBowcqDRRtfmv8s8sfST"});
                var uploadImage = upload.createFileInputHandler({

                    // show process of uploading image
                    onProgress: ({ bytesSent, bytesTotal }) => {
                        let loading = document.getElementsByName("processImgSub");

                        for (var i in loading) {
                            i.max = bytesTotal;
                            i.value = bytesSent;
                    }

//                        document.getElementsByName("processImgSub").value = bytesSent;

                    },
                    // upload image successfully
                    onUploaded: ({ fileUrl, fileId}) => {
                        console.log(fileUrl);
//                        document.getElementById("processImgSub").style.visibility = 'hidden';
                        Url_Upload = fileUrl;
//                        alert(fileUrl);
                        document.getElementById('uppload').value = fileUrl;
                    }
                });
            </script>



            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>

    </body>

</html>

<!--
                function previewImg(img_Url, div_preview, img_preview, Object_Attribute, object_id, type) {
                    if (img_Url === '') {
                        document.getElementById(div_preview + Object_Attribute + object_id).style.display = 'none';
                    } else {
                        var preview = document.getElementById(img_preview + Object_Attribute + object_id);
                        preview.src = img_Url;
                        document.getElementById(div_preview + Object_Attribute + object_id).style.display = 'block';
                    }
                }

                function previewAndUpImg(event, div_preview, img_preview, Object_Attribute, object_id, type) {
                    if (event.target.files.length > 0) {
//                        var src = URL.createObjectURL(event.target.files[0]);
                        uploadImage(event);
                        let img_Url = $('#img_onl').val();
                        //document.getElementById("img_onl").value;
//                        if(type === 'img'){
                        previewImg(img_Url, div_preview, img_preview, Object_Attribute, object_id);
//                        }
                        document.getElementById(Object_Attribute, object_id).value = img_Url; //Input_img

                        // loading
//                        document.getElementById(Object_Attribute, object_id).style.visibility = 'visible';


                    }
                }-->