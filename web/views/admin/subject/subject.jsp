<%-- 
    Document   : subject
    Created on : Jun 30, 2022, 8:43:56 AM
    Author     : Phamb
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Subjects" ></c:set>
        <c:set var="subTitle" scope="session" value="Subject List" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!--tham khảo: https://datatables.net --> 
        <script src="https://js.upload.io/upload-js/v1"></script>
        <style>
            body:not(.modal-open){
                padding-right: 0px !important;
            }
        </style>
    </head>

    <body id="page-top" >
        <!--data-new-gr-c-s-check-loaded="14.1017.0" data-gr-ext-installed class="modal-open" style="padding-right:10px;" cái này bị ghi đè khi click modal, ko hiểu tại sao @@--> 
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1>
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Earnings ${sessionScope.totalMoney.getTimeMoney()}</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$${sessionScope.totalMoney.getTotalMoney()}</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    ...</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$...</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">....
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">....</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar"
                                                                 style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                                 aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Pending Requests Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    ....</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">....</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Subject List
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#addNewSlide">New Subject</button>
                                </h6>
                                <!--Begin modal-->
                                <div>
                                    <div class="modal fade" id="addNewSlide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"  >New Subject</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        <input type="text" id="newSlide" value="newSlide" hidden class="form-control">
                                                    </button>
                                                </div>
                                                <form>
                                                    <!--Begin modal-body-->
                                                    <div class="modal-body">
                                                        <!--flag newSub to add new subject-->
                                                        <input type="text" value="newSub" id="newSub" hidden="">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Name</label>
                                                            <div class="col-sm-10">
                                                                <input required type="text" id="newSubName" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Category</label>
                                                            <div class="col-sm-10">
                                                                <select required  class="form-control" id="SubCateSelect" onchange="changeFunc();">
                                                                    <option disabled selected style="color: #004085">Select Category</option>
                                                                    <optgroup value=""  label="New Category">
                                                                        <option  value="" style="color: #0062cc">New SubCategory</option>
                                                                    </optgroup>
                                                                    <c:forEach items="${cats}" var="cats">
                                                                        <optgroup value="${cats.catId}" label="${cats.name}">
                                                                            <c:forEach items="${types}" var="types" varStatus="status">
                                                                                <c:if test="${cats.catId eq types.cat_id}">
                                                                                    <option value="${types.id}">${types.name}</option>
                                                                                </c:if>
                                                                                <c:if test="${status.last}">
                                                                                    <option value="" style="color: blue">New ${cats.name}</option>
                                                                                </c:if>
                                                                            </c:forEach>
                                                                        </optgroup>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="newCate" style="display: none">
                                                            <!--<div id="newCate">-->
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">New Category</label>
                                                                <div class="col-sm-10">
                                                                    <input required type="text" id="newSubCategory" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="newSubCate" style="display: none">
                                                            <!--<div id="newSubCate">-->
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">New SubCategory</label>
                                                                <div class="col-sm-10">
                                                                    <input required type="text" id="newSubSubCat" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Image of Subject</label>
                                                            <div class="col-sm-8">
                                                                <input id="newSubImage" type="text" required class="form-control" onchange="previewImg(this.value, 'Sub')">
                                                                <div class="col-sm-12">
                                                                    <progress class="col-sm-12" id="processImgSub" style="visibility: hidden" value=0 max=100></progress>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-success col-sm-2" style="width: 50%; height: 50%" 
                                                                    onclick="document.getElementById('inputFileSub').click()"
                                                                    >Up File</button>
                                                            <input type="file" id="inputFileSub" accept="image/*" hidden="" onchange="previewAndUpImg(event, 'Sub')">
                                                        </div>
                                                        <div  id="divOfPreviewSub" style="display: none">
                                                            <div class="form-group row" >
                                                                <!--<div class="form-group row">-->
                                                                <label class="col-sm-2 col-form-label">Preview Subject Image</label>
                                                                <div class="col-sm-10">
                                                                    <img class="col-sm-12" id="previewImg_Sub" src="https://s35691.pcdn.co/wp-content/uploads/2015/07/group-work150722.jpg">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Subject Description</label>
                                                            <div class="col-sm-10">
                                                                <textarea style='height: 200px;' type="text" required class="form-control" id="newSubDes" placeholder="Subject Description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Organization</label>
                                                            <div class="col-sm-10">
                                                                <input required type="text" id="newSubOrg" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Image of Organization</label>
                                                            <div class="col-sm-8">
                                                                <input id="newOrgImage" type="text" required class="form-control" onchange="previewImg(this.value, 'Org')">
                                                                <div class="col-sm-12">
                                                                    <progress class="col-sm-12" id="processImgOrg" style="visibility: hidden" value=0 max=100></progress>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-success col-sm-2" style="width: 50%; height: 50%" 
                                                                    onclick="document.getElementById('inputFileOrg').click()"
                                                                    >Up File</button>
                                                            <input type="file" id="inputFileOrg" accept="image/*" hidden="" onchange="previewAndUpImg(event, 'Org')">
                                                        </div>
                                                        <div  id="divOfPreviewOrg" style="display: none">
                                                            <div class="form-group row" >
                                                                <!--<div class="form-group row">-->
                                                                <label class="col-sm-2 col-form-label">Preview Organization Image</label>
                                                                <div class="col-sm-10">
                                                                    <img class="col-sm-12" id="previewImg_Org" src="https://fpt.edu.vn/Content/images/assets/img-logo-fe.png">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">Price</label>
                                                            <div class="col-sm-10">
                                                                <input required type="number" id="newSubPri" class="form-control" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End modal-body-->
                                                    <div class="modal-footer">
                                                        <h6 style="color: green">This page will reload if the subject is added successfully!</h6>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary" onclick="addNewSubject()">Add</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End modal-->
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Organization</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Organization</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${subjectData}" var="subject">
                                                <tr>
                                                    <td>${subject.getS_id()}</td>
                                                    <td>${subject.getName()}</td>
                                                    <td>
                                                        <c:forEach items="${types}" var="type">
                                                            <c:if test="${type.id eq subject.type_id}" >
                                                                ${type.name}
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                    <td>${subject.getOrganization()}</td>
                                                    <td>
                                                        <div>
                                                            <link href="${path}/assetAdmin/css/component-custom-switch.css" rel="stylesheet">
                                                            <link href="${path}/assetAdmin/css/boostrap.css" rel="stylesheet">
                                                            <div class="custom-switch custom-switch-label-status pl-0">
                                                                <c:if test="${subject.getStatus() eq 1}">
                                                                    <input class="custom-switch-input" id="status_${subject.getS_id()}" type="checkbox" 
                                                                           checked value="0" onclick="updateSubjectStatus('${subject.getS_id()}', 0, 'upSta')">
                                                                </c:if>
                                                                <c:if test="${subject.getStatus() ne 1}">
                                                                    <input class="custom-switch-input" id="status_${subject.getS_id()}" type="checkbox" 
                                                                           value="1" onclick="updateSubjectStatus('${subject.getS_id()}', 1, 'upSta')">
                                                                </c:if>
                                                                <label class="custom-switch-btn" for="status_${subject.getS_id()}"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-success"> <a href="${path}/Admin/Subject/View?s_id=${subject.s_id}">View</a></button>
                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#editSubject_${subject.getS_id()}">Edit</button>
                                                        <!--Begin modal-->
                                                        <div>
                                                            <div class="modal fade" id="editSubject_${subject.getS_id()}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <form>
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"  >${subject.getName()}</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <!--Begin modal-body-->
                                                                            <div class="modal-body">
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Name</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input type="text" name="id_${subject.getS_id()}" value="${subject.getS_id()}" class="form-control d-none">
                                                                                        <input type="text" name="id_${subject.getS_id()}" required class="form-control" value="${subject.getName()}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Category</label>
                                                                                    <div class="col-sm-10">
                                                                                        <select required name="id_${subject.getS_id()}" class="form-control"  placeholder="Subject Type">
                                                                                            <c:forEach items="${types}" var="types">
                                                                                                <c:if test="${types.id eq subject.type_id}" >
                                                                                                    <option value="${types.id}" selected>${types.name}</option>
                                                                                                </c:if>
                                                                                                <c:if test="${types.id ne subject.type_id}" >
                                                                                                    <option value="${types.id}" >${types.name}</option>
                                                                                                </c:if>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Subject Description</label>
                                                                                    <div class="col-sm-10">
                                                                                        <textarea style='height: 200px;' type="text" required class="form-control" id="subjectDes" name="id_${subject.getS_id()}" placeholder="Subject Description">${subject.getDescription()}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Organization</label>
                                                                                    <div class="col-sm-10">
                                                                                        <input name="id_${subject.getS_id()}" type="text" required class="form-control" value="${subject.getOrganization()}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-2 col-form-label">Price</label>
                                                                                    <div class="col-sm-10">
                                                                                        <c:forEach items="${prices}" var="prices">
                                                                                            <c:if test="${prices.getS_id() eq subject.getS_id() and prices.getStatus() eq 1}">
                                                                                                <input type="number" name="id_${subject.getS_id()}" value="${prices.getPrice_id()}" class="form-control d-none">
                                                                                                <input name="id_${subject.getS_id()}" type="number" required class="form-control" value="${prices.getPrice()}">
                                                                                            </c:if>
                                                                                        </c:forEach>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--End modal-body-->
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary" value="${subject}" onclick="updateSubjectDeatil(this.value, '${subject.getS_id()}')">Save</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End modal-->                                  
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>
        <script type="text/javascript">
                                                                                    function updateSubjectDeatil(Subject, id) {
                                                                                        var inputs = document.getElementsByName('id_' + id);
                                                                                        var newSub = 'none';
                                                                                        $.ajax({
                                                                                            url: '${path}/Admin/Subject',
                                                                                            method: 'POST',
                                                                                            data: {
                                                                                                newSub: newSub,
                                                                                                sjid: inputs[0].value,
                                                                                                sjname: inputs[1].value,
                                                                                                sjtype: inputs[2].value,
                                                                                                sjdes: inputs[3].value,
                                                                                                sjorga: inputs[4].value,
                                                                                                prid: inputs[5].value,
                                                                                                prprice: inputs[6].value
                                                                                            },
                                                                                            success: function (data, textStatus, jqXHR) {
                                                                                                alert("OK");
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    function updateSubjectStatus(id, status, upSta) {
                                                                                        var newSub = '';
                                                                                        $.ajax({
                                                                                            url: '${path}/Admin/Subject',
                                                                                            method: 'POST',
                                                                                            data: {
                                                                                                newSub: newSub,
                                                                                                sjid: id,
                                                                                                sjstatus: status,
                                                                                                sjupSta: upSta
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    function addNewSubject() {
                                                                                        var newSub = $('#newSub').val();
                                                                                        var newSubName = $('#newSubName').val();
                                                                                        var newSubCategory = $('#newSubCategory').val();
                                                                                        var newSubSubCat = $('#newSubSubCat').val();
                                                                                        var newSubImage = $('#newSubImage').val();
                                                                                        var newSubDes = $('#newSubDes').val();
                                                                                        var newSubOrg = $('#newSubOrg').val();
                                                                                        var newSubImageOrg = $('#newOrgImage').val();
                                                                                        var newSubPri = $('#newSubPri').val();

                                                                                        $.ajax({
                                                                                            url: '${path}/Admin/Subject',
                                                                                            method: 'POST',
                                                                                            data: {
                                                                                                newSub: newSub,
                                                                                                newSubName: newSubName,
                                                                                                newSubCategory: newSubCategory,
                                                                                                newSubSubCat: newSubSubCat,
                                                                                                newSubImage: newSubImage,
                                                                                                newSubDes: newSubDes,
                                                                                                newSubOrg: newSubOrg,
                                                                                                newSubImageOrg: newSubImageOrg,
                                                                                                newSubPri: newSubPri
                                                                                            },
                                                                                            success: function () {
                                                                                                alert("ok");
//                                                                                                location.replace("${path}/Admin/Subject");
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    var upload = new Upload({apiKey: "public_kW15asz9FBowcqDRRtfmv8s8sfST"});
                                                                                    var uploadFileSub = upload.createFileInputHandler({
                                                                                        // show process of uploading image
                                                                                        onProgress: ({ bytesSent, bytesTotal }) => {
                                                                                            document.getElementById("processImgSub").max = bytesTotal;
                                                                                            document.getElementById("processImgSub").value = bytesSent;
                                                                                        },
                                                                                        // upload image successfully
                                                                                        onUploaded: ({ fileUrl, fileId}) => {
                                                                                            console.log(fileUrl);
                                                                                            document.getElementById("processImgSub").style.visibility = 'hidden';
                                                                                            document.getElementById("newSubImage").value = fileUrl;
                                                                                        }
                                                                                    });
                                                                                    var uploadFileOrg = upload.createFileInputHandler({
                                                                                        // show process of uploading image
                                                                                        onProgress: ({ bytesSent, bytesTotal }) => {
                                                                                            document.getElementById("processImgOrg").max = bytesTotal;
                                                                                            document.getElementById("processImgOrg").value = bytesSent;
                                                                                        },
                                                                                        // upload image successfully
                                                                                        onUploaded: ({ fileUrl, fileId}) => {
                                                                                            console.log(fileUrl);
                                                                                            document.getElementById("processImgOrg").style.visibility = 'hidden';
                                                                                            document.getElementById("newOrgImage").value = fileUrl;
                                                                                        }
                                                                                    });

                                                                                    function previewAndUpImg(event, kind) {
                                                                                        if (event.target.files.length > 0) {
                                                                                            var src = URL.createObjectURL(event.target.files[0]);
                                                                                            var preview = document.getElementById("previewImg_" + kind);
                                                                                            preview.src = src;
                                                                                            if (kind === 'Sub') {
                                                                                                document.getElementById("processImgSub").style.visibility = 'visible';
                                                                                                uploadFileSub(event);
                                                                                                document.getElementById("divOfPreview" + kind).style.display = 'block';
                                                                                            } else {
                                                                                                document.getElementById("processImgOrg").style.visibility = 'visible';
                                                                                                uploadFileOrg(event);
                                                                                                document.getElementById("divOfPreview" + kind).style.display = 'block';
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    function previewImg(src, kind) {
                                                                                        if (src === '') {
                                                                                            document.getElementById("divOfPreview" + kind).style.display = 'none';
                                                                                        } else {
                                                                                            var preview = document.getElementById("previewImg_" + kind);
                                                                                            preview.src = src;
                                                                                            document.getElementById("divOfPreview" + kind).style.display = 'block';
                                                                                        }
                                                                                    }

                                                                                    function changeFunc() {
                                                                                        var gr = $('select option:selected').closest('optgroup').attr('value');
                                                                                        var newCate = document.getElementById("newCate");
                                                                                        document.getElementById("newSubCategory").value = gr;
                                                                                        console.log('gr: ' + gr);
                                                                                        var op = $('select option:selected').closest('option').attr('value');
                                                                                        var newSubCate = document.getElementById("newSubCate");
                                                                                        document.getElementById("newSubSubCat").value = op;
                                                                                        console.log('op: ' + op);
                                                                                        if (gr === '') {
                                                                                            newCate.style.display = "block";
                                                                                        } else {
                                                                                            newCate.style.display = "none";
                                                                                        }
                                                                                        if (op === '') {
                                                                                            newSubCate.style.display = "block";
                                                                                        } else {
                                                                                            newSubCate.style.display = "none";
                                                                                        }
                                                                                    }
        </script>
    </body>
</html>

