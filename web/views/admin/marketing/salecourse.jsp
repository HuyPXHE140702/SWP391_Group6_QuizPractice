<%-- 
    Document   : salecourse
    Created on : Jul 17, 2022, 8:43:56 AM
    Author     : Phamb
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <c:set var="title" scope="session" value="Marketing" ></c:set>
        <c:set var="subTitle" scope="session" value="Sale Course" ></c:set>

            <title>
            <c:out value="${title}" ></c:out> - <c:out value="${subTitle}" ></c:out>
            </title>
            <!-- Custom fonts for this template-->
                <link href="${path}/assetAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${path}/assetAdmin/css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Custom styles for this page -->
        <link href="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <!--tham khảo: https://datatables.net --> 
        <script src="https://js.upload.io/upload-js/v1"></script>
        <link href="${path}/assetAdmin/css/boostrap.css" rel="stylesheet">
        <style>
            body:not(.modal-open){
                padding-right: 0px !important;
            }
        </style>
    </head>

    <body id="page-top" >
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <jsp:include page="/views/admin/sidebar.jsp"></jsp:include>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                    <jsp:include page="/views/admin/header.jsp"></jsp:include>
                        <!-- End of Topbar -->

                        <!-- Begin Page Content -->
                        <div class="container-fluid">

                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">${title} - ${subTitle}</h1><br>
                            <h3 class="h4 mb-0 text-gray-800">
                                Notify users about the course sale event by sending an email
                                <button class="btn btn-success" data-toggle="modal" data-target="#addNotify">Notify</button>
                            </h3>
                            <h4 id="notifySuccessSendEmail" style="color: #007aff;">Status&emsp;</h4>
                            <!--Begin modal-->
                            <div>
                                <div class="modal fade" id="addNotify" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form>
                                                <div class="modal-header">
                                                    <h5 class="modal-title">New Notify</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <!--Begin modal-body-->
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Template used</label>
                                                        <div class="col-sm-10">
                                                            <select id="selectid" type="text" class="form-control" onchange="chooseEmail(this.value)">
                                                                <option value="newEmail" selected="">New</option>
                                                                <c:forEach items="${listMailSent}" var="ms">
                                                                    <option value="${ms.eid}">${ms.ediscription}</option>
                                                                    <c:set value="${ms.eid}" var="eid"></c:set>
                                                                    <!--<input type="text" id="subjectT_${ms.eid}" hidden="">-->
                                                                </c:forEach>
                                                            </select >
                                                            <script>
                                                                function chooseEmail(eid) {
                                                                    $.ajax({
                                                                        url: '${path}/Admin/MailTemplateController',
                                                                        method: 'POST',
                                                                        data: {
                                                                            eid: eid
                                                                        },
                                                                        success: function changeInput(responseText) {
                                                                            const myArray = responseText.split("<break>");
                                                                            document.getElementById('newNotifySubject').value = myArray[0];
                                                                            document.getElementById('newNotifyContent').value = myArray[1];
                                                                            document.getElementById('newNotifyDiscription').value = myArray[2];
                                                                        }
                                                                    });
                                                                    
                                                                }
                                                            </script>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Subject</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="newNotifySubject" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Content</label>
                                                        <div class="col-sm-10">
                                                            <textarea style='height: 200px;' type="text" id="newNotifyContent" required class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Description</label>
                                                        <div class="col-sm-10">
                                                            <textarea style='height: 100px;' type="text" id="newNotifyDiscription" required class="form-control" placeholder="Describe this email for future use"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End modal-body-->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick="addNewNotify()" data-dismiss="modal">Notify</button>
                                                    <script>
                                                        function addNewNotify() {
                                                            document.getElementById("notifySuccessSendEmail").innerHTML = "Sending";
                                                            var eid = $('#selectid').val();
                                                            var nsubject = $('#newNotifySubject').val();
                                                            var ncontent = $('#newNotifyContent').val();
                                                            var ndiscription = $('#newNotifyDiscription').val();
                                                            $.ajax({
                                                                url: '${path}/AdminSendMailController',
                                                                method: 'POST',
                                                                data: {
                                                                    eid: eid,
                                                                    nsubject: nsubject,
                                                                    ncontent: ncontent,
                                                                    ndiscription: ndiscription
                                                                },
                                                                success: function () {
                                                                    //location.replace("${path}/Admin/SaleCourse");
                                                                    document.getElementById("notifySuccessSendEmail").innerHTML = "Successfully!";
                                                                }
                                                            });
                                                        }
                                                    </script>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End modal--> 
                        </div>
                        <!-- Content Row -->
                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Earnings ${totalMoney.getTimeMoney()}</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$${totalMoney.getTotalMoney()}</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    ...</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">$...</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">....
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">....</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar"
                                                                 style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                                 aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Pending Requests Card Example -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    ....</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">....</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Subject List</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Category</th>
                                                <th>SubCategory</th>
                                                <th>Subject</th>
                                                <th>Price</th>
                                                <th>Sale</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Category</th>
                                                <th>SubCategory</th>
                                                <th>Subject</th>
                                                <th>Price</th>
                                                <th>Sale</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <script>
                                            function saveSales(cat_id) {
                                                var prices = document.getElementsByName('subP_' + cat_id);
                                                var sales = document.getElementsByName('subHasCate_' + cat_id);
                                                var subjects = document.getElementsByName('subID_' + cat_id);
                                                document.getElementById("saleButton_" + cat_id).style.display = "block";
                                                document.getElementById("saveButton_" + cat_id).style.display = "none";
                                                document.getElementById('saleNumber_' + cat_id).style.visibility = "hidden";
                                                for (var i = 0; i < prices.length; i++) {
                                                    console.log(
                                                            i + ": subId: " + subjects[i].value
                                                            + " : prices: " + prices[i].value
                                                            + " : sales: " + sales[i].value);
                                                    saveSale(subjects[i].value);
                                                }
                                            }
                                        </script>
                                        <!--for Each of categories-->
                                        <c:forEach items="${cats}" var="c">
                                            <tbody>
                                                <tr>
                                                    <td rowspan="100">${c.catId}</td>
                                                    <td rowspan="100">${c.catId}: ${c.name} <br>
                                                        <!--<button class="btn btn-primary" data-toggle="modal" data-target="#saleCategory_${c.catId}">Sale</button>-->
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <button id="saleButton_${c.catId}" class="btn btn-primary" onclick="document.getElementById('saleNumber_${c.catId}').style.visibility = 'visible'">Sale</button>
                                                            <button id="saveButton_${c.catId}" class="btn btn-success" style="display: none" onclick="saveSales('${c.catId}')">Save</button>
                                                            <input id="saleNumber_${c.catId}" class="form-control col-sm-8" min="0" max="100" type="number" style="visibility: hidden" onchange="changSaleFollow(this.value, '${c.catId}')">
                                                        </div>
                                                    </td>
                                                    <!--for Each of subcategory ( type)-->
                                                    <c:forEach items="${c.types}" var="t">
                                                        <c:if test="${t.num_of_sub eq 0}">
                                                            <td rowspan="1">
                                                                ${t.id}: ${t.name}
                                                            </td>
                                                            <td>&emsp;</td>
                                                            <td>&emsp;</td>
                                                            <td>&emsp;</td>
                                                            <td>&emsp;</td>
                                                        </tr><tr>
                                                        </c:if>
                                                        <c:if test="${t.num_of_sub ne 0}">
                                                            <td rowspan="${t.num_of_sub}">
                                                                ${t.id}: ${t.name}
                                                            </td>
                                                            <!--for Each of subject-->
                                                            <c:forEach items="${t.subjects}" var="s">
                                                                <td>
                                                                    ${s.getS_id()}: ${s.getName()} <br>
                                                                    <input name="subID_${c.catId}" style="display: none" value="${s.getS_id()}">
                                                                </td>
                                                                <!--Price-->
                                                                <td class="col-sm-1">
                                                                    <input type="number" min="0" class="form-control" name="subP_${c.catId}" 
                                                                           id="subPrice_${s.getS_id()}" value="${s.getPrice()}" onchange="calSale('${s.getS_id()}')">
                                                                </td>
                                                                <!--Sale-->
                                                                <td class="col-sm-2">
                                                                    <div class="d-flex justify-content-between align-items-center">
                                                                        <input type="number" min="0" max="100" class="form-control col-sm-5" value="${s.discount}" id="subSale_${s.getS_id()}"
                                                                               name="subHasCate_${c.catId}" onchange="calSale('${s.getS_id()}')">
                                                                        <span>=</span>
                                                                        <c:if test="${s.discount ne 0}">
                                                                            <input class="form-control col-sm-5" readonly="" name="subA_${c.catId}" id="priceAfterSale_${s.getS_id()}" value="${s.getPrice() -(s.getPrice()*s.discount)/100}">
                                                                        </c:if>
                                                                        <c:if test="${s.discount eq 0}">
                                                                            <input class="form-control col-sm-5" readonly="" name="subA_${c.catId}" id="priceAfterSale_${s.getS_id()}" value="0">
                                                                        </c:if>
                                                                    </div>
                                                                </td>
                                                                <!--Action-->
                                                                <td class="col-sm-1">
                                                                    <button id="saveSaleSub_${s.getS_id()}" class="btn btn-success" onclick="saveSale('${s.getS_id()}')" disabled="">Save</button>
                                                                </td>
                                                            </tr><tr>
                                                            </c:forEach>
                                                        </c:if>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </c:forEach>
                                        <script>
                                            function saveSale(sub_id) {
                                                var price = document.getElementById("subPrice_" + sub_id).value;
                                                var sale = document.getElementById("subSale_" + sub_id).value;
                                                $.ajax({
                                                    url: '${path}/Admin/SaleCourse',
                                                    method: 'POST',
                                                    data: {
                                                        sub_id: sub_id,
                                                        price: price,
                                                        sale: sale
                                                    },
                                                    success: function () {
                                                        console.log("sub_id: " + sub_id + ": ok");
                                                    }
                                                });
                                            }
                                            function changSaleFollow(value, cat_id) {
                                                document.getElementById("saleButton_" + cat_id).style.display = "none";
                                                document.getElementById("saveButton_" + cat_id).style.display = "block";
                                                var inputs = document.getElementsByName('subHasCate_' + cat_id);
                                                var prices = document.getElementsByName('subP_' + cat_id);
                                                var afters = document.getElementsByName('subA_' + cat_id);
                                                for (var i = 0; i < inputs.length; i++) {
                                                    afters[i].value = Math.round(prices[i].value - (prices[i].value * value) / 100);
                                                    inputs[i].value = value;
                                                }
                                            }
                                            
                                            function calSale(id) {
                                                let price = document.getElementById('subPrice_' + id).value;
                                                let discount = document.getElementById('subSale_' + id).value;
                                                let after = Math.round((price - (price * discount) / 100));
                                                document.getElementById('priceAfterSale_' + id).value = after;
                                                document.getElementById('saveSaleSub_' + id).disabled = false;
                                            }
                                        </script>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <jsp:include page="/views/admin/footer.jsp"></jsp:include>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Bootstrap core JavaScript-->
            <script src="${path}/assetAdmin/vendor/jquery/jquery.min.js"></script>
        <script src="${path}/assetAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${path}/assetAdmin/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${path}/assetAdmin/js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="${path}/assetAdmin/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="${path}/assetAdmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${path}/assetAdmin/js/demo/datatables-demo.js"></script>
    </body>
</html>
