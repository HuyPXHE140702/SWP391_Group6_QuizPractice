<%-- 
    Document   : footer
    Created on : Jun 17, 2022, 5:38:18 PM
    Author     : Phamb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Footer</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </body>
</html>
