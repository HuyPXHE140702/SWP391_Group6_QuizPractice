<%-- 
    Document   : doQuestion
    Created on : Jun 30, 2022, 2:33:58 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ezuni</title>
        <meta name="description" content="Free Bootstrap Theme by uicookies.com">
        <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="${path}/ViewsDA/css/styles-merged.css">
        <link rel="stylesheet" href="${path}/ViewsDA/css/style.min.css">
        <link rel="stylesheet" href="${path}/ViewsDA/css/custom.css">

        <!--[if lt IE 9]>
          <script src="js/vendor/html5shiv.min.js"></script>
          <script src="js/vendor/respond.min.js"></script>
        <![endif]-->

        <style>

            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;400&display=swap');



            .quiz-container {
                background-color: #fff;
                border-radius: 10px;
                box-shadow: 0 0 10px 2px rgba(100, 100, 100, 0.1);
                width: 600px;
                overflow: hidden;
                box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
                box-shadow: rgba(0, 0, 0, 0.15) 0px 5px 15px 0px;
            }

            .quiz-header {
                padding: 4rem;
            }

            h2 {
                padding: 1rem;
                text-align: center;
                margin: 0;
            }

            ul {
                list-style-type: none;
                padding: 0;
            }

            ul li {
                font-size: 1.2rem;
                margin: 1rem 0;
            }

            ul li label {
                cursor: pointer;
            }

            button {
                background-image: linear-gradient(to right, #ff5f6d     , #ffc371);
                color: #fff;
                border: none;
                display: block;
                width: 100%;
                cursor: pointer;
                font-size: 1.2rem;
                font-family: inherit;
                padding: 1.3rem;
                font-weight: bold;
            }

            button:hover {
                background-color: #732d91;
            }

            button:focus {
                outline: none;
                background-color: #5e3370;
            }

        </style>

    </head>
    <body>



        <section class="probootstrap-section">
            <div class="row">
                <a class="col-md-2 answer" href="${path}/LessonController?subID=${requestScope.subject}" onclick="return confirm('If you get out, your practice will lost \n Do you want to continue?')" style="text-decoration: none;color: white">
                    <div  style="display: flex ;align-items: center;width: 225px; height: 70px; background-image: linear-gradient(to right, #ff5f6d , #ffc371); border-bottom-right-radius: 50px;  border-top-right-radius: 50px ">
                        <button class="btn-group" style="font-size: large; justify-content: center; background: no-repeat"  >Exit</button>        
                    </div>
                </a>
                    
                    <div  class="col-md-1"">
                </div>
                <div class="col-md-8" style="display: flex ;align-items: center;">
                    <div class="center-block" style="font-size: large; justify-content: center; font-weight: bold">
                        <!--<span id="h">Giờ</span> :<span id="m">Phút</span> :<span id="s">Giây</span>-->
                        <input type="text" name="time" value="" id="countdown"/>


                    </div>

                </div>

                <div  class="col-md-1 d-none" style="display: flex ;align-items: center;width: 225px; height: 70px; background-image: linear-gradient(to right, #ff5f6d , #ffc371); border-bottom-left-radius: 50px;  border-top-left-radius: 50px ">
                    <button  class="btn-group" style="font-size: large; justify-content: center; background: no-repeat" >Exit</button>           
                </div>

            </div>

        </section>
        <form action="${path}/doQuestion?topic_id=${requestScope.topic}" method="Post" id = "myform">
            <input type="hidden" name="time" value="" id="time"/>
            <input type="hidden" name="u_id" value="${sessionScope.user.id}" />

            <!--Quizz-->
            <!--Quizz-List-->

            <%int i = 1;%>
            <c:forEach var="q" items="${requestScope.questions}">

                <div class="quiz-container center-block" id="quiz">
                    <div class="quiz-header">
                        <h2 id="question"><%=i%>) ${q.question}</h2>

                        <ul>

                            <c:forEach var="o" items="${q.questionChoices}">
                                <li>
                                    <input type="radio" name="answer${q.id}" required="" class="answer" value="${o.option}">
                                    <label for="a" id="a_text">${o.option}</label>
                                </li>
                            </c:forEach>

                        </ul>
                    </div>
                    <button ></button>
                </div>
                <br/><br/><br/>
                <%i = i + 1;%>

            </c:forEach>
            <!--Quizz-->
            <div class="quiz-header center-block" style="display: flex ;align-items: center;width: 300px; height: 30px; background-image: linear-gradient(to right, #ff5f6d , #ffc371); border-radius: 50px ">

                <button type="submit" style="font-size: large; justify-content: center; background: no-repeat">Submit</button>
            </div>
        </form>

        <br/><br/><br/>

    </section>
    <div class="d-none">
    <jsp:include page="../views/footer.jsp"></jsp:include> 

    </div>
    
    <!-- END wrapper -->

    <script>


    </script>
    <script src="${path}/ViewsDA/js/scripts.min.js"></script>
<script src="${path}/ViewsDA/js/main.min.js"></script>
<script src="${path}/ViewsDA/js/custom.js"></script>

<script>

        let h1 = null;
        let m1 = null;
        let s1 = null;


        var h = null; // Giờ
        var m = null; // Phút
        var s = null; // Giây

        var timeout = null; // Timeout

        function stop() {
            clearTimeout(timeout);
        }
        function start()
        {
            /*BƯỚC 1: LẤY GIÁ TRỊ BAN ĐẦU*/
            if (h === null)
            {
                h = 0;
                m = 30;
                s = 00;
            }

            /*BƯỚC 1: CHUYỂN ĐỔI DỮ LIỆU*/
            // Nếu số giây = -1 tức là đã chạy ngược hết số giây, lúc này:
            //  - giảm số phút xuống 1 đơn vị
            //  - thiết lập số giây lại 59
            if (s === -1) {
                m -= 1;
                s = 59;
            }

            // Nếu số phút = -1 tức là đã chạy ngược hết số phút, lúc này:
            //  - giảm số giờ xuống 1 đơn vị
            //  - thiết lập số phút lại 59
            if (m === -1) {
                h -= 1;
                m = 59;
            }

            // Nếu số giờ = -1 tức là đã hết giờ, lúc này:
            //  - Dừng chương trình
            if (h == -1) {
                clearTimeout(timeout);
                alert('Time Out');


                document.getElementById('myform').submit();

                return false;
            }

            //////////////////////////////////////////

            if (s1 === null)
            {
                h1 = 0;
                m1 = 0;
                s1 = 0;
            }

            if (s1 === 61) {
                m1 += 1;
                s1 = 00;
            }

            if (m1 === 61) {
                h1 += 1;
                m1 = 0;
            }


            //set time
            document.getElementById('countdown').value = h.toString() + ':' + m.toString() + ':' + s.toString();
            document.getElementById('time').value = h1.toString() + ':' + m1.toString() + ':' + s1.toString();
            /////////////////////////////////////////////

            /*BƯỚC 1: HIỂN THỊ ĐỒNG HỒ*/
//        document.getElementById('h').innerText = h.toString();
//        document.getElementById('m').innerText = m.toString();
//        document.getElementById('s').innerText = s.toString();



            /*BƯỚC 1: GIẢM PHÚT XUỐNG 1 GIÂY VÀ GỌI LẠI SAU 1 GIÂY */
            timeout = setTimeout(function () {
                s--;
                s1++;
                start();
            }, 1000);
        }


        window.onload = function ()
        {
            start();
        };
</script>

</body>
</html>
