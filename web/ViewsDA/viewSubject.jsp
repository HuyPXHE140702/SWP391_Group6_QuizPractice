<%-- 
    Document   : myRegistrations
    Created on : Jun 27, 2022, 10:17:33 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ezuni</title>
        <meta name="description" content="Free Bootstrap Theme by uicookies.com">
        <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="${path}/ViewsDA/css/styles-merged.css">
        <link rel="stylesheet" href="${path}/ViewsDA/css/style.min.css">
        <link rel="stylesheet" href="${path}/ViewsDA/css/custom.css">

        <!--[if lt IE 9]>
          <script src="js/vendor/html5shiv.min.js"></script>
          <script src="js/vendor/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <section class="probootstrap-section probootstrap-section-sm">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <nav>     
                            <div class="row">
                                <div class="col-md-1" >
                                    <a class="" href="${path}/Admin/Subject" style=" margin-bottom: 20px; display: block; width: 20px"> <button class="btn-close" style=""></button></a>
                                </div>
                                <div class="col-md-11">




                                </div>
                            </div>

                        </nav>

                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <form action="${path}/Admin/Subject/View" method="post">
                        <input type="hidden" name="action" value="add" />
                        <input type="hidden" name="s_id" value="${requestScope.subject.s_id}" />
                        <a> 
                            <button type="submit" class="" style="width: 70px; height: 30px; background-color: #71d8ea; border-radius: 5px"> 
                                <h5 style="font-weight: bold;">Add Topic </h5>
                            </button> 
                        </a>
                    </form>
                </div>
                </br>

                <script>
                    function submitOnchange(){
                        if( confirm('Do you want re-name Topic?') == true){
                            this.form.submit();
                        }
                    }
                    
                </script>
                
                <c:forEach var="t" items="${requestScope.subject.topics}">
                    <div class="row" style="border: 5px inset #71d8ea">
                        <div class="col-md-3">
                            <form action="${path}/Admin/Subject/View" method="post"  >
                                <input type="hidden" name="action" value="edit" />
                                <input type="hidden" name="topic_id" value="${t.topic_id}" />
                                <input type="text" name="topic_name" value="${t.name}" style="font-size: large; font-weight: inherit;" onchange="submit()" />
                                <!--<h3>{t.name}</h3>-->
                            </form>
                            
                            

                                <a href="${path}/Admin/Subject/Topic/Question"><p>View Question </p></a> 
                        </div>
                        <div class="col-md-7">
                            <p>Add Lesson</p>
                            <ol>
                                <c:forEach var="l" items="${t.lessons}">
                                    <li>
                                        <table >

                                            <tbody>

                                                <tr>
                                                    <td style="width: 1000px"> <a href="${path}/Admin/Subject/Topic/Lesson"> ${l.name} </a>  </td>
                                                    <td style="width: 20%"> 
                                                        <input type="checkbox" name="" value="ON" style="appearance: checkbox;"/>
                                                        Status
                                                    <td style="width: 30%"> Delete</td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </c:forEach>

                            </ol>
                        </div>
                        <div class="col-md-2">
                            <div class="row center">

                                <div class="col-md-6 center" style="margin-top: 15%; " > 
                                    <form action="${path}/Admin/Subject/View" method="POST">
                                        <input type="hidden" name="action" value="status" />
                                        <input type="hidden" name="topic_id" value="${t.topic_id}" />
                                        <input type="checkbox" name="status" value="ON" 
                                               <c:if test="${t.status==1}">
                                                   checked
                                               </c:if>
                                               style="appearance: checkbox;" 
                                               onchange="form.submit()" 
                                               /> 
                                        <span style="font-weight: bolder; s">status</span>
                                    </form>



                                </div>
                                <div class="col-md-6 center" >delete</div>
                            </div >

                        </div>
                    </div>
                    </br>
                    </br>
                    </br>
                </c:forEach>
            </div>

        </section>



        <section class="probootstrap-section probootstrap-section-sm">
            <div class="container">
                <div class="row">

                    <!--                                <div class="col-md-4" id="probootstrap-sidebar">
                                                        <div class="probootstrap-animate"><>
                                                        <div class="probootstrap-sidebar-inner probootstrap-overlap probootstrap-animate">
                                                            <h3> All Topic :</h3>
                                                            <ul class="probootstrap-side-menu">
                                                                <li class="active"><a>Chemical Engineering</a></li>
                                                                <li><a href="#">Application Design</a></li>
                                                                <li><a href="#">Math Major</a></li>
                                                                <li><a href="#">English Major</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-md-push-1  probootstrap-animate" id="probootstrap-content">
                                                        <h2>Description</h2>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime rerum possimus maiores, natus fugiat quibusdam dolorem. Dolore, odit ipsum, commodi distinctio repellendus vel tempore accusamus ea assumenda totam aut tempora. Illum sed harum, doloremque magnam nostrum. Fuga tempora corrupti unde ratione, placeat voluptates dicta asperiores est, ad voluptatem dolor, delectus sapiente voluptatum! Rerum magnam ducimus voluptas dolorum consectetur ex facilis quaerat sapiente eaque consequuntur perspiciatis nesciunt tenetur quibusdam corrupti voluptates consequatur repudiandae, quisquam aperiam veniam. Architecto voluptas blanditiis provident modi cumque a eius nam dignissimos numquam ducimus earum odit ipsam, reiciendis assumenda id quasi dolore, totam impedit molestias? Alias, ipsa!</p>
                                                        <p>Velit natus alias eligendi architecto rem, itaque distinctio? Excepturi obcaecati fuga ratione. Dolore in ipsam rem ullam nemo error aperiam dolores eius, doloremque blanditiis placeat fugiat libero id atque, recusandae, voluptate laboriosam, distinctio omnis ab. Eius obcaecati, laudantium ex voluptatum voluptas, cum vitae molestiae quam est omnis nulla, quis velit? Esse natus tempore molestias deserunt, tempora illum labore, fuga animi totam dolore doloribus doloremque laudantium velit distinctio, non cumque dolorem delectus quia quibusdam? Amet, consectetur. Reprehenderit unde, eveniet temporibus, quae possimus magni. Dolore quo eveniet, distinctio dicta quisquam? Aliquid libero dolore provident, beatae odit facilis quasi dolorum sit ad minus!</p>
                                                        <p>Tenetur, soluta natus porro, cumque vitae, iste accusamus beatae repudiandae quidem magnam sunt, nihil reprehenderit facere eveniet non sint aliquid. Ipsam totam nostrum, modi veritatis quidem maiores dolor minima incidunt non ratione qui quas, quasi aut sit obcaecati iure. Corporis, rem? Reprehenderit sequi, tenetur, libero nobis beatae ut doloremque obcaecati ipsa? Itaque numquam ratione et veniam porro magnam expedita ad laboriosam dolorum debitis omnis mollitia id eum minus necessitatibus ipsam consequuntur sed tempora assumenda, repudiandae culpa atque? Ad ut porro praesentium, earum voluptatum maxime, deserunt, omnis dolorem veniam cumque et officia veritatis, nam perferendis ex facilis sit sapiente at accusamus?</p>
                                                        <p><a href="#" class="btn btn-primary">Enroll with this course now</a> <span class="enrolled-count">2,928 students enrolled</span></p>
                                                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<jsp:include page="../views/footer.jsp"></jsp:include>

    <!--                <section class="probootstrap-cta">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Get your admission now!</h2>
                                    <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Enroll</a>
                                </div>
                            </div>
                        </div>
                    </section>-->
    <!--                <footer class="probootstrap-footer probootstrap-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="probootstrap-footer-widget">
                                        <h3>About The School</h3>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro provident suscipit natus a cupiditate ab minus illum quaerat maxime inventore Ea consequatur consectetur hic provident dolor ab aliquam eveniet alias</p>
                                        <h3>Social</h3>
                                        <ul class="probootstrap-footer-social">
                                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                                            <li><a href="#"><i class="icon-github"></i></a></li>
                                            <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                            <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                            <li><a href="#"><i class="icon-youtube"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-push-1">
                                    <div class="probootstrap-footer-widget">
                                        <h3>Links</h3>
                                        <ul>
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">Courses</a></li>
                                            <li><a href="#">Teachers</a></li>
                                            <li><a href="#">News</a></li>
                                            <li><a href="#">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="probootstrap-footer-widget">
                                        <h3>Contact Info</h3>
                                        <ul class="probootstrap-contact-info">
                                            <li><i class="icon-location2"></i> <span>198 West 21th Street, Suite 721 New York NY 10016</span></li>
                                            <li><i class="icon-mail"></i><span>info@domain.com</span></li>
                                            <li><i class="icon-phone2"></i><span>+123 456 7890</span></li>
                                        </ul>
                                    </div>
                                </div>
    
                            </div>
                             END row 
    
                        </div>
    
                        <div class="probootstrap-copyright">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 text-left">
                                        <p>&copy; 2017 <a href="https://uicookies.com/">uiCookies:Enlight</a>. All Rights Reserved. Designed &amp; Developed with <i class="icon icon-heart"></i> by <a href="https://uicookies.com/">uicookies.com</a></p>
                                    </div>
                                    <div class="col-md-4 probootstrap-back-to-top">
                                        <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>-->

</div>
<!-- END wrapper -->


<script src="${path}/ViewsDA/js/scripts.min.js"></script>
<script src="${path}/ViewsDA/js/main.min.js"></script>
<script src="${path}/ViewsDA/js/custom.js"></script>

</body>
</html>