USE [QuizPractice]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[blog_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](500) NOT NULL,
	[typeID] [int] NULL,
	[author] [int] NOT NULL,
	[time] [smalldatetime] NOT NULL,
	[type] [nvarchar](50) NOT NULL,
	[status] [smallint] NOT NULL,
	[content] [text] NOT NULL,
	[modifier] [int] NULL,
	[description] [text] NULL,
	[image] [nvarchar](300) NULL,
 CONSTRAINT [PK_198] PRIMARY KEY CLUSTERED 
(
	[blog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[c_id] [int] IDENTITY(1,1) NOT NULL,
	[u_id] [int] NOT NULL,
	[s_id] [int] NOT NULL,
	[received_day] [smalldatetime] NOT NULL,
	[status] [smallint] NOT NULL,
	[result] [float] NOT NULL,
	[cer_image] [nvarchar](300) NULL,
	[description] [nvarchar](500) NULL,
	[content] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_370] PRIMARY KEY CLUSTERED 
(
	[c_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Email]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email](
	[eid] [int] IDENTITY(1,1) NOT NULL,
	[esubject] [nvarchar](500) NOT NULL,
	[econtent] [nvarchar](max) NOT NULL,
	[ediscription] [nvarchar](500) NOT NULL,
	[ecreateTime] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED 
(
	[eid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exam](
	[exam_id] [int] NOT NULL,
	[u_id] [int] NOT NULL,
	[s_id] [int] NOT NULL,
	[level] [nvarchar](50) NULL,
	[created_date] [smalldatetime] NOT NULL,
	[number_of_question] [int] NOT NULL,
	[status] [smallint] NOT NULL,
	[pass_rate] [smallint] NOT NULL,
	[duration] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[exam_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lesson](
	[l_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](500) NOT NULL,
	[no] [int] NOT NULL,
	[video] [nvarchar](300) NULL,
	[image] [nvarchar](300) NULL,
	[content] [text] NOT NULL,
	[description] [text] NULL,
	[status] [smallint] NOT NULL,
	[references] [nvarchar](500) NULL,
	[documents] [nvarchar](500) NULL,
	[topic_id] [int] NOT NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_414] PRIMARY KEY CLUSTERED 
(
	[l_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LessonTopic]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LessonTopic](
	[name] [nvarchar](500) NOT NULL,
	[s_id] [int] NOT NULL,
	[description] [nvarchar](500) NULL,
	[purpose] [nvarchar](500) NULL,
	[status] [smallint] NOT NULL,
	[topic_id] [int] IDENTITY(1,1) NOT NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_373] PRIMARY KEY CLUSTERED 
(
	[topic_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nchar](30) NOT NULL,
	[Description] [nchar](225) NOT NULL,
	[Time] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PricePackage]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PricePackage](
	[price_id] [int] IDENTITY(1,1) NOT NULL,
	[s_id] [int] NOT NULL,
	[price] [int] NOT NULL,
	[status] [smallint] NOT NULL,
	[currency_unit] [nvarchar](50) NOT NULL,
	[discount] [int] NULL,
	[description] [nvarchar](500) NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_369] PRIMARY KEY CLUSTERED 
(
	[price_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionBank]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionBank](
	[question] [nvarchar](500) NOT NULL,
	[level] [nvarchar](10) NULL,
	[result] [nvarchar](1000) NOT NULL,
	[topic_id] [int] NOT NULL,
	[image] [nvarchar](300) NULL,
	[description] [nvarchar](500) NULL,
	[b_id] [int] IDENTITY(1,1) NOT NULL,
	[createdTime] [datetime] NULL,
 CONSTRAINT [PK_438] PRIMARY KEY CLUSTERED 
(
	[b_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionChoices]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionChoices](
	[QuestionId] [int] NOT NULL,
	[Option] [nvarchar](1000) NOT NULL,
	[createdTime] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[q_id] [int] IDENTITY(1,1) NOT NULL,
	[u_id] [int] NOT NULL,
	[topic_id] [int] NOT NULL,
	[required_time] [nvarchar](50) NULL,
	[taken_date] [smalldatetime] NOT NULL,
	[score] [float] NOT NULL,
	[status] [smallint] NOT NULL,
	[condition] [smallint] NOT NULL,
	[time] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_367] PRIMARY KEY CLUSTERED 
(
	[q_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuizTaken]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuizTaken](
	[b_id] [int] NULL,
	[q_id] [int] NOT NULL,
	[question] [nvarchar](500) NULL,
	[question_result] [nvarchar](500) NULL,
	[question_explain] [nvarchar](500) NULL,
	[user_result] [nvarchar](500) NULL,
	[status] [smallint] NOT NULL,
	[taken_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_469] PRIMARY KEY CLUSTERED 
(
	[taken_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slider](
	[sl_id] [int] IDENTITY(1,1) NOT NULL,
	[sl_heading] [nvarchar](200) NOT NULL,
	[sl_content] [nvarchar](500) NOT NULL,
	[sl_image] [nvarchar](200) NOT NULL,
	[sl_link] [nvarchar](200) NOT NULL,
	[sl_status] [int] NOT NULL,
	[sl_show] [int] NOT NULL,
	[sl_begin] [smalldatetime] NOT NULL,
	[sl_end] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Slider] PRIMARY KEY CLUSTERED 
(
	[sl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[name] [nvarchar](300) NOT NULL,
	[typeID] [int] NOT NULL,
	[instructor_id] [int] NOT NULL,
	[description] [nvarchar](500) NULL,
	[status] [smallint] NOT NULL,
	[image] [nvarchar](300) NULL,
	[organization] [nvarchar](50) NULL,
	[logo] [nvarchar](300) NOT NULL,
	[s_id] [int] IDENTITY(1,1) NOT NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_199] PRIMARY KEY CLUSTERED 
(
	[s_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubjectCategory]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectCategory](
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[status] [smallint] NOT NULL,
	[description] [nvarchar](500) NULL,
	[image] [nvarchar](200) NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_352] PRIMARY KEY CLUSTERED 
(
	[cat_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubjectRegister]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectRegister](
	[regis_id] [int] IDENTITY(1,1) NOT NULL,
	[price_id] [int] NOT NULL,
	[status] [smallint] NOT NULL,
	[register_date] [smalldatetime] NOT NULL,
	[u_id] [int] NOT NULL,
 CONSTRAINT [PK_468] PRIMARY KEY CLUSTERED 
(
	[regis_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubjectType]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectType](
	[name] [nvarchar](50) NOT NULL,
	[cat_id] [int] NOT NULL,
	[status] [smallint] NOT NULL,
	[typeID] [int] IDENTITY(1,1) NOT NULL,
	[createdTime] [smalldatetime] NULL,
 CONSTRAINT [PK_357] PRIMARY KEY CLUSTERED 
(
	[typeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 7/25/2022 12:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[u_id] [int] IDENTITY(1,1) NOT NULL,
	[role] [nvarchar](50) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[status] [smallint] NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[birth] [smalldatetime] NOT NULL,
	[gender] [nvarchar](10) NOT NULL,
	[phone] [nvarchar](20) NOT NULL,
	[avatar] [nvarchar](300) NULL,
	[email] [nvarchar](50) NOT NULL,
	[description] [nvarchar](500) NULL,
	[address] [nvarchar](100) NULL,
	[company] [nvarchar](50) NULL,
	[school] [nvarchar](50) NULL,
	[createdTime] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_182] PRIMARY KEY CLUSTERED 
(
	[u_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (1, N'The Largely Untold Story Of How One Guy In California Keeps The World’s Computers On The Right Time Zone.', NULL, 2, CAST(N'2022-06-10T00:00:00' AS SmallDateTime), N'sharing', 1, N'I run a small YouTube channel.
And now and again, I record short videos documenting how to “do” certain things using Linux.
I make them as much for myself as for my 300-odd subscribers.
Because Linux, or rather doing things with it, tends to … you know … be quite complicated. And I can’t always remember how I got X to work three months later.
It’s nice to create documentation I can refer back to and it’s even better if others find it interesting as they occasionally tell me they do. But, for now at least, that’s about all there is to it.
Yesterday evening, I recorded a short video describing how to look up the time zone database (tzdb) to find the right way to denote the time zone on a certain world clock program (gworldclock). I wasn’t expecting that the video would give Netflix a run for its money. It hasn’t. But it has brought me into contact with a world so wonderfully weird that it could well the stuff of fiction. Thankfully it isn’t.
As most techies know, time zone setting is a fairly elementary feature of computing which most operating systems bake into their graphical user interfaces (GUI). Time zones are attached to locales. Setting a locale is often done on the basis of rough geolocation which users can manually override. Once set, users typically never need to change it unless they move countries. “Ah, I see Paul has been up to his usual tinkering” might be a clever excuse for being late to a Zoom meeting. But it would very seldom be a truthful one (Eggert, the world time zone coordinator, will be introduced later).
Linux, of course, tends to excel in letting users jump under the bonnet and denote changes themselves. That’s largely why I, and many, love it. So we get to see from a closer distance exactly how all this arcane technology works.', 2, N'Down the rabbit hole: my brief odyssey into the esoteric world of the tight-knit time zone data maintenance community who quietly keep the world’s computers from avoiding DST-related-meltdowns', N'https://resources.stuff.co.nz/content/dam/images/1/d/y/g/p/n/image.related.StuffLandscapeSixteenByNine.620x349.1dygvy.png/1473035461586.jpg')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (2, N'Effective Educational Software for Students', 6, 2, CAST(N'2022-06-03T00:00:00' AS SmallDateTime), N'learning', 1, N'Technology has impacted every sector, including the education sector. In recent times, there has been a growth in education software as well as sites that teach about different subject matters (Udemy, Coursera, etc).

Education software isn’t complex. It’s any computer software that is made with the intention of teaching or for the intention of educational software. Education software has its application on a broad range.

It can be free software for students, used to teach students or software made to assist certain target groups such as visually impaired students or software used for classroom management.

Now that we know what education software is, let’s have a look at the what is educational software.

Types of educational software
There are many different types of educational software, but for now, we shall focus on the software as a learning tool. With this in mind, here are the types of educational software:

Utility Software
Let’s start the list with utility software. This educational software is geared towards teachers that don’t have a good grasp of technology. The software is made easy to use and can assist a teacher with preparing quizzes, tests and can also act as a grade book if needed.

Authoring Systems
This software is used by teachers who want to create their own learning material for their students. Let’s face facts, some textbooks are not written in a manner that makes it easy for students to digest the material.

With an authoring system, a teacher can make multimedia content such as images, flash cards and more to simplify the content for the students to learn.', 2, N'Technology has impacted every sector, including the education sector. In recent times, there has been a growth in education software as well as sites that teach about different subject matters (Udemy, Coursera, etc).', N'https://content.thriveglobal.com/wp-content/uploads/2018/10/parents_teaching_kids_online.jpg')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (3, N'7 Key Points to Know for Developing a Multilingual Website', 4, 1, CAST(N'2020-08-14T00:00:00' AS SmallDateTime), N'sharing', 1, N'Appropriate the UI for Right-to-Left Languages
When designing a website for multilingual content, you should consider right-to-left (RTL) languages. Arabic languages will require you to appropriate your web design to accommodate their content. Users who prefer using their native Arabic languages on your website will be extremely happy with your foresight. On the other hand, left-to-right (LTR) speakers like those from the West will be none the wiser since pages in those languages won’t be affected.

2Culturally Neutral Multimedia is Important
Once you decide that your website will be multilingual, you should be mindful of your multimedia going forward. Choosing the right visuals and videos for your website can present a localization problem depending on your site’s purpose on the market. It might be a good idea to use stock photography with neutral images, lacking any culture-specific clothing or appearances. It’s very easy to drive away users with poorly chosen multimedia on a multilingual website, so take the time to curate yours thoroughly.

3Think About Font Compatibility Upfront
Font compatibility is an essential part of what makes web design for multilingual pages tricky. To avoid any practical issues, you should use Google Fonts and its alternatives to choose the fonts to use on your site. This will ensure that any font you choose does indeed display different alphabets and special characters properly. Based on W3, it’s best to use fonts with UTF-8 encoding from the moment you create original content in your CMS. Most importantly, drawing fonts from cloud resources will let you change them easily going forward.', 22, N'Developing a multilingual website is somewhat more complex than designing one solely in English for obvious reasons. However, with User Experience (UX) at the forefront of modern design trends, multilingual content has become a necessity for good SEO ranking and brand recognition.', N'https://th.bing.com/th/id/R.55c0601f814d7c4874472c03610f77b3?rik=n%2bup8ODeXneRXQ&pid=ImgRaw&r=0')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (4, N'Email Tracking in the Business Environment — Tool Capabilities', 8, 3, CAST(N'2022-09-30T00:00:00' AS SmallDateTime), N'other', 1, N'Internet marketing for B2B has several features: low traffic, high average check, long sales cycle, and prohibitive rates in contextual advertising. Therefore, Internet marketers working in B2B have to maintain the highest possible quality of statistics used for end-to-end analytics — otherwise, it is impossible to optimize traffic channels. Many of the specialists offer custom development for Zendesk using an individual approach. In most B2B topics, customer requests come in 4 ways:

calls — call tracking is used to track them;
applications — based on web forms on the site;
online chats — through the service class of the same name;
email — using email-tracking.
Moreover, if call tracking, web forms, and online chats are popular solutions, few marketers use email tracking for analytics of hits that come by email. In this article, we will tell why email tracking is needed and how a finished product differs from individual development.
What is email tracking and how does it work?
Email-tracking is a special technology that helps to track the requests of potential and current customers received by e-mail and link them to an online or offline traffic channel. A code is installed on the website that replaces the main email address of the site with a tracking email address: a unique one either for each user or for each traffic channel. With the help of email tracking, you can also track offline sources — for this, a dedicated email address is assigned to a specific traffic channel.', 26, N'Internet marketing for B2B has several features: low traffic, high average check, long sales cycle, and prohibitive rates in contextual advertising.', N'https://th.bing.com/th/id/R.f2c0265f12a36fbc378f15428168f943?rik=rJhjzNtV4rpUBQ&pid=ImgRaw&r=0')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (5, N'Must-Have Skills for a Competent Business Owner', 7, 4, CAST(N'2022-09-11T00:00:00' AS SmallDateTime), N'learning', 1, N'Striving your best to nurture and grow your skillset is essential in the world of business, as it can help define you as a forward-thinking, dedicated, and well-rounded professional. But what exactly are these skills? The list is incredibly long, and many of them complement each other, so perhaps the best way to move forward as a business owner is to identify which areas could benefit from a little improvement.

Sometimes, taking your skillset to the next level requires a great deal of self-motivation, determination, and commitment, but if you pride yourself on being a diligent business owner, this should be a breeze.

Here are some important skills worth nurturing in 2022, as you might find that improving on your personal traits is the catalyst that drives your business to new heights.

Contents [show]

A Thirst for Knowledge
Education is a wonderful tool, one that can enrich life in general, so seeking out new knowledge wherever possible should be high on the priority list. In many ways, learning never stops. Embracing this concept can help you make the most of the many fantastic resources at hand.

For the busiest business owners out there, returning to education might be easier than first expected, thanks in part to the world of online learning. For example, you can now take a great MBA online from the comfort of your home or place of business, or wherever has an internet connection.', 3, N'Striving your best to nurture and grow your skillset is essential in the world of business, as it can help define you as a forward-thinking, dedicated, and well-rounded professional. But what exactly are these skills? The list is incredibly long, and many of them complement each other, so perhaps the best way to move forward as a business owner is to identify which areas could benefit from a little improvement.

', N'https://www.newsforpublic.com/wp-content/uploads/2020/06/Small-Business-Owners.jpg')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (6, N'6 Smart Travel Packing Tips for Your Next Holiday', NULL, 5, CAST(N'2022-09-06T00:00:00' AS SmallDateTime), N'other', 0, N'As per the U.S. Travel Association, in 2019, travel spending averaged around USD 3.1 billion per day in the United States. To give you a better idea, it signifies that people spent USD 128 million an hour and USD 35,700 every second! Traveling is an essential part of everyone’s lives. When we travel, we make precious memories with friends and families.

But traveling also inculcates packing a colossal number of things into tiny little boxes and carrying them around with you. By packing smart – from installing the best roof box on your car to folding your clothes correctly – you can minimize any untoward inconveniences during trips.

Contents [show]

Make a packing list
Procrastinating the packing process is a common complaint among travelers, especially people who tend to forget small essential items like an electric charger, toothpaste, trimmer, etc. A clever psychological hack to start packing days earlier is by creating a list containing all the things you will need for the trip.

Creating a list of required items acts as a failsafe from over or under-packing. Another upside of doing this is that if you start early, you will be aware of all the things that you need to buy and have ample time on your hands to procure them.

Airline Baggage Policy
Speaking of over-packing, you should be aware of your airline’s baggage fee policy. You must do this before beginning the actual packing; otherwise, you may have to unpack everything at the last moment to adjust the weight and chuck out any prohibited items.

Most American airlines allow passengers to check-in at least one bag for international flights. But some air carriers charge a hefty sum for the same on domestic flights, which can cost you up to 100 bucks for a carry-on bag. The same rule applies if you are planning on flying with an air carrier in a foreign country. Make sure you learn their baggage policies as well.

Tip – To save space and reduce the weight of your baggage, you can consider wearing your heaviest clothes on the plane.', 5, N'As per the U.S. Travel Association, in 2019, travel spending averaged around USD 3.1 billion per day in the United States. To give you a better idea, it signifies that people spent USD 128 million an hour and USD 35,700 every second! Traveling is an essential part of everyone’s lives. When we travel, we make precious memories with friends and families.', N'https://th.bing.com/th/id/R.caca7513ea2a5dd5a5cfff23c5687c61?rik=hfqhcf3UruuWmg&pid=ImgRaw&r=0')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (7, N'The Importance of Music Education', 5, 1, CAST(N'2022-08-27T00:00:00' AS SmallDateTime), N'learning', 1, N'What if there was one activity that could benefit every student in every school across the nation? An activity that could improve grades and scores on standardized testing? An activity that would allow students to form lasting friendships? An activity that would help students become more disciplined and confident?

Fortunately, there is such an activity. Unfortunately, many schools will not make it a part of their curriculum, due to issues of funding and scheduling. This activity is something that everyone is aware of, but not everyone has a chance to participate in. This activity is music.

For years, music classes have been the ugly ducklings of school curriculums—the last courses to be added, the first courses to be cut. They have always taken second place to traditional academic classes. Music, however, has proved itself to be extremely beneficial time and time again, from the undeniable improvement in grades regarding traditional academic classes to the glowing remarks from music students everywhere. In an ever-changing world, the addition of music education in schools needs to be next on the academic agenda.  Music education should be a required component in all schools due to the proven academic, social, and personal benefits that it provides.

According to the No Child Left Behind Act, the following are defined as, “core academic subjects”: English, reading or language arts, mathematics, science, foreign languages, civics and government, economics, the arts [emphasis added], history, and geography (Benefits of the Study 1). Although music, being a part of the arts, is supposedly on the same level as other academic subjects, it is not being treated as such.

Music education greatly enhances students’ understanding and achievement in non-musical subjects. For example, a ten-year study, which tracked over 25,000 middle and high school students, showed that students in music classes receive higher scores on standardized tests than students with little to no musical involvement. The musical students scored, on average, sixty-three points higher on the verbal section and forty-four points higher on the math sections of the SATs than non-music students (Judson). When applying to colleges, these points could be the difference between an acceptance letter and a rejection letter.', 1, N'What if there was one activity that could benefit every student in every school across the nation? An activity that could improve grades and scores on standardized testing? An activity that would allow students to form lasting friendships? An activity that would help students become more disciplined and confident?', N'https://i.ytimg.com/vi/pIpROSkof1w/maxresdefault.jpg')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (8, N'Vietnamese project wins top prize at ‘Social Business Creation’ contest', 2, 4, CAST(N'2022-02-10T00:00:00' AS SmallDateTime), N'learning', 1, N'The global final round of the competition took place on October 2 with the competition of the top five SBC 2020 and the top five SBC 2022, which brought together 265 teams of 80 universities from 24 countries around the world.

Excellently overcoming their rivals, Vietnam’s Nanoneem project won the top prize (the Scotiabank Prize) and grabbed the title, thereby receiving a cash prize of CA$30,000 (US$23,770) and $4,200 (US$3,330) worth of scholarships from MOSAIC Summer School.

The project was developed by Dr. Duong Nguyen Hong Nhung, a lecturer in the biotechnology faculty of International University in Ho Chi Minh City, her students, and some other students from the Foreign Trade University in the same city.

Nanoneem introduces the application of nanotechnology to manufacturing safe plant protection drugs from herbs, toward clean and sustainable agriculture.

As a unique project that provides a potential business model benefiting the social community, Nanoneem was highly appreciated by the judges and always held the top positions through the last competition rounds.

The second prize went to the Argentinian team that presented a project on vocational training and job search for disadvantaged young people.', 3, N'The Social Business Creation (SBC) 2021 competition wrapped up in Canada with the first prize awarded to the Nanoneem project of two Vietnamese universities, its organizers announced on Sunday.', N'https://secure3.vncdn.vn/ttnew/r/2021/10/05/nanoneem-1633268210571417682431-1633408394.png')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (9, N'US SEC Approves Volt Equity’s ETF, Allowing Investors to Gain Exposure to Bitcoin', 7, 4, CAST(N'2020-04-15T00:00:00' AS SmallDateTime), N'other', 0, N'reviously, the SEC has approved the Volt crypto industry revolution and tech ETF on Tuesday, October 5.

According to SEC’s filing, the Volt Equity’s ETF aims to track so-called “Bitcoin Industry Revolution Companies”, which are defined as companies that hold a majority of their net assets in Bitcoin or derive a majority of their profit or revenue from Bitcoin-related activities like lending, mining, transacting, or manufacturing mining equipment.

Tad Pak, the founder and CEO of Volt Equity, talked about the development and said that the fund would offer investors easy access to firms with significant exposure to Bitcoin. In other words, the new “Volt Bitcoin Revolution ETF” service is set to allow investors to create a portfolio of “Bitcoin revolution companies” that hold a significant amount of the cryptocurrency on their balance sheets.

“I’m a strong believer in bitcoin and was really excited about launching an ETF that could take advantage of the coming bitcoin revolution. We can get exposure to Bitcoin without necessarily holding the coin, especially with options positions,” Pak noted.

Pak explained that the fund’s assets would consist of shares of about 30 companies, including Tesla, Square, Coinbase, PayPal, MicroStrategy, as well as Twitter, which recently made Bitcoin tipping as part of its operations, including Bitcoin mining firms like Bitfarms, Marathon Digital Holdings, which ahold Bitcoin in their corporate treasures, and other related companies.', 4, N'As investors are anxiously waiting for the US Securities and Exchange Commission (SEC) to approve a Bitcoin EFT, the regulator has taken a step closer in that direction by giving approval of a fund called the “Volt Bitcoin Revolution ETF.”', N'https://cryptonewstop.com/wp-content/uploads/2021/10/457679AF17FDFCF084130D02F280185B449AC25FEA64776617716EB483C70D0F-1024x538.jpg')
INSERT [dbo].[Blog] ([blog_id], [title], [typeID], [author], [time], [type], [status], [content], [modifier], [description], [image]) VALUES (10, N'Vietnamese university to offer $1mn scholarships to international students', NULL, 5, CAST(N'2022-06-20T00:00:00' AS SmallDateTime), N'sharing', 1, N'A total of 170 scholarships worth $1 million will be granted to international students in the academic year 2022-22, said Prof. Dr. Su Dinh Thanh, the UEH’s principal.

These scholarships include 100 undergraduate scholarships, 50 scholarships for master’s degree and 20 fellowship scholarships.

They will be granted during the official training period of the academic year, including funding of 50 to 100 percent of the tuition and boarding costs depending on the type of partial, full or excellent scholarships.

Such scholarships will be given to foreign students who are admitted into training programs taught in Vietnamese or English at the UEH, but they are not exchange students or those nominated for training under treaties.

In addition, scholarship winners must be those who have yet to receive any scholarship in Vietnam.  

Foreign students do not need to prepare application for such scholarships, since the UEH will directly select qualified candidates from their enrollment documents and grant appropriate scholarships for them.

Excellent scholarships will be kept unchanged throughout the official study period, while full and partial scholarships will be subject to upgrade or downgrade depending on academic achievements of the beneficiaries of such scholarships, Dr. Thanh said.

“This policy aims to motivate students to strive for better schooling results to maintain their scholarship levels during their study at the school,” the principal added.

The UEH, established in 1976, has been detached from the Vietnam National University - Ho Chi Minh City since 2000 to operate under the administration of the Ministry of Education and Training.

As shown on its website, the university has been recognized in the Top 1000 Best Business Schools globally, and in 2020 it was ranked in the Top 5 National Universities by several prestigious international publications in Vietnam.', 22, N'Vietnam’s University of Economics Ho Chi Minh City (UEH) is considering awarding some US$1 million worth of scholarships to foreign students in the coming time, the school’s headmaster has said.', N'https://secure3.vncdn.vn/ttnew/r/2021/07/02/ueh-16251968390511608436167-1625219195.jpg')
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[Email] ON 

INSERT [dbo].[Email] ([eid], [esubject], [econtent], [ediscription], [ecreateTime]) VALUES (1, N'Thousands of courses 70% off this summer. Hurry up now', N'Nhanh tay nao', N'sale summer', CAST(N'2022-07-13T22:17:00' AS SmallDateTime))
INSERT [dbo].[Email] ([eid], [esubject], [econtent], [ediscription], [ecreateTime]) VALUES (2, N'Back to School Sale 99%', N'Thousands of courses for schools have already started selling, register quickly.', N'Back to School Sale', CAST(N'2022-07-13T22:27:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Email] OFF
GO
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (1, 1, 1, N'e', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (6, 3, 2, N'h', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (7, 2, 3, N'h', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (9, 3, 1, N'h', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (10, 1, 2, N'h', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (11, 2, 3, N'm', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (12, 4, 1, N'e', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (13, 4, 2, N'h', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (14, 5, 3, N'e', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (15, 5, 1, N'm', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
INSERT [dbo].[Exam] ([exam_id], [u_id], [s_id], [level], [created_date], [number_of_question], [status], [pass_rate], [duration]) VALUES (16, 3, 2, N'e', CAST(N'2022-06-22T00:00:00' AS SmallDateTime), 50, 1, 50, 360)
GO
SET IDENTITY_INSERT [dbo].[Lesson] ON 

INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (1, N'Basics of Meeting Online', 1, N'https://youtu.be/p2J7wSuFRl8', NULL, N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', NULL, 0, N'https://images.unsplash.com/photo-1634125486933-df0e64f5bbca?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0MHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60', N'https://drive.google.com/file/d/1aRfi_fj0aPaYOO4xC5OzWHtfXh4S4nfu/view?usp=sharing', 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (2, N'Practice with Dialogues', 2, N'https://youtu.be/p2J7wSuFRl8', NULL, N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', NULL, 0, N'https://drive.google.com/file/d/1aYF45f6V2s2xsLWDgAPIvBZsBX-V6rrp/view?usp=sharing', N'https://youtu.be/gaVg5S7NaxU', 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (3, N'Check Your Knowledge', 3, N'https://youtu.be/p2J7wSuFRl8', NULL, N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', N'How Other Nations Pay for Child Care. The U.S. Is an Outlier.', 0, N'https://drive.google.com/file/d/1aYF45f6V2s2xsLWDgAPIvBZsBX-V6rrp/view?usp=sharing', N'https://drive.google.com/file/d/1aRfi_fj0aPaYOO4xC5OzWHtfXh4S4nfu/view?usp=sharing', 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (4, N'Lets Learn Telephone Language', 1, N'https://youtu.be/p2J7wSuFRl8', NULL, N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', NULL, 0, N'https://drive.google.com/file/d/1aYF45f6V2s2xsLWDgAPIvBZsBX-V6rrp/view?usp=sharing', N'https://drive.google.com/file/d/1aRfi_fj0aPaYOO4xC5OzWHtfXh4S4nfu/view?usp=sharing', 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (5, N'Phone Role Play', 2, N'https://youtu.be/p2J7wSuFRl8', N'https://images.unsplash.com/photo-1633114130148-3f40987134d2?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80', N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', N'(CNN)Distributing Covid-19 vaccine booster shots in some countries while inoculations across Africa lag is "immoral," according to the director general of the World Health Organization (WHO).', 0, N'https://genk.vn/banh-mi-mon-an-co-lich-su-6000-nam-da-dat-nen-mong-cho-am-thuc-cua-chung-ta-hom-nay-20211005121406213.chn', N'https://drive.google.com/file/d/1aRfi_fj0aPaYOO4xC5OzWHtfXh4S4nfu/view?usp=sharing', 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (6, N'Practice Stress', 3, N'https://youtu.be/p2J7wSuFRl8', N'https://images.unsplash.com/photo-1634157014713-23e5cd60b674?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60', N'Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.', NULL, 0, N'https://drive.google.com/file/d/1aYF45f6V2s2xsLWDgAPIvBZsBX-V6rrp/view?usp=sharing', N'https://designervn.net/', 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (7, N'Soạn bài Khái quát văn học Việt Nam từ đầu Cách mạng tháng Tám 1945 đến thế kỉ XX', 1, N'https://www.tiktok.com/@nganhuahihi/video/6995783615053335834', N'https://tech12h.com/sites/default/files/anh_7.png', N'So?n bài Khái quát van h?c Vi?t Nam t? d?u Cách m?ng tháng Tám 1945 d?n th? k? XX', N'So?n bài Khái quát van h?c Vi?t Nam t? d?u Cách m?ng tháng Tám 1945 d?n th? k? XX', 0, N'https://hoc247.net/ngu-van-12/soan-bai-khai-quat-vhvn-tu-dau-cmt8-1945-den-the-ki-xx-l3418.html', N'https://hoc247.net/hoi-dap/ngu-van-12/ve-dep-cua-con-nguoi-viet-nam-qua-cac-chang-duong-lich-su-faq379456.html', 4, CAST(N'2022-07-25T12:15:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (8, N'Nghị luận về một tư tưởng, đạo lý', 2, N'we', N'https://toplist.vn/images/800px/bai-soan-nghi-luan-ve-mot-van-de-tu-tuong-dao-li-lop-9-hay-nhat-543004.jpg', N'Bài h?c giúp các em n?m du?c cách làm bài van ngh? lu?n v? m?t tu tu?ng, d?o lý nhu: phân tích d?; l?p dàn ý; nêu ý ki?n dánh giá, nh?n xét v? v?n d? c?n ngh? lu?n;', N'Bài h?c giúp các em n?m du?c cách làm bài van ngh? lu?n v? m?t tu tu?ng, d?o lý nhu: phân tích d?; l?p dàn ý; nêu ý ki?n dánh giá, nh?n xét v? v?n d? c?n ngh? lu?n;', 0, N'https://hoc247.net/ngu-van-12/nghi-luan-ve-mot-tu-tuong-dao-ly-l3419.html', N'https://hoc247.net/ngu-van-12/nghi-luan-ve-mot-tu-tuong-dao-ly-l3419.html', 4, CAST(N'2022-07-25T12:17:00' AS SmallDateTime))
INSERT [dbo].[Lesson] ([l_id], [name], [no], [video], [image], [content], [description], [status], [references], [documents], [topic_id], [createdTime]) VALUES (9, N'Tuyên ngôn độc lập - Hồ Chí Minh', 3, N'3', N'https://toplist.vn/images/800px/bai-soan-nghi-luan-ve-mot-van-de-tu-tuong-dao-li-lop-9-hay-nhat-543004.jpg', N'N?i dung bài h?c Tuyên ngôn d?c l?p ph?n tác gi? H? Chí Minh giúp các em hi?u du?c nh?ng nét khái quát v? s? nghi?p van h?c, quan di?m sáng tác và nh?ng d?c di?m co b?n c?a phong cách ngh? thu?t H? Chí Minh.', N'So?n bài Khái quát van h?c Vi?t Nam t? d?u Cách m?ng tháng Tám 1945 d?n th? k? XX', 0, N'https://hoc247.net/ngu-van-12/tuyen-ngon-doc-lap-ho-chi-minh-phan-1-tac-gia-l3423.html', N'https://hoc247.net/ngu-van-12/tuyen-ngon-doc-lap-ho-chi-minh-phan-1-tac-gia-l3423.html', 4, CAST(N'2022-07-25T12:20:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Lesson] OFF
GO
SET IDENTITY_INSERT [dbo].[LessonTopic] ON 

INSERT [dbo].[LessonTopic] ([name], [s_id], [description], [purpose], [status], [topic_id], [createdTime]) VALUES (N'Entering the Job Market', 1, NULL, NULL, 1, 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[LessonTopic] ([name], [s_id], [description], [purpose], [status], [topic_id], [createdTime]) VALUES (N'Resumes', 1, NULL, NULL, 1, 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[LessonTopic] ([name], [s_id], [description], [purpose], [status], [topic_id], [createdTime]) VALUES (N'Writing a Cover Letter', 1, NULL, NULL, 1, 3, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[LessonTopic] ([name], [s_id], [description], [purpose], [status], [topic_id], [createdTime]) VALUES (N'Khái quát văn học Việt Nam từ đầu Cách mạng tháng Tám 1945 đến thế kỉ XX ', 8, N'Bài học Khái quát văn học Việt Nam từ đầu Cách mạng tháng Tám năm 1945 đến hết thế kỉ XX giúp các em nắm được một số nét tổng quát về các chặng đường phát triển, những thành tựu chủ yếu và những đặc điểm cơ bản của văn học Việt Nam', N'những đặc điểm cơ bản của văn học Việt Nam (VHVN) từ CMT8 năm 1945 đến năm 1975 và những đổi mới bước đầu của VHVN giai đoạn từ năm 1975, nhất là từ năm 1986 đến hết thế kỉ XX. ', 0, 4, CAST(N'2022-07-25T12:08:00' AS SmallDateTime))
INSERT [dbo].[LessonTopic] ([name], [s_id], [description], [purpose], [status], [topic_id], [createdTime]) VALUES (N'Giữ gìn sự trong sáng của tiếng Việt', 8, N'Bài học Giữ gìn sự trong sáng của tiếng Việt sẽ giúp các em nhận thức được sự trong sáng là một trong những phẩm chất của tiếng Việt, là kết quả phấn đấu lâu dài của ông cha ta.', N'Nắm được các phương diện biểu hiện sự trong sáng của tiếng Việt: tính chuẩn mực, có quy tắc;', 0, 5, CAST(N'2022-07-25T12:21:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[LessonTopic] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (1, N'test                          ', N'des1                                                                                                                                                                                                                             ', CAST(N'2022-06-24T06:03:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (2, N'New user arrived              ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-24T06:20:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (3, N'New user arrived              ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-24T07:08:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (4, N'16has logined                 ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-24T07:12:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (5, N'16has logined                 ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-24T07:13:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (6, N'16has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:15:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (7, N'16has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:17:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (8, N'16has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:28:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (9, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:30:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (10, N'20 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:35:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (11, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T07:43:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (12, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T09:51:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (13, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T14:55:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (14, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T14:59:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (15, N'20 has logined                ', N'HelloCao Ba                                                                                                                                                                                                                      ', CAST(N'2022-06-24T15:00:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (16, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T15:03:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (17, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T15:13:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (18, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T15:21:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (19, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T15:21:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (20, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-24T15:25:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (21, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-26T20:32:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (22, N'User 1 Has Been Edited By 16  ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-26T20:40:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (23, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T09:48:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (24, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:02:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (25, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:06:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (26, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:07:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (27, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:08:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (28, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:09:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (29, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:09:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (30, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:12:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (31, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:14:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (32, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:15:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (33, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T10:22:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (34, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T15:05:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (35, N'16 has logined                ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T16:22:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (36, N'New user arrived              ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-12T16:24:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (37, N'New user arrived              ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-12T16:25:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (38, N'1 has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-06-12T16:39:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (39, N'User 1 Has Been Edited By 1   ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-06-12T16:42:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (40, N'1 has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-11-01T07:11:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (41, N'1 has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-11-01T21:23:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (42, N'1 has logined                 ', N'HelloXuan Huy                                                                                                                                                                                                                    ', CAST(N'2022-11-01T22:53:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (43, N'User 1 Has Been Edited By 1   ', N'Hello                                                                                                                                                                                                                            ', CAST(N'2022-11-01T22:53:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (44, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-06T21:36:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (45, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-06T21:36:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (46, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-06T21:43:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (48, N'huydthe150252 tried to login  ', N'Logined Fail                                                                                                                                                                                                                     ', CAST(N'2022-11-07T17:19:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (49, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:19:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (50, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:26:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (51, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:26:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (52, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:27:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (53, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:27:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (54, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:28:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (55, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:28:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (56, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:29:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (57, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:29:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (58, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:33:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (59, N'Xuan HuyHuy tried to logout   ', N'Logouted Success                                                                                                                                                                                                                 ', CAST(N'2022-11-07T17:35:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (60, N'Tran VanAn tried to login     ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:35:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (61, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:43:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (62, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:48:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (63, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:50:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (64, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-11-07T17:52:00' AS SmallDateTime))
INSERT [dbo].[Notification] ([ID], [Title], [Description], [Time]) VALUES (1001, N'Xuan HuyHuy tried to login    ', N'Logined Success                                                                                                                                                                                                                  ', CAST(N'2022-05-27T14:24:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
SET IDENTITY_INSERT [dbo].[PricePackage] ON 

INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (1, 1, 29, 1, N'usd', 15, NULL, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (2, 2, 29, 1, N'usd', NULL, NULL, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (3, 2, 43, 0, N'usd', 30, NULL, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (4, 3, 20, 1, N'usd', 10, NULL, CAST(N'2022-07-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (5, 8, 20, 0, N'usd', 10, NULL, CAST(N'2022-07-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (6, 8, 20, 1, N'usd', 10, NULL, CAST(N'2022-07-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (7, 13, 20, 1, N'usd', 10, NULL, CAST(N'2022-07-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (8, 15, 20, 1, N'usd', 10, NULL, CAST(N'2022-07-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[PricePackage] ([price_id], [s_id], [price], [status], [currency_unit], [discount], [description], [createdTime]) VALUES (9, 16, 20, 1, N'usd', 10, NULL, CAST(N'2022-07-25T11:06:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[PricePackage] OFF
GO
SET IDENTITY_INSERT [dbo].[QuestionBank] ON 

INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'When Python is running in the interactive mode and displaying the chevron prompt (>>>) - what question is Python asking you?', N'm', N'What would you like to do?', 1, NULL, N'This is the explaination', 1, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'Python scripts (files) have names that end with:', N'm', N'.py', 1, NULL, N'This is the explaination', 2, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'Which of the parts of a computer actually executes the program instructions?', N'm', N'Central Processing Unit', 1, NULL, N'This is the explaination', 3, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'What is "code" in the context of this course?', N'm', N'A sequence of instructions in a programming language', 1, NULL, N'This is the explaination', 4, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'A USB memory stick is an example of which of the following components of computer architecture?', N'm', N'Secondary Memory', 1, NULL, N'This is the explaination', 5, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'What is the best way to think about a "Syntax Error" while programming?', N'm', N' The computer did not understand the statement that you entered', 1, NULL, N'This is the explaination', 6, CAST(N'2022-06-12T09:40:00.000' AS DateTime))
INSERT [dbo].[QuestionBank] ([question], [level], [result], [topic_id], [image], [description], [b_id], [createdTime]) VALUES (N'test', NULL, N'result', 2, NULL, N'description', 7, CAST(N'2022-07-25T12:04:35.610' AS DateTime))
SET IDENTITY_INSERT [dbo].[QuestionBank] OFF
GO
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (1, N'What Python script would you like me to run?', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (1, N'What is your favourite color?', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (1, N'What would you like to do?', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (1, N'What is the next machine language instruction to run?', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (2, N'.py', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (2, N'.exe', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (2, N'.png', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (2, N'.doc', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (3, N'Main Memory', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (3, N'Central Processing Unit', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (3, N'Input/Output Devices', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (3, N'Secondary Memory', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (4, N'A password we use to unlock Python features', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (4, N'A sequence of instructions in a programming language', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (4, N'A way to encrypt data during World War II', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (4, N'A set of rules that govern the style of programs', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (5, N'Central Processing Unit', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (5, N'Main Memory', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (5, N'Output Device', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (5, N'Secondary Memory', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (6, N'The computer is overheating and just wants you to stop to let it cool down', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (6, N'The computer has used GPS to find your location and hates everyone from your town', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (6, N'The computer did not understand the statement that you entered', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[QuestionChoices] ([QuestionId], [Option], [createdTime]) VALUES (6, N'The computer needs to have its software upgraded', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
GO
SET IDENTITY_INSERT [dbo].[Slider] ON 

INSERT [dbo].[Slider] ([sl_id], [sl_heading], [sl_content], [sl_image], [sl_link], [sl_status], [sl_show], [sl_begin], [sl_end]) VALUES (1, N'Coursee', N'Online learning system with thousands of quality courses', N'https://s35691.pcdn.co/wp-content/uploads/2015/07/group-work150722.jpg', N'SubjectController', 1, 0, CAST(N'2022-07-01T00:00:00' AS SmallDateTime), CAST(N'2022-08-13T15:07:00' AS SmallDateTime))
INSERT [dbo].[Slider] ([sl_id], [sl_heading], [sl_content], [sl_image], [sl_link], [sl_status], [sl_show], [sl_begin], [sl_end]) VALUES (2, N'Blog', N'Discuss with thousands of teacher and student blogs', N'https://s35691.pcdn.co/wp-content/uploads/2021/06/positive-excited-multiethnic-students-in-casual-clothing-lying-on-in-picture-id1185581975.jpg', N'BlogList', 1, 0, CAST(N'2022-07-12T00:02:00' AS SmallDateTime), CAST(N'2022-08-13T19:59:00' AS SmallDateTime))
INSERT [dbo].[Slider] ([sl_id], [sl_heading], [sl_content], [sl_image], [sl_link], [sl_status], [sl_show], [sl_begin], [sl_end]) VALUES (3, N'Subject Summer Sale', N'Thousands of courses 70% off this summer. Hurry up now', N'https://img.freepik.com/free-vector/end-season-summer-sale-banner_23-2148621268.jpg?w=2000', N'home', 0, 1, CAST(N'2022-07-11T15:06:00' AS SmallDateTime), CAST(N'2022-07-15T15:06:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Slider] OFF
GO
SET IDENTITY_INSERT [dbo].[Subject] ON 

INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'English for Career Development', 1, 1, N'This course is designed for non-native English speakers who are interested in advancing their careers in the global marketplace.  In this course, you will learn about the job search, application, and interview process in the United States, while comparing and contrasting the same process in your home country.
                                                                                        
                                                                                        
        ', 1, N'https://th.bing.com/th/id/OIP.rDFKuHWE2loRkegtRYyFpAHaEC?pid=ImgDet&w=1480&h=808&rs=1', N'FPT Universityyy', N'https://fpt.edu.vn/Content/images/assets/img-logo-fe.png', 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Speak English Professionally', 1, 2, N'Do you want to speak better English? This course will help you reach that goal. Speak English Professionally: In person, Online and On the Phone will boost your English speaking skills. In this 5 week course, you will learn how to identify and make a strong personal introduction.', 1, N'https://ieltsamericas.com/wp-content/uploads/2017/02/man-speaking-english.png', N'Havard University', N'https://th.bing.com/th/id/OIP.ixJlQpQGon62rGWW_U_YHQHaHa?pid=ImgDet&rs=1', 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Grammar and Punctuation', 1, 5, N'Do you need to review English grammar? Have you forgotten the grammar you once studied? If so, this course is perfect for you.', 1, N'https://cheaptraining.com/wp-content/uploads/2020/10/AdobeStock_103437308-2.jpeg', N'GLN Organizartion', N'https://th.bing.com/th/id/R.8271c2f3153964c7393497f4bfa1505b?rik=I3yB%2bp0i4BkgIA&pid=ImgRaw&r=0', 3, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Ngữ Văn Lớp 12', 2, 1, N'Môn Ngữ văn 12 không chỉ quan trọng trong chương trình học lớp 12 mà nó còn là môn thi chính trong các kì thi tốt nghiệp THPT Quốc Gia. Chính vì vậy, việc học môn này các bạn học sinh lớp 12 cũng cần phải đầu tư nhiều thời gian, công sức hơn. ', 1, N'https://upcdn.io/kW15aszFwfYHQXLH3bADkRS', N'Hoc247', N'Hoc247', 8, CAST(N'2022-07-22T00:24:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Web Design for Everybody: Basics of Web Development & Coding', 9, 1, N'This Specialization covers how to write syntactically correct HTML5 and CSS3, and how to create interactive web experiences with JavaScript. Mastering this range of technologies will allow you to develop high quality web sites that, work seamlessly on mobile, tablet, and large screen browsers accessible. During the capstone you will develop a professional-quality web portfolio demonstrating your growth as a web developer and your knowledge of accessible web design. ', 1, N'https://upcdn.io/kW15aszYBoJyrb1KxQ5KcaA', N'Coursera', N'Coursera', 13, CAST(N'2022-07-20T23:20:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Meta Back-End Developer', 8, 1, N'Ready to gain new skills and learn the tools developers use to create websites and web applications? This 10-course program, designed by the software engineering experts at  Meta, will prepare you for an entry-level career as a back-end developer. On completion, you’ll get exclusive access to the Meta Career Programs Job Board—a job search platform that connects you with 200+ employers who have committed to sourcing talent through Meta’s certificate programs.', 1, N'https://upcdn.io/kW15aszVHZPckJqVApufMsx', N'Coursera', N'Coursera', 15, CAST(N'2022-07-20T23:35:00' AS SmallDateTime))
INSERT [dbo].[Subject] ([name], [typeID], [instructor_id], [description], [status], [image], [organization], [logo], [s_id], [createdTime]) VALUES (N'Meta Front-End Developer', 8, 1, N'Want to get started in the world of coding and build beautiful websites as a career? This 9-course program, designed by the software engineering experts at Meta, will prepare you for a career as a front-end developer.Once you complete the program, you’ll get exclusive access to the Meta Career Programs Job Board—a job search platform that connects you with 200+ employers who have committed to sourcing talent through Meta’s certificate programs.', 1, N'https://upcdn.io/kW15asz9g5HwXNrrGwc4XWa', N'Coursera', N'Coursera', 16, CAST(N'2022-07-20T23:37:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Subject] OFF
GO
SET IDENTITY_INSERT [dbo].[SubjectCategory] ON 

INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (1, N'Language Learning', 1, N'Best Language Learning Methods and Teaching Approaches Explained.', N'https://upcdn.io/kW15aszUbocX52FC8MALdHY', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (2, N'Math and Logic', 1, N'Mathematical logic is the study of formal logic within mathematics.', N'https://upcdn.io/kW15asz7kimhPogHbfRhKgA', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (3, N'Arts and Humanities', 1, N'The Best Liberal Arts Colleges', N'https://upcdn.io/kW15aszFcrcfpZETSq1EGkg', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (4, N'Computer Science', 1, N'Take a look at some of the top universities offering computer science in the UK.', N'https://upcdn.io/kW15aszDfHdFCm4kNGpfQZg', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (5, N'Information Technology', 1, N'Information Technology, abbreviated IT', N'https://upcdn.io/kW15aszTtFyD6XqpybELauk', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (6, N'Business', 1, N'Know more Business', N'https://upcdn.io/kW15aszCq9ahBmweXhi7VZG', CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectCategory] ([cat_id], [name], [status], [description], [image], [createdTime]) VALUES (7, N'Design', 1, N'Know more Design', N'https://upcdn.io/kW15aszXprMaWk34sxdLf2V', CAST(N'2022-07-20T23:33:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[SubjectCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[SubjectType] ON 

INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'EngLish', 1, 1, 1, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Vietnamese', 1, 1, 2, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Linear Algebra', 2, 1, 3, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Design', 3, 1, 4, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Music', 3, 1, 5, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Java', 4, 1, 6, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Blockchain', 6, 1, 7, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'Google', 5, 0, 8, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
INSERT [dbo].[SubjectType] ([name], [cat_id], [status], [typeID], [createdTime]) VALUES (N'DevOps', 5, 1, 9, CAST(N'2022-06-12T09:40:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[SubjectType] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (1, N'admin', N'oaiPBHE15051', N'0000', 1, N'Oai', N'Phạm', CAST(N'2001-10-03T00:00:00' AS SmallDateTime), N'male', N'03687837952323', N'https://www.w3schools.com/howto/img_forest.jpg', N'oaipbhe150516@fpt.edu.vn', N'Xuan Huy Huy, Professor of Computer Science, serves as the Interim Dean of College of Engineering.', N'Ha Noi', N'', N'', CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (2, N'admin', N'sangDTHE1308', N'0000', 1, N'Sang', N'Đào', CAST(N'2001-04-19T00:00:00' AS SmallDateTime), N'male', N'0939001374', N'https://scontent.fhan5-4.fna.fbcdn.net/v/t1.6435-9/36316650_2178077255754148_3741668116912930816_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=VPjbTg1i9loAX_yonmZ&tn=fQnj9WBuZpYGjz2k&_nc_ht=scontent.fhan5-4.fna&oh=004527db2144cc2add6dc4f21fd6511d&oe=6187160C', N'SangDTHE130883@fpt.edu.vn', N'Professor of Computer Science, serves as the Interim Dean of College of Engineering.', N'Ha Noi', NULL, NULL, CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (3, N'admin', N'ducVLHE15373', N'0000', 1, N'Linh', N'Vũ', CAST(N'2001-06-06T00:00:00' AS SmallDateTime), N'male', N'0815960062', N'https://scontent.fhan5-4.fna.fbcdn.net/v/t39.30808-6/242662178_3069830233338420_717062301363859372_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=174925&_nc_ohc=8pQKdejBat8AX9v1O3m&_nc_ht=scontent.fhan5-4.fna&oh=5dbb90e3d252c5be26e983f0ae1ea37b&oe=616767DC', N'DucVLHE153739@fpt.edu.vn', N'Professor of Computer Science, serves as the Interim Dean of College of Engineering.', N'Nghe An', NULL, NULL, CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (4, N'admin', N'hieuNDHE1304', N'0000', 1, N'Hiếu', N'Nguyễn', CAST(N'2001-08-28T00:00:00' AS SmallDateTime), N'male', N'0384295199', N'https://images.unsplash.com/photo-1547425260-76bcadfb4f2c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80', N'HieuNDHE130453@fpt.edu.vn', N'Professor of Computer Science, serves as the Interim Dean of College of Engineering.', N'Ha Noi', NULL, NULL, CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (5, N'admin', N'anhBDDHE151', N'0000', 1, N'Anh', N'Bùi', CAST(N'2001-11-15T00:00:00' AS SmallDateTime), N'male', N'03537771558', N'https://www.w3schools.com/howto/img_forest.jpg', N'AnhBDDHE151102@fpt.edu.vn', N'Professor of Computer Science, serves as the Interim Dean of College of Engineering.', N'Nghe An', N'', N'', CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (10, N'customer', N'customerDemo', N'123', 1, N'Demo', N'Customer', CAST(N'2000-01-01T00:00:00' AS SmallDateTime), N'male', N'0123123123', N'https://www.w3schools.com/howto/img_forest.jpg', N'orehx01@gmail.com', N'Demo Customer', N'', N'', N'', CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (15, N'admin', N'admin', N'admin', 1, N'Huy', N'Phạm', CAST(N'2000-01-13T00:00:00' AS SmallDateTime), N'male', N'0336577960', N'https://www.w3schools.com/howto/img_forest.jpg', N'cuongtmhe150619@fpt.edu.vn', N'Đây là tài khoản Admin của HuyPX', N'Tổ 8 Thạch Thất - Thạch Hoà - Hà Nội', NULL, N'FPT University', CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (17, N'customer', N'asd', N'asd', 1, N'Huy', N'Phạm', CAST(N'2000-01-13T00:00:00' AS SmallDateTime), N'male', N'0336577960`', N'https://www.w3schools.com/howto/img_forest.jpg', N'huypxhe140702@gmail.com', N'Đây là tài khoản customer của HuyPX', N'Tổ 8 Thạch Thất - Thạch Hoà - Hà Nội', NULL, N'FPT University', CAST(N'2022-05-16T00:00:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (22, N'customer', N'hieu', N'123', 1, N'Hieu', N'Nguyễn', CAST(N'2000-04-27T00:00:00' AS SmallDateTime), N'male', N'09876543210', N'1234.png', N'hieundhe130453"fpt.edu.vn', N'ơeoiwenogwegobw', N'obwegbwogbwebg', N'bgerbgbgeob', N'bgerbgouebgro', CAST(N'2022-06-02T23:08:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (23, N'customer', N'William', N'238P47JBZ31WWF', 0, N'Ronda', N'Holland', CAST(N'2011-12-24T21:23:00' AS SmallDateTime), N'female', N'825-850-7006', N'https://images.unsplash.com/photo-1530268729831-4b0b9e170218?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'kryi@yzgja-.org', N'Tam quantare et quad estum. nomen ut imaginator et novum quartu pars fecit. fecundio, sed cognitio, pars rarendum gravis', N'903 South Clarendon Parkway', N'Grohupentor  ', N'L', CAST(N'2022-06-27T13:47:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (25, N'customer', N'William', N'238P47JBZ31WWF', 0, N'Ronda', N'Holland', CAST(N'2011-12-24T21:23:00' AS SmallDateTime), N'female', N'825-850-7006', N'https://images.unsplash.com/photo-1530268729831-4b0b9e170218?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'kryi@yzgja-.org', N'Tam quantare et quad estum. nomen ut imaginator et novum quartu pars fecit. fecundio, sed cognitio, pars rarendum gravis', N'903 South Clarendon Parkway', N'Grohupentor  ', N'L', CAST(N'2022-06-27T13:47:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (26, N'customer', N'Toby227', N'AXAF56HRPDW7UIWWWTMX63NJQEVOQUJ6128CZYCJLBJC70PT', 0, N'Rickey', N'Mercer', CAST(N'1989-09-03T17:12:00' AS SmallDateTime), N'male', N'864648-0953', N'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'wmofd.nnuebyw@alhvoacwq.pywghv.org', N'transit. essit. quis si esset Multum eggredior. egreddior Sed vobis rarendum linguens Multum eggredior. quad', N'668 Green Second St.', N'Supcadefex Direct ', N'T4LLKTFXE54TJBDL3R4IYW37M5', CAST(N'2022-07-17T18:45:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (35, N'customer', N'Gerald632', N'6429HV69QPRAS1VTQ5R9KWXG3CVKBBQP2LJPGWK72299L8', 1, N'Amelia', N'Stephens', CAST(N'2015-03-16T11:18:00' AS SmallDateTime), N'male', N'4391160338', N'https://images.unsplash.com/photo-1517462964-21fdcec3f25b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzB8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'yvpytpdl.qmjczejq@bwjqor.org', N'in esset essit. linguens quantare quorum estum. gravis novum linguens vobis regit, venit. habitatio gravum', N'51 Cowley Street', N'Vartanin  Group', N'B2Q4TG4APCYQR11EMD8OMRPERU3IW1EYEM3VO0BCF1', CAST(N'2022-06-10T01:35:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (36, N'customer', N'Bridget74', N'ANIHUXJJA', 0, N'Carol', N'Sheppard', CAST(N'1984-01-18T23:06:00' AS SmallDateTime), N'female', N'5260313421', N'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjF8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'qgmix.fnbr@betfcb.com', N'eudis sed quorum fecundio, gravis Longam, quorum quartu cognitio, Et bono habitatio quo et pladior quo novum regit,', N'36 Rocky Cowley Parkway', N'Tupnipilower  ', N'840DGWHGUEHJKCBCY', CAST(N'2022-06-02T04:16:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (37, N'customer', N'Kellie20', N'L54ZLYEY176I6CTKJ7HNOUVJLXSUVSI28IVRG8G3U', 0, N'Nicholas', N'Murphy', CAST(N'2000-05-07T23:12:00' AS SmallDateTime), N'male', N'907-402-6944', N'https://images.unsplash.com/photo-1519419691348-3b3433c4c20e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjd8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'sxrc.ckyjbjnvow@peuqiy.nzqqkl.com', N'e regit, et brevens, quoque novum non pars glavans parte manifestum quartu in egreddior delerium. fecit, fecundio,', N'65 East Green Second Road', N'Enddudepax International ', N'MDOY0C4Y6QL1V8L3H9QHMLFKJJW3IJC', CAST(N'2022-07-01T07:48:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (38, N'customer', N'Donna', N'K0MRH5OADC1NIRAKEO6K38P49NH24BRDSEAU', 1, N'Marsha', N'Rocha', CAST(N'2004-09-16T22:32:00' AS SmallDateTime), N'female', N'319-726-1145', N'https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'rxjvf8@rpiegq.net', N'non non e delerium. eudis Sed fecundio, Quad quoque quad bono Et dolorum Quad sed dolorum quo, quo quartu', N'934 West Old Blvd.', N'Innipexistor WorldWide ', N'CZSOZ94N9HQRUTJ8Y', CAST(N'2022-06-26T14:16:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (39, N'customer', N'Brandi67', N'TFM', 1, N'Jennie', N'Decker', CAST(N'1980-02-19T01:06:00' AS SmallDateTime), N'female', N'7940525922', N'https://images.unsplash.com/photo-1530268729831-4b0b9e170218?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'onie7@cltnxmq.yeabow.com', N'eudis delerium. in vantis. vobis sed plurissimum si linguens estum. non quad manifestum plorum Et novum dolorum', N'690 Rocky Old St.', N'Klipebplar International ', N'V1MHQ0X3FGF0GNDYPWGW374384', CAST(N'2022-06-02T21:37:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (40, N'customer', N'Gerald5', N'LF60L03GVTRXA6CBHHTOM51KOU3HFNL8JEITI4ONDL', 0, N'Alexander', N'Torres', CAST(N'2002-01-17T20:06:00' AS SmallDateTime), N'female', N'0872406430', N'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'aqir9@xaklut.org', N'gravis nomen plurissimum nomen parte eudis quartu pars vantis. fecit, fecit. apparens si linguens Quad', N'938 Second Avenue', N'Thrutinonor Holdings Inc', N'OQQZN5', CAST(N'2022-06-04T12:26:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (41, N'customer', N'Felix6', N'I0FI7XJFBMYJFDMBQWM178IEVOITCT6A1WJK8', 0, N'Tyson', N'Shannon', CAST(N'2012-09-18T23:04:00' AS SmallDateTime), N'male', N'267648-7814', N'https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'qqjbcuds48@qyeazj.com', N'fecundio, habitatio si e et Tam quo, quorum Multum pladior regit, e estis quo, quorum cognitio, egreddior vobis', N'39 East Milton Freeway', N'Trusipan International Group', N'NA1U8SQWUO7812DW3NS1DOXI9M49DPHSVFZ2WUNN57DGVZ3SA', CAST(N'2022-07-20T08:48:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (42, N'customer', N'Kendra', N'WJW79MKV2GWSXQQJ19H1MJFP2IFNIIT1', 0, N'Darrell', N'Patterson', CAST(N'1998-12-01T06:24:00' AS SmallDateTime), N'male', N'803-2372991', N'https://images.unsplash.com/photo-1481824429379-07aa5e5b0739?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'jlabalfj99@zyyvey.net', N'vantis. novum brevens, non habitatio in quis si si fecundio, quantare Sed Sed vobis rarendum quartu non', N'90 Oak Boulevard', N'Frodudinicator Holdings ', N'MCU3NZDIL', CAST(N'2022-07-12T23:03:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (43, N'customer', N'Joni', N'DZ340ZWTZL2XKVGJHV937QD364R7WDKMUPKQNF1AL7MXCS', 1, N'Ashley', N'Powers', CAST(N'1981-09-05T10:17:00' AS SmallDateTime), N'male', N'622926-6078', N'https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'owqxkmh.stpgy@trnzeaxxc.yfbcdk.org', N'vobis non quoque Longam, rarendum et quartu et trepicandor gravum sed venit. et quoque plurissimum quo,', N'302 North Fabien Avenue', N'Raptumegower Direct ', N'OHJ47FCM3A09Z5W6BGN', CAST(N'2022-07-06T15:30:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (44, N'customer', N'Joy49', N'MY40NKYVNMZYV7OWM1G2UBPW', 1, N'Colleen', N'Barber', CAST(N'1986-04-24T02:10:00' AS SmallDateTime), N'male', N'335-602-9909', N'https://images.unsplash.com/photo-1499996860823-5214fcc65f8f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'xdkd@qgfizbx.bnlzen.net', N'ut travissimantor trepicandor plurissimum esset plurissimum bono funem. vantis. travissimantor imaginator', N'21 Clarendon Boulevard', N'Dopmunackower Direct Corp.', N'JBYJ34XSR0PD81YON89FMGP0TDCP7GOXVC2CI', CAST(N'2022-06-10T18:19:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (45, N'customer', N'Cameron351', N'TYHZ3FQDL48SCW1WTW4NG52WA0WWQOA97YTLGWK78K93G39', 1, N'Amy', N'Hatfield', CAST(N'2005-09-19T01:44:00' AS SmallDateTime), N'female', N'757-451-5304', N'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjB8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'vanaq.iohaxqq@xtjqrt.com', N'non volcans quartu parte et glavans linguens habitatio plorum gravum venit. quorum si quantare plurissimum', N'569 Green Hague Freeway', N'Inmunar Holdings ', N'02HKU2LM0Y67J268M2MYQQS8LBCRIWYM6N77X2LMF6CEIZXLQN', CAST(N'2022-07-14T23:48:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (46, N'customer', N'Gena199', N'E60EL8LOVLP2JUNXTZKE2FFXCWEWVZG6RRIJNVNZC5Q', 0, N'Telly', N'Frank', CAST(N'2018-12-10T18:22:00' AS SmallDateTime), N'female', N'269934-6022', N'https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTZ8fHJhbmRvbSUyMHBlb3BsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', N'jebq.tjdriagbtz@tr-zgl.net', N'plorum vobis si Tam gravum apparens vobis quo volcans funem. e et quad brevens, nomen manifestum dolorum', N'70 South Rocky Milton Parkway', N'Lomdudicator WorldWide ', N'0QK9A99ZJQT43Z', CAST(N'2022-07-05T18:21:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (47, N'customer', N'Krystal', N'7IOEXC9', 0, N'Micah', N'Olsen', CAST(N'1988-11-02T14:57:00' AS SmallDateTime), N'male', N'0170337152', N'https://images.unsplash.com/photo-1525134479668-1bee5c7c6845?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'bleia6@qwhloq.net', N'quad volcans nomen et Id fecit. plorum volcans apparens quorum et e in non Id non non Et habitatio esset', N'80 East Green Oak Road', N'Hapmunar WorldWide Company', N'1WUZAKHZJJDEQLEQ1V6YV', CAST(N'2022-06-28T14:24:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (48, N'customer', N'Amie', N'JU2YQ32OTPSV5IARB39XBL1WPK', 1, N'Hugh', N'Good', CAST(N'2002-02-03T16:37:00' AS SmallDateTime), N'female', N'2982953023', N'https://images.unsplash.com/photo-1530785602389-07594beb8b73?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'iqmw98@xjxou.zhevqg.com', N'in delerium. quantare linguens linguens nomen plorum quo, quo e in gravum volcans quis Pro regit, nomen', N'56 West New Boulevard', N'Kliweredower International ', N'DXXO21VL19FXY1RRC5I', CAST(N'2022-06-10T04:08:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (49, N'customer', N'Harvey49', N'U80ZJQYNXOJSK', 1, N'Glenn', N'Carey', CAST(N'2018-09-17T21:20:00' AS SmallDateTime), N'female', N'438-463-9338', N'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'jiiwo323@ffltkfax.hnabam.org', N'fecit. habitatio volcans transit. plurissimum manifestum venit. non quoque et essit. volcans Sed vantis.', N'46 White First Street', N'Adfropistor International ', N'0PXVJE50R9W0O', CAST(N'2022-07-10T19:26:00' AS SmallDateTime))
INSERT [dbo].[User] ([u_id], [role], [username], [password], [status], [first_name], [last_name], [birth], [gender], [phone], [avatar], [email], [description], [address], [company], [school], [createdTime]) VALUES (50, N'customer', N'Scot2', N'Z5NDUPO7W', 1, N'Chadwick', N'Spears', CAST(N'1996-03-03T15:57:00' AS SmallDateTime), N'female', N'872-6150611', N'https://images.unsplash.com/photo-1517841905240-472988babdf9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8cmFuZG9tJTIwcGVvcGxlfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60', N'yibfwn.bgtpoeplob@lkkfmy.org', N'essit. bono vobis vobis esset fecit. Et novum quorum fecit, si Versus quantare egreddior Longam, fecit. trepicandor bono', N'487 North Rocky Cowley Street', N'Monmunilower Direct ', N'IX1LHF4XI4ZSNDZAI1F5LNZ25OWVT', CAST(N'2022-07-23T04:03:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[PricePackage] ADD  CONSTRAINT [DF_PricePackage_currency_unit]  DEFAULT ('dollar') FOR [currency_unit]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_status]  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_151] FOREIGN KEY([author])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_151]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_385] FOREIGN KEY([typeID])
REFERENCES [dbo].[SubjectType] ([typeID])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_385]
GO
ALTER TABLE [dbo].[Certificate]  WITH CHECK ADD  CONSTRAINT [FK_276] FOREIGN KEY([u_id])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[Certificate] CHECK CONSTRAINT [FK_276]
GO
ALTER TABLE [dbo].[Certificate]  WITH CHECK ADD  CONSTRAINT [FK_279] FOREIGN KEY([s_id])
REFERENCES [dbo].[Subject] ([s_id])
GO
ALTER TABLE [dbo].[Certificate] CHECK CONSTRAINT [FK_279]
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD  CONSTRAINT [FK_570] FOREIGN KEY([s_id])
REFERENCES [dbo].[Subject] ([s_id])
GO
ALTER TABLE [dbo].[Exam] CHECK CONSTRAINT [FK_570]
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD  CONSTRAINT [FK_571] FOREIGN KEY([u_id])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[Exam] CHECK CONSTRAINT [FK_571]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_381] FOREIGN KEY([topic_id])
REFERENCES [dbo].[LessonTopic] ([topic_id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_381]
GO
ALTER TABLE [dbo].[LessonTopic]  WITH CHECK ADD  CONSTRAINT [FK_378] FOREIGN KEY([s_id])
REFERENCES [dbo].[Subject] ([s_id])
GO
ALTER TABLE [dbo].[LessonTopic] CHECK CONSTRAINT [FK_378]
GO
ALTER TABLE [dbo].[PricePackage]  WITH CHECK ADD  CONSTRAINT [FK_98] FOREIGN KEY([s_id])
REFERENCES [dbo].[Subject] ([s_id])
GO
ALTER TABLE [dbo].[PricePackage] CHECK CONSTRAINT [FK_98]
GO
ALTER TABLE [dbo].[QuestionChoices]  WITH CHECK ADD  CONSTRAINT [FK_QuestionChoices_QuestionBank] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[QuestionBank] ([b_id])
GO
ALTER TABLE [dbo].[QuestionChoices] CHECK CONSTRAINT [FK_QuestionChoices_QuestionBank]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_324] FOREIGN KEY([u_id])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_324]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_462] FOREIGN KEY([topic_id])
REFERENCES [dbo].[LessonTopic] ([topic_id])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_462]
GO
ALTER TABLE [dbo].[QuizTaken]  WITH CHECK ADD  CONSTRAINT [FK_142] FOREIGN KEY([b_id])
REFERENCES [dbo].[QuestionBank] ([b_id])
GO
ALTER TABLE [dbo].[QuizTaken] CHECK CONSTRAINT [FK_142]
GO
ALTER TABLE [dbo].[QuizTaken]  WITH CHECK ADD  CONSTRAINT [FK_145] FOREIGN KEY([q_id])
REFERENCES [dbo].[Quiz] ([q_id])
GO
ALTER TABLE [dbo].[QuizTaken] CHECK CONSTRAINT [FK_145]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_363] FOREIGN KEY([typeID])
REFERENCES [dbo].[SubjectType] ([typeID])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_363]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_79] FOREIGN KEY([instructor_id])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_79]
GO
ALTER TABLE [dbo].[SubjectRegister]  WITH CHECK ADD  CONSTRAINT [FK_343] FOREIGN KEY([u_id])
REFERENCES [dbo].[User] ([u_id])
GO
ALTER TABLE [dbo].[SubjectRegister] CHECK CONSTRAINT [FK_343]
GO
ALTER TABLE [dbo].[SubjectRegister]  WITH CHECK ADD  CONSTRAINT [FK_346] FOREIGN KEY([price_id])
REFERENCES [dbo].[PricePackage] ([price_id])
GO
ALTER TABLE [dbo].[SubjectRegister] CHECK CONSTRAINT [FK_346]
GO
ALTER TABLE [dbo].[SubjectType]  WITH CHECK ADD  CONSTRAINT [FK_360] FOREIGN KEY([cat_id])
REFERENCES [dbo].[SubjectCategory] ([cat_id])
GO
ALTER TABLE [dbo].[SubjectType] CHECK CONSTRAINT [FK_360]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Người viết bài' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Blog', @level2type=N'COLUMN',@level2name=N'author'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'kiểu bài viết (về khoa học, hướng dẫn học, mẹo,...)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Blog', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Người chỉnh sửa sau cùng của bài viết' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Blog', @level2type=N'COLUMN',@level2name=N'modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'mô tả' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Blog', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'số thứ tự của lesson' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lesson', @level2type=N'COLUMN',@level2name=N'no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'link' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lesson', @level2type=N'COLUMN',@level2name=N'video'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'link' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lesson', @level2type=N'COLUMN',@level2name=N'image'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'nguồn tham khảo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lesson', @level2type=N'COLUMN',@level2name=N'references'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nguồn tài liệu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Lesson', @level2type=N'COLUMN',@level2name=N'documents'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'giá' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PricePackage', @level2type=N'COLUMN',@level2name=N'price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'đơn vị tiền tệ (mặc định là dollar)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PricePackage', @level2type=N'COLUMN',@level2name=N'currency_unit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'giảm giá' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PricePackage', @level2type=N'COLUMN',@level2name=N'discount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'độ khó của câu hỏi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuestionBank', @level2type=N'COLUMN',@level2name=N'level'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'đáp án đúng' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuestionBank', @level2type=N'COLUMN',@level2name=N'result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'câu hỏi của lesson nào' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuestionBank', @level2type=N'COLUMN',@level2name=N'topic_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'link' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuestionBank', @level2type=N'COLUMN',@level2name=N'image'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'user nào đã làm quiz này' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'u_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'thời gian được cho phép để làm quiz' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'required_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ngày làm quiz' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'taken_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'trạng thái đậu hay trượt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'condition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thời gian user làm quiz thực tế' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Kết quả mà user trả lời' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuizTaken', @level2type=N'COLUMN',@level2name=N'user_result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'user id của người tạo subject' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subject', @level2type=N'COLUMN',@level2name=N'instructor_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'link ảnh của khoá học' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subject', @level2type=N'COLUMN',@level2name=N'image'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tổ chức tài trợ hoặc sở hữu khoá học' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subject', @level2type=N'COLUMN',@level2name=N'organization'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'link ảnh logo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subject', @level2type=N'COLUMN',@level2name=N'logo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'package đã đky' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SubjectRegister', @level2type=N'COLUMN',@level2name=N'price_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ngày đăng kí' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SubjectRegister', @level2type=N'COLUMN',@level2name=N'register_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'id của user đã đky' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SubjectRegister', @level2type=N'COLUMN',@level2name=N'u_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Có 4 role chính: student, admin, expert, maketer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mặc định là 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lưu 1 trong 2 giá trị mà M hoặc F' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lưu link ảnh, lúc dùng chỉ cần nhúng vào thẻ img' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'avatar'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mô tả của user. VD: giáo viên có x năm kinh nghiệm giảng dạy, ....' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Công ty/ tổ chức: dành cho mentor, instructor để lưu thông tin nơi họ đang làm việc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trường học: dành cho hssv' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'school'
GO
