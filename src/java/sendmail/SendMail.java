/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sendmail;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

/**
 *
 * @author Admin
 */
public class SendMail {

    public static void sendmail(String email, String subject, String content) {
        //Creating properties
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");//change accordingly
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Utils.EMAIL, Utils.PASSWORD);
            }
        });

        try {
            MimeMessage mm = new MimeMessage(session);
            mm.setFrom(new InternetAddress(Utils.EMAIL));
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mm.setSubject(subject);
            mm.setText(content);
            try {
                Transport.send(mm);
            } catch (MessagingException e) {
                System.out.println(e);
            }

        } catch (MessagingException e) {
            System.out.println(e);
        }

    }

    public static void sendmaillist(ArrayList<String> listMails, String nsubject, String ncontent) throws AddressException, MessagingException {
        //Creating properties
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");//change accordingly
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Utils.EMAIL, Utils.PASSWORD);
            }
        });
//        MimeMessage mm = new MimeMessage(session);
//        mm.setFrom(new InternetAddress(Utils.EMAIL));
//        mm.setSubject(nsubject);
//        mm.setText(ncontent);
//        for (String listMail : listMails) {
//            try {
//                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(listMail));
//                try {
//                    Transport.send(mm);
//                } catch (MessagingException e) {
//                    System.out.println(e);
//                }
//            } catch (MessagingException e) {
//                System.out.println(e);
//            }
//        }

        for (String listMail : listMails) {
            try {
                MimeMessage mm = new MimeMessage(session);
                mm.setFrom(new InternetAddress(Utils.EMAIL));
                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(listMail));
                mm.setSubject(nsubject);
                mm.setText(ncontent);
                try {
                    Transport.send(mm);
                    System.out.println(listMail+" done!");
                } catch (MessagingException e) {
                    System.out.println(e);
                }

            } catch (MessagingException e) {
                System.out.println(e);
            }
        }
    }
}
