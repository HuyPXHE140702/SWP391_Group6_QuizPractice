/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class QuestionBank {
    
    private int id;
    private String question;
    private String level;
    private String result;
    private String image;
    private String description;
    private String created_date;
    
    private Topic topic;
    private ArrayList<QuestionChoices> questionChoices = new ArrayList<>();
    

    public QuestionBank() {
    }

    public QuestionBank(int id, String question, String level, String result, String image, String description, String created_date, Topic topic) {
        this.id = id;
        this.question = question;
        this.level = level;
        this.result = result;
        this.image = image;
        this.description = description;
        this.created_date = created_date;
        this.topic = topic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public ArrayList<QuestionChoices> getQuestionChoices() {
        return questionChoices;
    }

    public void setQuestionChoices(ArrayList<QuestionChoices> questionChoices) {
        this.questionChoices = questionChoices;
    }

    
    
    
}
