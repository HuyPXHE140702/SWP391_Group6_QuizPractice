/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class SubjectCat {

    protected String name;
    protected int status;
    protected int catId;
    private String description;
    private String image;
    private int num_of_subcate; // subcate = type
    private int num_of_sub;
    private ArrayList<SubjectType> types;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNum_of_sub() {
        return num_of_sub;
    }

    public void setNum_of_sub(int num_of_sub) {
        this.num_of_sub = num_of_sub;
    }

    public ArrayList<SubjectType> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<SubjectType> types) {
        this.types = types;
    }

    public int getNum_of_subcate() {
        return num_of_subcate;
    }

    public void setNum_of_subcate(int num_of_subcate) {
        this.num_of_subcate = num_of_subcate;
    }

    public SubjectCat(int catId, String name, int num_of_subcate) {
        this.catId = catId;
        this.name = name;
        this.num_of_subcate = num_of_subcate;
    }

    public SubjectCat() {
    }

    public SubjectCat(String name, int status, int catId) {
        this.name = name;
        this.status = status;
        this.catId = catId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

}
