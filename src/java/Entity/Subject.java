/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Subject {

    private int s_id;
    private String name;
    private String description;
    private String image;
    private String organization;
    private String logo;

    // DA
    private User instructor;
    private SubjectType subjectType;

    private ArrayList<Topic> topics;
    //DA

    private int num_regis_sub;
    private int type_id;
    private int instructor_id;
    private int status;
    private int price_id;
    private int price;
    private int cat_id;
    private String cat_name;
    private int discount;

    private String currency_unit;
    private String instructor_first_name;
    private String instructor_last_name;
    private String instructor_role;
    private String instructor_avatar;

    public String getInstructor_avatar() {
        return instructor_avatar;
    }

    public void setInstructor_avatar(String instructor_avatar) {
        this.instructor_avatar = instructor_avatar;
    }

    public int getNum_regis_sub() {
        return num_regis_sub;
    }

    public void setNum_regis_sub(int num_regis_sub) {
        this.num_regis_sub = num_regis_sub;
    }

    public String getInstructor_role() {
        return instructor_role;
    }

    public void setInstructor_role(String instructor_role) {
        this.instructor_role = instructor_role;
    }

    public Subject(int s_id, String name, String description, String image, String organization, String logo, User instructor, SubjectType subjectType, ArrayList<Topic> topics, int type_id, int instructor_id, int status, int price_id, int price, int cat_id, String cat_name, int discount, String currency_unit, String instructor_first_name, String instructor_last_name) {
        this.s_id = s_id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.organization = organization;
        this.logo = logo;
        this.instructor = instructor;
        this.subjectType = subjectType;
        this.topics = topics;
        this.type_id = type_id;
        this.instructor_id = instructor_id;
        this.status = status;
        this.price_id = price_id;
        this.price = price;
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.discount = discount;
        this.currency_unit = currency_unit;
        this.instructor_first_name = instructor_first_name;
        this.instructor_last_name = instructor_last_name;
    }

    public Subject() {
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public int getPrice_id() {
        return price_id;
    }

    public void setPrice_id(int price_id) {
        this.price_id = price_id;
    }

    public Subject(int type_id, int instructor_id, int status, String name, String description, String image, String organization, String logo) {
        this.type_id = type_id;
        this.instructor_id = instructor_id;
        this.status = status;
        this.name = name;
        this.description = description;
        this.image = image;
        this.organization = organization;
        this.logo = logo;
    }

    public Subject(int type_id, int instructor_id, int status, String name, String description, String image, String organization, String logo, int price) {
        this.type_id = type_id;
        this.instructor_id = instructor_id;
        this.status = status;
        this.name = name;
        this.description = description;
        this.image = image;
        this.organization = organization;
        this.logo = logo;
        this.price = price;
    }

    public User getInstructor() {
        return instructor;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public int getInstructor_id() {
        return instructor_id;
    }

    public void setInstructor_id(int instructor_id) {
        this.instructor_id = instructor_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCurrency_unit() {
        return currency_unit;
    }

    public void setCurrency_unit(String currency_unit) {
        this.currency_unit = currency_unit;
    }

    public String getInstructor_first_name() {
        return instructor_first_name;
    }

    public void setInstructor_first_name(String instructor_first_name) {
        this.instructor_first_name = instructor_first_name;
    }

    public String getInstructor_last_name() {
        return instructor_last_name;
    }

    public void setInstructor_last_name(String instructor_last_name) {
        this.instructor_last_name = instructor_last_name;
    }

}
