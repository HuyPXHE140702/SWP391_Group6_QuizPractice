/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class SubjectType {

    protected String name;
    protected int id;
    protected int status;
    protected int cat_id;
    private SubjectCat subjectCategory;
    private int num_of_sub;// sub = subject has that typeId
    private ArrayList<Subject> subjects;

    public int getNum_of_sub() {
        return num_of_sub;
    }

    public void setNum_of_sub(int num_of_sub) {
        this.num_of_sub = num_of_sub;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(ArrayList<Subject> subjects) {
        this.subjects = subjects;
    }

    public SubjectCat getSubjectCategory() {
        return subjectCategory;
    }

    public void setSubjectCategory(SubjectCat subjectCategory) {
        this.subjectCategory = subjectCategory;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public SubjectType(String name) {
        this.name = name;
    }

    public SubjectType(String name, int status) {
        this.name = name;
        this.status = status;
    }

    public SubjectType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
