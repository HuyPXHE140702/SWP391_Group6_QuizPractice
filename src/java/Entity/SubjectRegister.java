/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author ADMIN
 */
public class SubjectRegister extends Subject{
    private int sr_id;
    private int no;
    private String date;
    private Subject subject;
    private int u_id;
    private Price priceP;
    
     private String currency;
     
     private String u_email;
     
    public SubjectRegister() {
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }


    

      public SubjectRegister(int price, String currency, String date, int type_id, int instructor_id, int status, String name, String description, String image, String organization, String logo) {

        super(type_id, instructor_id, status, name, description, image, organization, logo, price);
        this.currency = currency;
        this.date = date;
    }

    public Price getPriceP() {
        return priceP;
    }

    public void setPriceP(Price priceP) {
        this.priceP = priceP;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }



    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSr_id() {
        return sr_id;
    }

    public void setSr_id(int sr_id) {
        this.sr_id = sr_id;
    }   

    public String getU_email() {
        return u_email;
    }

    public void setU_email(String u_email) {
        this.u_email = u_email;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }  
}
