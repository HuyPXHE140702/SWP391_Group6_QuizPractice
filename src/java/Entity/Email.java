/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author Phamb
 */
public class Email {

    public int eid;
    public String esubject;
    public String econtent;
    public String ediscription;
    public String ecreateTime;

    public Email() {
    }

    public Email(int eid, String esubject, String econtent, String ediscription, String ecreateTime) {
        this.eid = eid;
        this.esubject = esubject;
        this.econtent = econtent;
        this.ediscription = ediscription;
        this.ecreateTime = ecreateTime;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public String getEsubject() {
        return esubject;
    }

    public void setEsubject(String esubject) {
        this.esubject = esubject;
    }

    public String getEcontent() {
        return econtent;
    }

    public void setEcontent(String econtent) {
        this.econtent = econtent;
    }

    public String getEdiscription() {
        return ediscription;
    }

    public void setEdiscription(String ediscription) {
        this.ediscription = ediscription;
    }

    public String getEcreateTime() {
        return ecreateTime;
    }

    public void setEcreateTime(String ecreateTime) {
        this.ecreateTime = ecreateTime;
    }

}
