/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author Phamb
 */
public class Slide {

    private int sl_id;
    private String sl_heading;
    private String sl_content;
    private String sl_image;
    private String sl_link;
    private int sl_status;
    private int sl_show;
    private String sl_begin;
    private String sl_end;

    public Slide() {
    }

    public Slide(int sl_id, String sl_heading, String sl_content, String sl_image, String sl_link, int sl_status, int sl_show, String sl_begin, String sl_end) {
        this.sl_id = sl_id;
        this.sl_heading = sl_heading;
        this.sl_content = sl_content;
        this.sl_image = sl_image;
        this.sl_link = sl_link;
        this.sl_status = sl_status;
        this.sl_show = sl_show;
        this.sl_begin = sl_begin;
        this.sl_end = sl_end;
    }

    public int getSl_id() {
        return sl_id;
    }

    public void setSl_id(int sl_id) {
        this.sl_id = sl_id;
    }

    public String getSl_heading() {
        return sl_heading;
    }

    public void setSl_heading(String sl_heading) {
        this.sl_heading = sl_heading;
    }

    public String getSl_content() {
        return sl_content;
    }

    public void setSl_content(String sl_content) {
        this.sl_content = sl_content;
    }

    public String getSl_image() {
        return sl_image;
    }

    public void setSl_image(String sl_image) {
        this.sl_image = sl_image;
    }

    public String getSl_link() {
        return sl_link;
    }

    public void setSl_link(String sl_link) {
        this.sl_link = sl_link;
    }

    public int getSl_status() {
        return sl_status;
    }

    public void setSl_status(int sl_status) {
        this.sl_status = sl_status;
    }

    public int getSl_show() {
        return sl_show;
    }

    public void setSl_show(int sl_show) {
        this.sl_show = sl_show;
    }

    public String getSl_begin() {
        return sl_begin;
    }

    public void setSl_begin(String sl_begin) {
        this.sl_begin = sl_begin;
    }

    public String getSl_end() {
        return sl_end;
    }

    public void setSl_end(String sl_end) {
        this.sl_end = sl_end;
    }

}
