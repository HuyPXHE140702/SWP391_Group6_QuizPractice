/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author ADMIN
 */
public class QuizTaken {
    private int taken_id;
    private QuestionBank questionBank;  // Questiion 
    private int q_id; //QuizID
    private String user_result;
    private String question;
    private String question_result;
    private String question_explain;
    private int status;

    public QuizTaken() {
    }

    public QuizTaken(int taken_id, QuestionBank questionBank, int q_id, String user_result, int status) {
        this.taken_id = taken_id;
        this.questionBank = questionBank;
        this.q_id = q_id;
        this.user_result = user_result;
        this.status = status;
    }

    public int getTaken_id() {
        return taken_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion_explain() {
        return question_explain;
    }

    public void setQuestion_explain(String question_explain) {
        this.question_explain = question_explain;
    }

    public void setTaken_id(int taken_id) {
        this.taken_id = taken_id;
    }

    public QuestionBank getQuestionBank() {
        return questionBank;
    }

    public void setQuestionBank(QuestionBank questionBank) {
        this.questionBank = questionBank;
    }

    public int getQ_id() {
        return q_id;
    }

    public String getQuestion_result() {
        return question_result;
    }

    public void setQuestion_result(String question_result) {
        this.question_result = question_result;
    }

    public void setQ_id(int q_id) {
        this.q_id = q_id;
    }

    public String getUser_result() {
        return user_result;
    }

    public void setUser_result(String user_result) {
        this.user_result = user_result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
    
}
