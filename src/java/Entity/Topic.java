/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class Topic {
    private int topic_id; 
    private String name; 
    private String description;
    private String purpose ;
            
    //DA
    private Subject subject;
    private ArrayList<Lesson> lessons;
    private ArrayList<QuestionBank> questions;
   //private ArrayList<Quizz> quizes;
    //DA
    private int s_id;
    private int status;
    
    

    public Topic() {
    }

    public int getTopic_id() {
        return topic_id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public ArrayList<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(ArrayList<Lesson> lessons) {
        this.lessons = lessons;
    }

    public ArrayList<QuestionBank> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionBank> questions) {
        this.questions = questions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
           
    
    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
