/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author Phamb
 */
public class Money {

    private int totalMoney;
    private String timeMoney;

    public Money() {
    }

    public Money(int totalMoney, String timeMoney) {
        this.totalMoney = totalMoney;
        this.timeMoney = timeMoney;
    }

    public int getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(int totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getTimeMoney() {
        return timeMoney;
    }

    public void setTimeMoney(String timeMoney) {
        this.timeMoney = timeMoney;
    }
    
    
}
