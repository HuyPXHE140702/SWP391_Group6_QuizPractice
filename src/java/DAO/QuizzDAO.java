/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Quizz;
import Entity.Question;
import Entity.QuizTaken;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class QuizzDAO extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public static void main(String[] args) {
        QuizzDAO dao = new QuizzDAO();
        ArrayList<Quizz> quizzList = dao.getQuizByUserId(17);
        System.out.println(quizzList.get(0).getTopic_name());
    }

    //Get Quizz by User Id
    public ArrayList<Quizz> getQuizzByUserId(int id) {
        ArrayList<Quizz> quizzList = new ArrayList<>();
        String query = "select [Quiz].*, LessonTopic.[name] as topic_name, [Subject].[name] as s_name "
                + "from (([Quiz] join [LessonTopic] on Quiz.topic_id=[LessonTopic].topic_id) "
                + "join [Subject] on [LessonTopic].s_id = [Subject].s_id) "
                + "where u_id=" + id;

        try {
            conn = DBConnection.open();
            ps = conn.prepareCall(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                // Get all quizz by user Id 
                Quizz q = new Quizz();
                q.setQ_id(rs.getInt("q_id"));
                q.setU_id(rs.getInt("u_id"));
                q.setTopic_id(rs.getInt("topic_id"));
                q.setRequired_time(rs.getString("required_time"));
                q.setTaken_date(rs.getString("taken_date"));
                q.setScore(rs.getFloat("score"));
                q.setCondition(rs.getInt("condition"));
                q.setStatus(rs.getInt("status"));
                q.setTime(rs.getString("time"));
                q.setTopic_name(rs.getString("topic_name"));
                q.setS_name(rs.getString("s_name"));
                quizzList.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return quizzList;
    }

    /////// DA
    public ArrayList<Quizz> getQuizByUserId(int u_id) {
        ArrayList<Quizz> quizzs = new ArrayList<>();
        try {

            String sql = "select q.*, topic_name= t.[name], subject_name= s.[name] \n"
                    + "from quiz q\n"
                    + "left join [LessonTopic] t on t.topic_id=q.topic_id\n"
                    + "left join [subject] s on s.s_id = t.s_id\n"
                    + "where q.u_id =? "
                    + "order by q.taken_date desc"
                    + "";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, u_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Quizz q = new Quizz();
                q.setQ_id(rs.getInt("q_id"));
                q.setU_id(rs.getInt("u_id"));
                q.setTopic_id(rs.getInt("topic_id"));
                q.setRequired_time(rs.getString("required_time"));
                q.setTaken_date(rs.getString("taken_date"));
                q.setScore(rs.getFloat("score"));
                q.setCondition(rs.getInt("condition"));
                q.setStatus(rs.getInt("status"));
                q.setTime(rs.getString("time"));

                q.setQuizTakens(getQuizTakenByQuiz(q.getQ_id()));

                q.setTopic_name(rs.getString("topic_name"));
                q.setS_name(rs.getString("subject_name"));

                quizzs.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return quizzs;
    }

    public ArrayList<QuizTaken> getQuizTakenByQuiz(int quiz_id) {
        ArrayList<QuizTaken> quizTakens = new ArrayList<>();
        try {

            String sql = "select * \n"
                    + "from QuizTaken qt\n"
                    + "where qt.q_id =?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quiz_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                QuizTaken qt = new QuizTaken();
                
                qt.setQ_id(rs.getInt("q_id"));
                qt.setQuestion(rs.getString("question"));
                qt.setQuestion_explain(rs.getString("question_explain"));
                qt.setQuestion_result(rs.getString("question_explain"));
                qt.setUser_result(rs.getString("user_result"));
                qt.setStatus(rs.getInt("status"));
                qt.setTaken_id(rs.getInt("taken_id"));
                
                quizTakens.add(qt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return quizTakens;
    }

//////////////////////////////////DA
//Get all question for admin
    public ArrayList<Question> getAllQues() {
        ArrayList<Question> quesList = new ArrayList<>();
        String query = "select [QuestionBank].*,[Lesson].name as lesson_name from [QuestionBank] "
                + "join [Lesson] on [QuestionBank].l_id=[Lesson].l_id "
                + "order by [QuestionBank].b_id "
                + "desc";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                // Get all quizz by user Id 
                Question ques = new Question();
                ques.setId(rs.getString("b_id"));
                ques.setAnswer(rs.getString("result"));
                ques.setContent(rs.getString("question"));
                ques.setDescription(rs.getString("description"));
                ques.setImage(rs.getString("image"));
                ques.setLessonId(rs.getString("l_id"));
                ques.setType(rs.getString("level"));
                ques.setCreated_date(rs.getString("createdTime"));
                ques.setLessonName(rs.getString("lesson_name"));

                quesList.add(ques);

            }
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
            if (quesList != null) {
                for (int i = 0; i < quesList.size(); i++) {
                    quesList.get(i).setOptions(getOptionsById(Integer.parseInt(quesList.get(i).getId())));
                }
            }
        }
        return quesList;
    }

    // Tham chiếu bới id của mỗi question
    private ArrayList<String> getOptionsById(int id) {
        ArrayList<String> list = new ArrayList<>();
        String query = "SELECT TOP (1000) [QuestionId]\n"
                + "      ,[Option]\n"
                + "  FROM [dbo].[QuestionChoices] where QuestionId = " + id;

        try {
            conn = DBConnection.open();
            ps = conn.prepareCall(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                // Lấy toàn bộ những lựa chọn theo id của câu hỏi
                list.add(rs.getString("Option"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return list;
    }

}
