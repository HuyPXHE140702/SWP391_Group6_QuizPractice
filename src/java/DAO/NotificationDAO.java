/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Notification;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class NotificationDAO {
    public int createNotification(Notification n) {
        Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
        try {
            conn = DBConnection.open();
            String query = " insert into [QuizPractice].[dbo].[Notification] values (?,?,GETDATE()) ";
            System.out.println(n.getTitle());

            ps = conn.prepareStatement(query);
            ps.setString(1, n.getTitle());
            ps.setString(2, n.getDes());

            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return 0;
    }
}
