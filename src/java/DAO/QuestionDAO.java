/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Entity.QuestionBank;
import Entity.QuestionChoices;
import Entity.QuizTaken;
import Entity.Quizz;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class QuestionDAO extends DBContext {

    public static void main(String[] args) {
        QuestionDAO QuestionDAO = new QuestionDAO();
//        ArrayList<QuestionChoices> options = QuestionDAO.getQuestionChoies("where qc.QuestionId = " + 2);
//        System.out.println("" + options.get(0).getOption());

        Quizz q = new Quizz();
        q.setU_id(1072);
        q.setTopic_id(2);
        q.setGrade(6.05);
        q.setTime("0:0:34");

        QuestionDAO.createQuestionBank("test", "result", 2, "description");
//        
//        QuestionDAO.createQuestionChoise(7, "ressqies");
        QuestionBank qb = QuestionDAO.getNewQuestionByTopicId(2);

        System.out.println("" + qb.getId());
    }

    public void deleteQuestionBank(int ques_id) {
        try {

            deleteQuestionChoise(ques_id);

            String sql = "delete QuestionBank\n"
                    + "where b_id = ?; ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, ques_id);

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateQuestionBank(int ques_id, String question, String result, String description) {
        try {
            String sql = "update QuestionBank\n"
                    + "set [question] = ?,\n"
                    + "[result] = ?,\n"
                    + "[description] =?\n"
                    + "where [b_id] =? ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, question);
            stm.setString(2, result);
            stm.setInt(4, ques_id);
            stm.setString(3, description);

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public void createQuestionBank(String question, String result, int topic_id, String description) {
        try {
            String sql = "insert into QuestionBank\n"
                    + "(\n"
                    + "[question],\n"
                    + "[result],\n"
                    + "[topic_id],\n"
                    + "[description],\n"
                    + "[createdTime]\n"
                    + ")\n"
                    + "values (?, ?, ?, ?, getdate())\n"
                    + "; ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, question);
            stm.setString(2, result);
            stm.setInt(3, topic_id);
            stm.setString(4, description);

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<QuestionBank> getQuestionBanksByTopic(int topic_id) {
        ArrayList<QuestionBank> questions = getQuestionBanks("where qb.topic_id = "+ topic_id);
        
        return questions;
    }
    
     public ArrayList<QuestionBank> getQuestionBanksDoQues(int topic_id) {
                ArrayList<QuestionBank> questions = getQuestionBanks("where qb.topic_id = " + topic_id + " order by newid();");
                return questions;
     }
     
    // Get Ques
    public ArrayList<QuestionBank> getQuestionBanks(String condition) {
        ArrayList<QuestionBank> questions = new ArrayList<>();
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from QuestionBank qb";
            } else {
                sql = "select * from QuestionBank qb " + condition;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                QuestionBank q = new QuestionBank();
                q.setId(rs.getInt("b_id"));
                q.setQuestion(rs.getString("question"));
                q.setResult(rs.getString("result"));
                q.setDescription(rs.getString("description"));
                q.setCreated_date(rs.getString("createdTime"));
                // topic

                //Question Option
                ArrayList<QuestionChoices> options = getQuestionChoiesByQues(q.getId());
                q.setQuestionChoices(options);

                questions.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return questions;
    }

    public void createQuestionChoise(int questionBank, String option) {
        try {
            String sql = "insert into QuestionChoices\n"
                    + "(\n"
                    + "[questionId],\n"
                    + "[Option],\n"
                    + "[createdTime]\n"
                    + ")\n"
                    + "values (?, ?, getdate())  ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, questionBank); //quizId
            stm.setString(2, option);

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteQuestionChoise(int questionBank) {
        try {
            String sql = "delete QuestionChoices\n"
                    + "where [QuestionId] = ? ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, questionBank);

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Close() {

    }

    // Get Ques
    public QuestionBank getNewQuestionByTopicId(int topic_id) {
        try {

            String sql = "select top 1 * \n"
                    + "from QuestionBank qb \n"
                    + "where qb.topic_id = ? \n"
                    + "order by qb.createdTime desc";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, topic_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                QuestionBank q = new QuestionBank();
                q.setId(rs.getInt("b_id"));
                q.setQuestion(rs.getString("question"));
                q.setResult(rs.getString("result"));
                q.setDescription(rs.getString("description"));
                q.setCreated_date(rs.getString("createdTime"));
                // topic

                //Question Option
//                ArrayList<QuestionChoices> options = getQuestionChoies("where qc.QuestionId = " + q.getId());
//                q.setQuestionChoices(options);

                return q;

            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    //
    public ArrayList<QuestionChoices> getQuestionChoiesByQues(int ques_id) {
        ArrayList<QuestionChoices> options = getQuestionChoies("where qc.QuestionId = " + ques_id);
        return options;
    }
    
    // Get Ques
    public QuestionBank getQuestionBank(int ques_id) {
        try {

            String sql = "select * \n"
                    + "from QuestionBank qb \n"
                    + "where qb.b_id = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, ques_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                QuestionBank q = new QuestionBank();
                q.setId(rs.getInt("b_id"));
                q.setQuestion(rs.getString("question"));
                q.setResult(rs.getString("result"));
                q.setDescription(rs.getString("description"));
                q.setCreated_date(rs.getString("createdTime"));
                // topic

                //Question Option
                    ArrayList<QuestionChoices> options = getQuestionChoiesByQues(q.getId());
                q.setQuestionChoices(options);

                return q;

            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<QuestionChoices> getQuestionChoies(String condition) {
        ArrayList<QuestionChoices> options = new ArrayList<>();
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from QuestionChoices qc";
            } else {
                sql = "select * from QuestionChoices qc " + condition;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                QuestionChoices q = new QuestionChoices();
                q.setId(rs.getInt("QuestionId"));
                q.setOption(rs.getString("Option"));
                q.setCreated_date(rs.getString("createdTime"));

                options.add(q);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return options;
    }

    // Take Question
    public void createQuizz(Quizz q) {
        try {
            String sql = "insert into Quiz\n"
                    + "([u_id],\n"
                    + "[topic_id],\n"
                    + "[required_time],\n"
                    + "[taken_date],\n"
                    + "[score],\n"
                    + "[status],\n"
                    + "[condition],\n"
                    + "[time]\n"
                    + ")\n"
                    + "values(?, ?, 30, getdate(), ?, 1, ?, ?);  ";

//            String sql = "insert into Quiz\n"
//                    + "([u_id],\n"
//                    + "[topic_id],\n"
//                    + "[required_time],\n"
//                    + "[taken_date],\n"
//                    + "[score],\n"
//                    + "[status],\n"
//                    + "[condition],\n"
//                    + "[time]\n"
//                    + ")\n"
//                    + "values(1072, 2, 30, getdate(), 6.5, 1, 1, '0:0:34');  ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, q.getU_id());
            stm.setInt(2, q.getTopic_id());

            stm.setDouble(3, q.getGrade());

            stm.setInt(4, (q.getGrade() >= 5) ? 1 : 0);
            stm.setString(5, q.getTime());

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Quizz getNewQuizzByUser(int u_id) {
        try {
            String sql = "select top 1 * from Quiz q where q.u_id = ? order by q.taken_date desc ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, u_id);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Quizz q = new Quizz();
                q.setQ_id(rs.getInt("q_id"));
                q.setU_id(u_id);
                q.setTopic_id(rs.getInt("topic_id"));
                q.setRequired_time(rs.getString("required_time"));
                q.setTaken_date(rs.getString("taken_date"));
                q.setScore(rs.getFloat("score"));
                q.setStatus(rs.getInt("status"));
                q.setCondition(rs.getInt("condition"));
                q.setTime(rs.getString("time"));

                return q;
            }

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void createQuizzTake(QuizTaken qt) {
        try {
            String sql = "insert into QuizTaken\n"
                    + "([q_id],\n"
                    + "[b_id],\n"
                    + "[question],\n"
                    + "[question_result],\n"
                    + "[question_explain],\n"
                    + "[user_result],\n"
                    + "[status]\n"
                    + ")\n"
                    + "values(?, ?, ?, ?, ?, ?, ?);  ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, qt.getQ_id()); //quizId
            stm.setInt(2, qt.getQuestionBank().getId());
            stm.setString(3, qt.getQuestion());
            stm.setString(4, qt.getQuestion_explain());
            stm.setString(5, qt.getQuestion_result());

            stm.setString(6, qt.getUser_result());
            stm.setInt(7, qt.getStatus());

            stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
