/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Blog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phamb
 */
public class BlogDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<Blog> getBlogs(int status) {
        ArrayList<Blog> blogs = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (status == 1) {
                ps = conn.prepareCall("SELECT * FROM [QuizPractice].[dbo].[Blog]"
                        + " where status = 1 order by time desc ");
            } else {
                ps = conn.prepareCall("SELECT * FROM [QuizPractice].[dbo].[Blog] order by time desc ");
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt(1));
                b.setTitle(rs.getString(2));
                b.setTypeId(rs.getInt(3));
                b.setAuthor(rs.getInt(4));
                b.setDate(rs.getDate(5).toString());
                b.setType(rs.getString(6));
                b.setStatus(rs.getInt(7));
                b.setContent(rs.getString(8));
                b.setModifier(rs.getInt(9));
                b.setDescription(rs.getString(10));
                b.setImage(rs.getString(11));
                blogs.add(b);
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return blogs;
    }

    public ArrayList<Blog> GetBlogs(int mode) {
        ArrayList<Blog> bl = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (mode == 1) {
                ps = conn.prepareCall("SELECT * FROM [dbo].[Blog] where status = 1 order by time desc ");
            } else {
                ps = conn.prepareCall("SELECT * FROM [dbo].[Blog] order by time desc ");
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                //nu standfor new-user
                Blog b = new Blog();
                b.setId(rs.getInt(1));
                b.setTitle(rs.getString(2));
                b.setTypeId(rs.getInt(3));
                b.setAuthor(rs.getInt(4));
                b.setDate(rs.getDate(5).toString());
                b.setType(rs.getString(6));
                b.setStatus(rs.getInt(7));
                b.setContent(rs.getString(8));
                b.setModifier(rs.getInt(9));
                b.setDescription(rs.getString(10));
                b.setImage(rs.getString(11));
                bl.add(b);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return bl;
    }

    public Blog getBlogById(int id) {
        ArrayList<Blog> blogs = this.getBlogs(0);
        for (Blog blog : blogs) {
            if (blog.getId() == id) {
                return blog;
            }
        }
        return null;
    }

    public ArrayList<Blog> getBlogsByCategory(String category) {
        ArrayList<Blog> blogs = new ArrayList<>();
        try {
            conn = DBConnection.open();
            ps = conn.prepareCall("SELECT * FROM [QuizPractice].[dbo].[Blog] "
                    + "where type like " + "'" + category + "'" + "order by time desc ");
            rs = ps.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId(rs.getInt(1));
                b.setTitle(rs.getString(2));
                b.setTypeId(rs.getInt(3));
                b.setAuthor(rs.getInt(4));
                b.setDate(rs.getDate(5).toString());
                b.setType(rs.getString(6));
                b.setStatus(rs.getInt(7));
                b.setContent(rs.getString(8));
                b.setModifier(rs.getInt(9));
                b.setDescription(rs.getString(10));
                b.setImage(rs.getString(11));
                blogs.add(b);
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return blogs;
    }

    public ArrayList<String> getBlogCategory() {
        ArrayList<String> cates = new ArrayList<>();
        try {
            conn = DBConnection.open();
            ps = conn.prepareCall("SELECT distinct type from blog");
            rs = ps.executeQuery();
            while (rs.next()) {
                String temp = rs.getString("type");
                cates.add(temp);
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return cates;
    }
}
