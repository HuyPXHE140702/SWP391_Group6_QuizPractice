/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Lesson;
import Entity.Topic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class LessonDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public static void main(String[] args) {
        LessonDAO dao = new LessonDAO();
//        System.out.println("" + dao.getLesson("and [Subject].s_id = 2 and  Lesson.topic_id =5").get(0).getVideo());
        System.out.println("" + dao.getOnlyLesson("2").getTopicName());
    }

    public Lesson getOnlyLesson(String l_id) {
        try {
            conn = DBConnection.open();

            String query = "select *, Lesson.l_id as lesson_id, Lesson.[name] as lesson_name,\n"
                    + "                        Lesson.[image] as lesson_image,\n"
                    + "                        Lesson.[description] as lesson_description,\n"
                    + "                        Lesson.[status] as lesson_status,\n"
                    + "                        Lesson.topic_id as lesson_topic,\n"
                    + "                        Lesson.createdTime as lesson_created,\n"
                    + "                        LessonTopic.[name] as topic_name \n"
                    + "from [Subject] \n"
                    + " left join LessonTopic on   [Subject].s_id = LessonTopic.s_id \n"
                    + " left join [Lesson] on  Lesson.topic_id = LessonTopic.topic_id\n"
                    + "\n"
                    + "WHERE LessonTopic.[status]=1 and Lesson.l_id = ? ;";

            ps = conn.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(l_id));
            rs = ps.executeQuery();

            while (rs.next()) {
                Lesson ls = new Lesson();
                ls.setL_id(rs.getInt("lesson_id"));
                ls.setName(rs.getString("lesson_name"));
                ls.setNo(rs.getInt("no"));
                ls.setVideo(rs.getString("video"));
                ls.setImage(rs.getString("lesson_image"));
                ls.setContent(rs.getString("content"));
                ls.setDescription(rs.getString("lesson_description"));

                ls.setStatus(rs.getInt("lesson_status"));
                ls.setReferences(rs.getString("references"));
                ls.setDocuments(rs.getString("documents"));
                ls.setTopic_id(rs.getInt("lesson_topic"));
                ls.setCreatedTime(rs.getString("lesson_created"));
                ls.setTopicName(rs.getString("topic_name"));
                return ls;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return null;
    }

    public List<Lesson> getLesson(String condition) {
        List<Lesson> lessons = new ArrayList<>();
        try {
            conn = DBConnection.open();

            String query = "select *, Lesson.l_id as lesson_id, Lesson.[name] as lesson_name,\n"
                    + "                        Lesson.[image] as lesson_image,\n"
                    + "                        Lesson.[description] as lesson_description,\n"
                    + "                        Lesson.[status] as lesson_status,\n"
                    + "                        Lesson.topic_id as lesson_topic,\n"
                    + "                        Lesson.createdTime as lesson_created,\n"
                    + "                        LessonTopic.[name] as topic_name \n"
                    + "from [Subject] \n"
                    + " left join LessonTopic on   [Subject].s_id = LessonTopic.s_id \n"
                    + " left join [Lesson] on  Lesson.topic_id = LessonTopic.topic_id\n"
                    + "\n"
                    + "WHERE LessonTopic.[status]=1 ";

            if (!condition.isEmpty()) {
                query = query + condition + "";
            } else {

            }
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Lesson ls = new Lesson();
                ls.setL_id(rs.getInt("lesson_id"));
                ls.setName(rs.getString("lesson_name"));
                ls.setNo(rs.getInt("no"));
                ls.setVideo(rs.getString("video"));
                ls.setImage(rs.getString("lesson_image"));
                ls.setContent(rs.getString("content"));
                ls.setDescription(rs.getString("lesson_description"));

                ls.setStatus(rs.getInt("lesson_status"));
                ls.setReferences(rs.getString("references"));
                ls.setDocuments(rs.getString("documents"));
                ls.setTopic_id(rs.getInt("lesson_topic"));
                ls.setCreatedTime(rs.getString("lesson_created"));
                ls.setTopicName(rs.getString("topic_name"));
                lessons.add(ls);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return lessons;
    }

    public List<Topic> getTopic(String condition) {
        List<Topic> topics = new ArrayList<>();
        try {
            conn = DBConnection.open();

            if (condition.isEmpty()) {
                ps = conn.prepareStatement("select * from [LessonTopic]");
            } else {
                ps = conn.prepareStatement("select * from [LessonTopic] " + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Topic sc = new Topic();
                sc.setS_id(rs.getInt("s_id"));
                sc.setName(rs.getString("name"));
                sc.setStatus(rs.getInt("status"));
                sc.setTopic_id(rs.getInt("topic_id"));
                topics.add(sc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return topics;
    }

}
