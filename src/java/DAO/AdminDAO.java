/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Money;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class AdminDAO extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public void updateUserStatus(int userID, int status) {
        String query = "UPDATE [dbo].[User]\n"
                + "   SET [status] = " + status + "\n"
                + " WHERE [u_id] = " + userID;
        try {
            conn = new DBConnection().open();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void updateSlideStatus(int id, int status) {
        String query = "UPDATE [dbo].[Slider]\n"
                + "   SET [sl_status] = " + status + "\n"
                + " WHERE [sl_id] = " + id;
        try {
            conn = new DBConnection().open();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void updateSlideDetail(int id, String heading, String content, String image, String begin, String end, String link) {
        String query = "UPDATE [dbo].[Slider]\n"
                + "   SET [sl_heading] = ?\n"
                + "      ,[sl_content] = ?\n"
                + "      ,[sl_image] = ?\n"
                + "      ,[sl_link] = ?\n"
                + "      ,[sl_begin] = ?\n"
                + "      ,[sl_end] = ?\n"
                + " WHERE [sl_id] = ?";
        try {
            conn = new DBConnection().open();
            ps = conn.prepareStatement(query);
            ps.setString(1, heading);
            ps.setString(2, content);
            ps.setString(3, image);
            ps.setString(4, link);
            ps.setString(5, begin);
            ps.setString(6, end);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void updateSubjectStatus(int id, int status) {
        String query = "UPDATE [dbo].[Subject]\n"
                + "   SET [status] = " + status + "\n"
                + " WHERE [s_id] = " + id;
        try {
            conn = new DBConnection().open();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void updateSubjectDetail(int id, String name, int type, String des, String orga, int priceId, int price) {
        String query = "UPDATE [dbo].[Subject]\n"
                + "   SET [name] = ?\n"
                + "      ,[typeID] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[organization] = ?\n"
                + " WHERE [s_id] = ?";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setInt(2, type);
            ps.setString(3, des);
            ps.setString(4, orga);
            ps.setInt(5, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        updatePrice(priceId, price);
    }

    public void updatePrice(int id, int price) {
        String query = "UPDATE [dbo].[PricePackage]\n"
                + "   SET [price] = ?\n"
                + " WHERE [price_id] =?";
        try {
            conn = new DBConnection().open();
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ps.setInt(2, price);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public Money getTotalMoneySubjectRegis(String condition) {
        Money totalMoney = new Money();
        String query = "SELECT SUM(pp.price) AS sumPri\n"
                + "FROM [dbo].[SubjectRegister] AS sr\n"
                + "    JOIN [dbo].[PricePackage] AS pp\n"
                + "        ON pp.price_id = sr.price_id;";
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    totalMoney.setTimeMoney("all the time");
                    totalMoney.setTotalMoney(rs.getInt("sumPri"));
                }
            } else {
                ps = conn.prepareStatement(query + "\n" + condition);
                rs = ps.executeQuery();
                while (rs.next()) {
                    totalMoney.setTimeMoney("month");
                    totalMoney.setTotalMoney(rs.getInt("sumPri"));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return totalMoney;
    }

    public Money getTotalMoneyBySubject(String condition) {
        Money totalMoney = new Money();

        try {
            String sql = "select sum(pp.price) AS sumPri\n"
                    + "from [dbo].[SubjectRegister] sr\n"
                    + "	join [dbo].[PricePackage] pp on pp.price_id = sr.price_id \n"
                    + "	join [dbo].[Subject] s on s.s_id = pp.price_id  ";

            PreparedStatement stm = connection.prepareStatement(sql);

            ResultSet srs = stm.executeQuery();
            if (!condition.isEmpty()) {
                sql = sql.concat(condition);
            }

            while (srs.next()) {
                totalMoney.setTimeMoney("all the time");
                totalMoney.setTotalMoney(srs.getInt("sumPri"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return totalMoney;
    }

    public Money getTotalMoneyASubjectRegis(int s_id, String condition) {
        Money totalMoney = new Money();
        String query = "SELECT SUM(pp.price) AS sumPri\n"
                + "FROM [dbo].[SubjectRegister] AS sr\n"
                + "    JOIN [dbo].[PricePackage] AS pp\n"
                + "        ON pp.price_id = sr.price_id and pp.s_id=?;";
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, s_id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    totalMoney.setTimeMoney("all the time");
                    totalMoney.setTotalMoney(rs.getInt("sumPri"));
                }
            } else {
                ps = conn.prepareStatement(query + "\n" + condition);
                ps.setInt(1, s_id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    totalMoney.setTimeMoney("month");
                    totalMoney.setTotalMoney(rs.getInt("sumPri"));
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return totalMoney;
    }

    public void addNewSlide(String nsheading, String nscontent, String nsimage, String nsbegin, String nsend, String nslink) {
        int status = 1;
        String query = "INSERT INTO [dbo].[Slider]\n"
                + "           ([sl_heading]\n"
                + "           ,[sl_content]\n"
                + "           ,[sl_image]\n"
                + "           ,[sl_link]\n"
                + "           ,[sl_status]\n"
                + "           ,[sl_begin]\n"
                + "           ,[sl_end])\n"
                + "     VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, nsheading);
            ps.setString(2, nscontent);
            ps.setString(3, nsimage);
            ps.setString(4, nslink);
            ps.setInt(5, status);
            ps.setString(6, nsbegin);
            ps.setString(7, nsend);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void changeEnd(int id, int status) {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = dateTime.format(formatter);
        if (status == 0) {
            formattedDateTime = dateTime.minusMonths(1).format(formatter);
        } else {
            formattedDateTime = dateTime.plusMonths(1).format(formatter);
        }

        String query = "UPDATE [dbo].[Slider]\n"
                + "   SET [sl_end] = ?\n"
                + " WHERE [sl_id] =?";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, formattedDateTime);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void updateSlideShow(int id, int show) {
        String query = "UPDATE [dbo].[Slider]\n"
                + "   SET [sl_show] = " + show + "\n"
                + " WHERE [sl_id] = " + id;
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void addNewSubSubCate(int oldSubCategoryInt, String newSubSubCat) {
        int status = 1;
        String query = "INSERT INTO [dbo].[SubjectType]\n"
                + "           ([name]\n"
                + "           ,[cat_id]\n"
                + "           ,[status]\n"
                + "           ,[createdTime])\n"
                + "     VALUES (?,?,?,GETDATE())";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, newSubSubCat);
            ps.setInt(2, oldSubCategoryInt);
            ps.setInt(3, status);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public void addNewSubCateAndSubSubCate(String newSubCategory, String newSubSubCat) {
        int status = 1;
        String query = "INSERT INTO [dbo].[SubjectCategory]\n"
                + "           ([name]\n"
                + "           ,[status]\n"
                + "           ,[createdTime])\n"
                + "     VALUES (?, ?, GETDATE())";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, newSubCategory);
            ps.setInt(2, status);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        addNewSubSubCate(getSubCateIdByName(newSubCategory), newSubSubCat);
    }

    public int getSubCateIdByName(String newSubCategory) {
        String query = "SELECT [cat_id] FROM [dbo].[SubjectCategory] WHERE [name] = ?";
        int subId = 0;
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, newSubCategory);
            rs = ps.executeQuery();
            while (rs.next()) {
                subId = rs.getInt("cat_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subId;
    }

    public int getSubSubCatIdByName(String subSubCateName) {
        String query = "SELECT [typeID] FROM [dbo].[SubjectType] WHERE [name] = ?";
        int typeID = 0;
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, subSubCateName);
            rs = ps.executeQuery();
            while (rs.next()) {
                typeID = rs.getInt("typeID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return typeID;
    }

    public void addNewSubject(String newSubName, int typeId, int instructor_id, String newSubImage, String newSubDes, String newSubOrg, String newSubImageOrg) {
        int status = 1;
        String query = "INSERT INTO [dbo].[Subject]\n"
                + "           ([name]\n"
                + "           ,[typeID]\n"
                + "           ,[instructor_id]\n"
                + "           ,[description]\n"
                + "           ,[status]\n"
                + "           ,[image]\n"
                + "           ,[organization]\n"
                + "           ,[logo]\n"
                + "           ,[createdTime])\n"
                + "     VALUES (?, ?, ?, ?, ?, ?, ?, ?, GETDATE())";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, newSubName);
            ps.setInt(2, typeId);
            ps.setInt(3, instructor_id);
            ps.setString(4, newSubDes);
            ps.setInt(5, status);
            ps.setString(6, newSubImage);
            ps.setString(7, newSubOrg);
            ps.setString(8, newSubImageOrg);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    public int getSubIdByName(String newSubName) {
        String query = "SELECT TOP(1) [s_id] FROM dbo.[Subject] WHERE [name] =? ORDER BY [s_id] DESC";
        int subId = 0;
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setString(1, newSubName);
            rs = ps.executeQuery();
            while (rs.next()) {
                subId = rs.getInt("s_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subId;
    }

    public void addNewPrice(int subIdByName, int newSubPri) {
        int status = 1;
        String unit = "usd";
        String query = "INSERT INTO [dbo].[PricePackage]\n"
                + "           ([s_id]\n"
                + "           ,[price]\n"
                + "           ,[status]\n"
                + "           ,[currency_unit]\n"
                + "           ,[createdTime])\n"
                + "     VALUES (?, ?, ?, ?, GETDATE())";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setInt(1, subIdByName);
            ps.setInt(2, newSubPri);
            ps.setInt(3, status);
            ps.setString(4, unit);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }
    
    public void updateSale(int sub_id, int price, int sale) {
        offStatusPrice(sub_id);
        int status = 1;
        String current_unit = "usd";
        String query = "INSERT INTO [dbo].[PricePackage]\n"
                + "           ([s_id]\n"
                + "           ,[price]\n"
                + "           ,[status]\n"
                + "           ,[currency_unit]\n"
                + "           ,[discount]\n"
                + "           ,[createdTime])\n"
                + "     VALUES\n"
                + "           (?, ?, ?, ?, ? ,GETDATE())";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setInt(1, sub_id);
            ps.setInt(2, price);
            ps.setInt(3, status);
            ps.setString(4, current_unit);
            ps.setInt(5, sale);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }

    private void offStatusPrice(int sub_id) {
        int status = 0;
        String query = "UPDATE [dbo].[PricePackage]\n"
                + "   SET [status] = ?\n"
                + " WHERE [s_id] = ?";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.setInt(1, status);
            ps.setInt(2, sub_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }
}
