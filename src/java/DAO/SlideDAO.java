/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Slide;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phamb
 */
public class SlideDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
    }

    public ArrayList<Slide> getSlides(int status) {
        ArrayList<Slide> slides = new ArrayList<>();
        updateStatusSlide();
        try {
            conn = DBConnection.open();
            if (status == 1) {
                ps = conn.prepareCall("SELECT [sl_id]\n"
                        + "      ,[sl_heading]\n"
                        + "      ,[sl_content]\n"
                        + "      ,[sl_image]\n"
                        + "      ,[sl_link]\n"
                        + "      ,[sl_status]\n"
                        + "      ,[sl_show]\n"
                        + "      ,[sl_begin]\n"
                        + "      ,[sl_end]\n"
                        + "  FROM [dbo].[Slider]\n"
                        + "  WHERE [sl_status] = 1");
            } else {
                ps = conn.prepareCall("SELECT [sl_id]\n"
                        + "      ,[sl_heading]\n"
                        + "      ,[sl_content]\n"
                        + "      ,[sl_image]\n"
                        + "      ,[sl_link]\n"
                        + "      ,[sl_status]\n"
                        + "      ,[sl_show]\n"
                        + "      ,[sl_begin]\n"
                        + "      ,[sl_end]\n"
                        + "  FROM [dbo].[Slider]");
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Slide s = new Slide();
                s.setSl_id(rs.getInt("sl_id"));
                s.setSl_heading(rs.getString("sl_heading"));
                s.setSl_content(rs.getString("sl_content"));
                s.setSl_image(rs.getString("sl_image"));
                s.setSl_link(rs.getString("sl_link"));
                s.setSl_status(rs.getInt("sl_status"));
                s.setSl_show(rs.getInt("sl_show"));
                s.setSl_begin(rs.getString("sl_begin"));
                s.setSl_end(rs.getString("sl_end"));
                slides.add(s);
            }
        } catch (SQLException e) {
            Logger.getLogger(SlideDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return slides;
    }

    public Slide getSlideById(int sl_id) {
        updateStatusSlide();
        try {
            conn = DBConnection.open();
            ps = conn.prepareCall("SELECT [sl_id]\n"
                    + "      ,[sl_heading]\n"
                    + "      ,[sl_content]\n"
                    + "      ,[sl_image]\n"
                    + "      ,[sl_link]\n"
                    + "      ,[sl_status]\n"
                    + "      ,[sl_show]\n"
                    + "      ,[sl_begin]\n"
                    + "      ,[sl_end]\n"
                    + "  FROM [dbo].[Slider]\n"
                    + "  WHERE [sl_id] = " + sl_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Slide s = new Slide();
                s.setSl_id(rs.getInt("sl_id"));
                s.setSl_heading(rs.getString("sl_heading"));
                s.setSl_content(rs.getString("sl_content"));
                s.setSl_image(rs.getString("sl_image"));
                s.setSl_link(rs.getString("sl_link"));
                s.setSl_id(rs.getInt("sl_status"));
                s.setSl_show(rs.getInt("sl_show"));
                s.setSl_begin(rs.getString("sl_begin"));
                s.setSl_end(rs.getString("sl_end"));
                return s;
            }
        } catch (SQLException e) {
            Logger.getLogger(SlideDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return null;
    }

    public void updateStatusSlide() {
        String query = "DECLARE @getDate SMALLDATETIME = (CAST(CAST(GETDATE() AS DATE) AS SMALLDATETIME));\n"
                + "UPDATE [dbo].[Slider]\n"
                + "SET [sl_status] = CASE\n"
                + "                      WHEN [sl_begin] < @getDate\n"
                + "                           AND [sl_end] > @getDate THEN\n"
                + "                          1\n"
                + "                      ELSE\n"
                + "                          0\n"
                + "                  END;\n";
        try {
            conn = DBConnection.open();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
            System.out.println("Update status of slide successfully");
        } catch (SQLException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
    }
}
