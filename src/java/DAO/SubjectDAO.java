/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnection.DBConnection;
import Entity.Lesson;
import Entity.Price;
import Entity.Subject;
import Entity.SubjectCat;
import Entity.SubjectRegister;
import Entity.SubjectType;
import Entity.Topic;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.coyote.ActionCode;

/**
 *
 * @author Admin
 */
public class SubjectDAO extends DBContext {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    UserDAO userDAO = new UserDAO();
    QuestionDAO questionDAO = new QuestionDAO();

    public List<Subject> getSubjects(String condition) {
        List<Subject> subjects = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement("select * from [QuizPractice].[dbo].[Subject] WHERE status=1 ");
            } else {
                ps = conn.prepareStatement("select * from [Subject] " + condition + " and status = 1");
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                //structor
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setInstructor(userDAO.GetUserById(rs.getInt("instructor_id")));

                // type
                s.setType_id(rs.getInt("typeID"));
                s.setSubjectType(getSubjectType1(rs.getInt("typeID")));
                // price packpage
                //s.setPricePackage(getPricePackage(rs.getInt("s_id")));

                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));

                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }

    public List<SubjectCat> getSubjectCat(String condition) {
        List<SubjectCat> cats = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement("select * from [QuizPractice].[dbo].[SubjectCategory]");
            } else {
                ps = conn.prepareStatement("select * from [SubjectCategory]" + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                SubjectCat sc = new SubjectCat();
                sc.setCatId(rs.getInt("cat_id"));
                sc.setName(rs.getString("name"));
                sc.setStatus(rs.getInt("status"));
                cats.add(sc);
            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return cats;
    }
    ////DA 

    public Subject getSubject(int s_id) {
        try {

            String sql = "select * from [Subject] s where s.s_id = ? ";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, s_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                //structor
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setInstructor(userDAO.GetUserById(rs.getInt("instructor_id")));

                // type
                s.setType_id(rs.getInt("typeID"));
                s.setSubjectType(getSubjectType1(rs.getInt("typeID")));
                // price packpage
//                s.setPricePackage(getPricePackage(rs.getInt("s_id")));

                // topic
                s.setTopics(getSubjectTopics("where ls.s_id = " + s.getS_id()));

                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));

                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Subject getSubject(String subjectId, String condition) {
        try {

            String sql = "select * from [Subject] s where s.s_id = ? and s.[status] = 1";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, Integer.parseInt(subjectId));
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                //structor
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setInstructor(userDAO.GetUserById(rs.getInt("instructor_id")));

                // type
                s.setType_id(rs.getInt("typeID"));
                s.setSubjectType(getSubjectType1(rs.getInt("typeID")));
                // price packpage
//                s.setPricePackage(getPricePackage(rs.getInt("s_id")));

                // topic
                s.setTopics(getSubjectTopics("where ls.s_id = " + s.getS_id()));

                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));

                return s;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<SubjectCat> getSubjectCategories(String condition) {
        ArrayList<SubjectCat> cats = new ArrayList<>();
        try {
            String sql;
            if (condition.isEmpty() || condition == null) {
                sql = "select * from SubjectCategory sc ";
            } else {
                sql = "select * from SubjectCategory sc " + condition;
            }

            PreparedStatement stm = connection.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                SubjectCat sc = new SubjectCat();
                sc.setCatId(rs.getInt("cat_id"));
                sc.setName(rs.getString("name"));
                sc.setStatus(rs.getInt("status"));

                cats.add(sc);

            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return cats;
    }

    public SubjectCat getSubjectCategory(int cat_id) {
        try {
            String sql = "select * from SubjectCategory sc where sc.cat_id = ?;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, cat_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                SubjectCat sc = new SubjectCat();
                sc.setCatId(rs.getInt("cat_id"));
                sc.setName(rs.getString("name"));
                sc.setStatus(rs.getInt("status"));

                return sc;

            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public ArrayList<SubjectType> getSubjectTypes(String condition) {
        ArrayList<SubjectType> types = new ArrayList<>();
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from SubjectType st ";
            } else {
                sql = "select * from SubjectType st  " + condition;
            }

            PreparedStatement stm = connection.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                SubjectType st = new SubjectType();
                st.setId(rs.getInt("typeID"));
                st.setStatus(rs.getInt("status"));
                st.setName(rs.getString("name"));
                //
                st.setCat_id(rs.getInt("cat_id"));
                st.setSubjectCategory(getSubjectCategory(rs.getInt("cat_id")));

                types.add(st);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //DBConnection.close(conn, ps, rs);
        }
        return types;
    }

    public SubjectType getSubjectType1(int type_Id) {

        try {
            String sql = "select * from SubjectType st where st.typeID = ? ";

            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, type_Id);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                SubjectType st = new SubjectType();
                st.setId(rs.getInt("typeID"));
                st.setStatus(rs.getInt("status"));
                st.setName(rs.getString("name"));
                //
                st.setCat_id(rs.getInt("cat_id"));
                st.setSubjectCategory(getSubjectCategory(rs.getInt("cat_id")));

                return st;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //DBConnection.close(conn, ps, rs);
        }
        return null;
    }

    public Price getPricePackage(int price_Id) {

        try {
            String sql = "select top 1 * from PricePackage pp where pp.price_id = ? Order by pp.createdTime desc";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, price_Id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Price p = new Price();
                p.setPrice_id(rs.getInt("price_id"));
                p.setPrice(rs.getInt("price"));
                p.setCurrency_unit(rs.getString("currency_unit"));
                p.setStatus(rs.getInt("status"));
                p.setDiscount(rs.getString("discount"));
                p.setSubject(getSubject(rs.getInt("s_id")));
                return p;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Topic> getSubjectTopics(String condition) {
        ArrayList<Topic> topics = new ArrayList<>();
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from LessonTopic ls";
            } else {
                sql = "select * from LessonTopic ls " + condition;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Topic t = new Topic();
                t.setTopic_id(rs.getInt("topic_id"));
                t.setName(rs.getString("name"));
                t.setDescription(rs.getString("description"));
                t.setPurpose(rs.getString("purpose"));
                // lessons
                t.setLessons(getTopicLessons(" where l.topic_id = " + t.getTopic_id()));
                // Question
                t.setQuestions(questionDAO.getQuestionBanks("where qb.topic_id = " + t.getTopic_id()));

                t.setStatus(rs.getInt("status"));
                t.setS_id(rs.getInt("s_id"));
                topics.add(t);

            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return topics;
    }

    public Topic getSubjectTopic(int topic_id, String condition) {
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from LessonTopic ls where ls.topic_id=?";
            } else {
                sql = "select * from LessonTopic ls where ls.topic_id=? " + condition;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, topic_id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Topic t = new Topic();
                t.setTopic_id(rs.getInt("topic_id"));
                t.setName(rs.getString("name"));

                // lessons
                t.setLessons(getTopicLessons(" where l.topic_id = " + t.getTopic_id()));
                // Question
                t.setQuestions(questionDAO.getQuestionBanksByTopic(t.getTopic_id()));

                t.setStatus(rs.getInt("status"));
                t.setS_id(rs.getInt("s_id"));
                return t;

            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public ArrayList<Lesson> getTopicLessons(String condition) {
        ArrayList<Lesson> lessons = new ArrayList<>();
        try {
            String sql;

            if (condition.isEmpty() || condition == null) {
                sql = "select * from Lesson l order by l.[no]";
            } else {
                sql = "select * from Lesson l " + condition + " order by l.[no]";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setL_id(rs.getInt("l_id"));
                l.setName(rs.getString("name"));
                l.setNo(rs.getInt("no"));
                l.setVideo(rs.getString("video"));
                l.setImage(rs.getString("image"));
                l.setContent(rs.getString("content"));
                l.setDescription(rs.getString("description"));
                l.setReferences(rs.getString("references"));
                l.setDocuments(rs.getString("documents"));

                l.setStatus(rs.getInt("status"));

                lessons.add(l);

            }
        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return lessons;
    }

    // Topic Manager
    public void createTopicLesson(int subject_Id, String topic_name, String topic_description, String topic_purpose) {
        try {
            String sql = "insert into LessonTopic\n"
                    + "(\n"
                    + "[name],\n"
                    + "[description],\n"
                    + "[purpose],\n"
                    + "[s_id],\n"
                    + "[createdTime],\n"
                    + "[status]\n"
                    + ")\n"
                    + "values(?, ?, ?, ?, getdate(),0); ";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setString(1, topic_name);
            stm.setString(2, topic_description);
            stm.setString(3, topic_purpose);
            stm.setInt(4, subject_Id);
            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void editTopicLesson(int topic_Id, String topic_Name, String topic_description, String topic_purpose) {
        try {
            String sql = "update [LessonTopic]\n"
                    + "set [name] = ?,\n"
                    + "[description] = ?,\n"
                    + "[purpose] = ?\n"
                    + "where topic_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, topic_Name);
            stm.setString(2, topic_description);
            stm.setString(3, topic_purpose);
            stm.setInt(4, topic_Id);
            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void editTopicStatus(int topic_Id, int status) {
        try {
            String sql = "UPDATE [LessonTopic]\n"
                    + "SET [status] = ?\n"
                    + "WHERE topic_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, topic_Id);
            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    // Manage Topic

//    lesson_id : inputs[1].value,
//                    lesson_name :  inputs[2].value,
//                    lesson_no : inputs[3].value,
//                    lesson_video : inputs[4].value,
//                    lesson_image : inputs[5].value,
//                    lesson_content : inputs[5].value,
//                    lesson_description : inputs[6].value,
//                    lesson_references : inputs[7].value,
//                    lesson_documents : inputs[8].value
    public static void main(String[] args) {
        SubjectDAO subjectDAO = new SubjectDAO();
        ArrayList<Subject> subs = subjectDAO.getOaiSubjects("", "Admin");
        for (Subject sub : subs) {
            System.out.println("s_id: "+ sub.getS_id());
        }
//        System.out.println("" + subjectDAO.getSubject("10").getName());
//        System.out.println("" + subjectDAO.getPricePackage(10).getPrice());
//        System.out.println("" + subjectDAO.getSubjectCategory(1).getName());
//        System.out.println("" + subjectDAO.getSubjectType1(1).getName());
//        System.out.println("" + subjectDAO.getTopicLessons("").get(0).getName());
//        subjectDAO.createLesson(3, "name", 0, "vie", "img", "conte", "dess", "refer", "documen");
        //System.out.println(""+ subjectDAO.getRegisterByUserId(1072).get(0).getPriceP().getPrice());

    }

    public void createLesson(int t_id, String lesson_name, int lesson_no, String lesson_video, String lesson_image,
            String lesson_content, String lesson_description, String lesson_references, String lesson_documents) {
        try {
            String sql = "insert into [Lesson]\n"
                    + "(\n"
                    + "[name],\n"
                    + "[no],\n"
                    + "[video],\n"
                    + "[image],\n"
                    + "[content],\n"
                    + "[description],\n"
                    + "[status],\n"
                    + "[references],\n"
                    + "[documents],\n"
                    + "[topic_id],\n"
                    + "[createdTime]\n"
                    + ")\n"
                    + "values (?, ?, ?, ?, ?, ?, 0, ?, ?, ?, getdate());";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, lesson_name);
            stm.setInt(2, lesson_no);
            stm.setString(3, lesson_video);
            stm.setString(4, lesson_image);
            stm.setString(5, lesson_content);
            stm.setString(6, lesson_description);
            stm.setString(7, lesson_references);
            stm.setString(8, lesson_documents);
            stm.setInt(9, t_id);

            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void editLessonDetail(int l_id, String lesson_name, int lesson_no, String lesson_video, String lesson_image,
            String lesson_content, String lesson_description, String lesson_references, String lesson_documents) {
        try {
            String sql = "update Lesson\n"
                    + "set \n"
                    + "[name] = ?,\n"
                    + "[no] = ?,\n"
                    + "[video] = ?,\n"
                    + "[image] = ?,\n"
                    + "[content] = ?,\n"
                    + "[description] = ?,\n"
                    + "[references] = ?,\n"
                    + "[documents] = ?\n"
                    + "where [l_id] = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, lesson_name);
            stm.setInt(2, lesson_no);
            stm.setString(3, lesson_video);
            stm.setString(4, lesson_image);
            stm.setString(5, lesson_content);
            stm.setString(6, lesson_description);
            stm.setString(7, lesson_references);
            stm.setString(8, lesson_documents);
            stm.setInt(9, l_id);

            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void editLessonStatus(int l_id, int status) {
        try {
            String sql = "update Lesson\n"
                    + "set [status]= ?\n"
                    + "where [l_id] = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, status);
            stm.setInt(2, l_id);

            stm.execute();

        } catch (SQLException e) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    ///////////////////////////DA///////////////////////////////////////////////////////////////////////////////////////////////////  
    public List getSubjectType(String condition) {
        List<SubjectType> types = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement("select * from [QuizPractice].[dbo].[SubjectType]");
            } else {
                ps = conn.prepareStatement("select * from [SubjectType] " + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                SubjectType st = new SubjectType();
                st.setId(rs.getInt("typeID"));
                st.setStatus(rs.getInt("status"));
                st.setName(rs.getString("name"));
                st.setCat_id(rs.getInt("cat_id"));
                types.add(st);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return types;
    }

    public ArrayList<Lesson> getSimpleLessons(String condition) {
        ArrayList<Lesson> lessonList = new ArrayList<>();
        try {
            conn = DBConnection.open();
            String query = "select * from [Lesson]";
            if (condition.isEmpty()) {
                ps = conn.prepareStatement(query);
            } else {
                ps = conn.prepareStatement(query + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setL_id(rs.getInt("l_id"));
                l.setName(rs.getString("name"));
                lessonList.add(l);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return lessonList;
    }

//    public List<Subject> getCompleteSubjects() {
//        List<Subject> subjects = new ArrayList<>();
//        try {
//            conn = DBConnection.open();
//            String query = "select [Subject].*,[PricePackage].price,[PricePackage].currency_unit,[PricePackage].discount,[User].first_name,[User].last_name from [Subject] "
//                    + "join [PricePackage] on [Subject].s_id=[PricePackage].s_id "
//                    + "join [User] on [Subject].instructor_id=[User].u_id "
//                    + " where [PricePackage].[status]=1";
//
//            ps = conn.prepareStatement(query);
//
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                Subject s = new Subject();
//                s.setS_id(rs.getInt("s_id"));
//                s.setName(rs.getString("name"));
//                s.setImage(rs.getString("image"));
//                s.setInstructor_id(rs.getInt("instructor_id"));
//                s.setDescription(rs.getString("description"));
//                s.setLogo(rs.getString("logo"));
//                s.setOrganization(rs.getString("organization"));
//                s.setStatus(rs.getInt("status"));
//                s.setType_id(rs.getInt("typeID"));
//                s.setPrice(rs.getInt("price"));
//                s.setDiscount(rs.getInt("discount"));
//                s.setCurrency_unit(rs.getString("currency_unit"));
//                s.setInstructor_first_name(rs.getString("first_name"));
//                s.setInstructor_last_name(rs.getString("last_name"));
//
//                subjects.add(s);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            DBConnection.close(conn, ps, rs);
//        }
//        return subjects;
//    }

        public List<Subject> getCompleteSubjects() {
        List<Subject> subjects = new ArrayList<>();
        try {
            conn = DBConnection.open();
            String query = "SELECT	Sub.[name], Sub.[typeID], Sub.[instructor_id], Sub.[description], Sub.[status], \n"
                    + "		Sub.[image], Sub.[organization], Sub.[logo], Sub.[s_id], Sub.[createdTime],\n"
                    + "		Typ.[name],\n"
                    + "		Cate.[cat_id], Cate.[name] AS [cat_name],\n"
                    + "		Pri.[price_id], Pri.[price], Pri.[currency_unit], Pri.[discount],\n"
                    + "		Us.[first_name], Us.[last_name]\n"
                    + "  FROM [dbo].[Subject] AS Sub\n"
                    + "  JOIN [dbo].[SubjectType] AS Typ ON Typ.typeID = Sub.typeID\n"
                    + "  JOIN [dbo].[SubjectCategory] AS Cate ON Cate.cat_id = Typ.cat_id\n"
                    + "  JOIN [dbo].[PricePackage] AS Pri ON Pri.s_id = Sub.s_id\n"
                    + "  JOIN [dbo].[User] AS Us ON Us.u_id = Sub.instructor_id\n"
                    + "  WHERE Pri.[status] = 1"
                    + " AND Sub.[status] = 1";

            ps = conn.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));
                s.setType_id(rs.getInt("typeID"));
                s.setPrice_id(rs.getInt("price_id"));
                s.setPrice(rs.getInt("price"));
                s.setCat_id(rs.getInt("cat_id"));
                s.setCat_name(rs.getString("cat_name"));
                s.setDiscount(rs.getInt("discount"));
                s.setCurrency_unit(rs.getString("currency_unit"));
                s.setInstructor_first_name(rs.getString("first_name"));
                s.setInstructor_last_name(rs.getString("last_name"));

                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }
    public ArrayList<Subject> getCompleteSubjects(String condition) {
        ArrayList<Subject> subjects = new ArrayList<>();
        try {
            conn = DBConnection.open();
            String query = "select [Subject].*,[PricePackage].price,[PricePackage].currency_unit,[PricePackage].discount,[User].first_name,[User].last_name from [Subject] "
                    + "join [PricePackage] on [Subject].s_id=[PricePackage].s_id "
                    + "join [User] on [Subject].instructor_id=[User].u_id "
                    + " where [PricePackage].[status]=1 and [Subject].[status]=1 ";
            if (condition.isEmpty()) {
                ps = conn.prepareStatement(query);
            } else {
                ps = conn.prepareStatement(query + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));
                s.setType_id(rs.getInt("typeID"));
                s.setPrice(rs.getInt("price"));
                s.setDiscount(rs.getInt("discount"));
                s.setCurrency_unit(rs.getString("currency_unit"));
                s.setInstructor_first_name(rs.getString("first_name"));
                s.setInstructor_last_name(rs.getString("last_name"));

                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }

    public List<SubjectRegister> getSubjectsRegisterOfExpert(String condition) {
        List<SubjectRegister> subjects = new ArrayList<>();

        try {
            conn = DBConnection.open();
            String query = " select SubjectRegister.u_id,Subject.description, Subject.name,Subject.typeID,Subject.instructor_id,Subject.image,Subject.logo,Subject.\n"
                    + "  organization, Subject.s_id, PricePackage.price, PricePackage.currency_unit,\n"
                    + "   SubjectRegister.[status], SubjectRegister.register_date\n"
                    + "  from ((PricePackage INNER JOIN [Subject] on [Subject].s_id=PricePackage.s_id) \n"
                    + "  inner join SubjectRegister on SubjectRegister.price_id= PricePackage.price_id)\n"
                    + "   where SubjectRegister.[status]='1'" + condition;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                SubjectRegister s = new SubjectRegister();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));
                s.setImage(rs.getString("image"));
                s.setInstructor_id(rs.getInt("instructor_id"));
                s.setDescription(rs.getString("description"));
                s.setLogo(rs.getString("logo"));
                s.setOrganization(rs.getString("organization"));
                s.setStatus(rs.getInt("status"));
                s.setType_id(rs.getInt("typeID"));
                s.setCurrency(rs.getString("currency_unit"));
                s.setDate(rs.getString("register_date"));
                s.setPrice(rs.getInt("price"));

                subjects.add(s);

            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }

    public List<SubjectRegister> getSubjectsRegisterByUser(String condition, int uid) {
        List<SubjectRegister> subjects = new ArrayList<>();
        String userCondition = "";
        if (uid > 0) {
            userCondition = "and SubjectRegister.u_id = " + uid;
        }

        try {
            conn = DBConnection.open();
            String query = " select SubjectRegister.u_id,Subject.description, Subject.name,Subject.typeID,Subject.instructor_id,Subject.image,Subject.logo,Subject.\n"
                    + "  organization, Subject.s_id, PricePackage.price, PricePackage.currency_unit,\n"
                    + "   SubjectRegister.[status], SubjectRegister.register_date\n"
                    + "  from ((PricePackage INNER JOIN [Subject] on [Subject].s_id=PricePackage.s_id) \n"
                    + "  inner join SubjectRegister on SubjectRegister.price_id= PricePackage.price_id)\n"
                    + "   where SubjectRegister.[status]='1'" + userCondition + condition;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs != null) {

                while (rs.next()) {

                    SubjectRegister s = new SubjectRegister();
                    s.setS_id(rs.getInt("s_id"));
                    s.setName(rs.getString("name"));
                    s.setImage(rs.getString("image"));
                    s.setInstructor_id(rs.getInt("instructor_id"));
                    s.setDescription(rs.getString("description"));
                    s.setLogo(rs.getString("logo"));
                    s.setOrganization(rs.getString("organization"));
                    s.setStatus(rs.getInt("status"));
                    s.setType_id(rs.getInt("typeID"));
                    s.setCurrency(rs.getString("currency_unit"));
                    s.setDate(rs.getString("register_date"));
                    s.setPrice(rs.getInt("Price"));

                    subjects.add(s);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }

    public int updateSubject(Subject s) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String strDate = formatter.format(date);
        try {
            conn = DBConnection.open();
            String query = "";
            if (s.getName() != null) {
                query = "Update Subject "
                        + " SET [name] = '" + s.getName() + "', typeID = " + s.getType_id() + ", description ='" + s.getDescription() + "', status=" + s.getStatus() + ", image = '" + s.getImage() + "', organization='" + s.getOrganization() + "', logo='" + s.getLogo() + "'"
                        + " WHERE s_id=" + s.getS_id();
            } else {
                query = "Update Subject SET status = 0 " + " WHERE s_id=" + s.getS_id();

            }
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(RegisterDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return 0;
    }

//    public int updateSubject(Subject s) {
//        String query = "UPDATE [dbo].[Subject]\n"
//                + "   SET [name] = ?\n"
//                + "      ,[typeID] = ?\n"
//                + "      ,[instructor_id] = ?\n"
//                + "      ,[description] = ?\n"
//                + "      ,[status] = ?\n"
//                + "      ,[image] = ?\n"
//                + "      ,[organization] = ?\n"
//                + "      ,[logo] = <?\n"
//                + " WHERE ?";
//        try {
//            conn = DBConnection.open();
//            ps = conn.prepareStatement(query);
//            ps.setString(1, s.getName());
//            ps.setInt(2, s.getType_id());
//            ps.setInt(3, s.getInstructor_id());
//            ps.setString(4, s.getDescription());
//            ps.setInt(5, s.getStatus());
//            ps.setString(6, s.getImage());
//            ps.setString(7, s.getOrganization());
//            ps.setString(8, s.getLogo());
//            ps.setInt(9, s.getS_id());
//            ps.executeUpdate();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(RegisterDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            DBConnection.close(conn, ps, rs);
//        }
//        return 0;
//    }
    public int addSubject(Subject newSubject) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String strDate = formatter.format(date);
        try {
            conn = DBConnection.open();
            String query = "";
            if (newSubject.getName() != null) {
                query = "Insert into Subject (name, typeID, instructor_id, [description], [status], [image], organization, logo, createdTime) "
                        + " Values( '" + newSubject.getName() + "', " + newSubject.getType_id() + ", " + newSubject.getInstructor_id() + ", '" + newSubject.getDescription() + "', " + newSubject.getStatus() + ",  '" + newSubject.getImage() + "', '" + newSubject.getOrganization() + "', '" + newSubject.getLogo() + "', '" + strDate + "')";
            }
            ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();

            try ( ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(RegisterDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return 0;
    }

    public ArrayList<Subject> getSimpleSubjects(String condition) {
        ArrayList<Subject> subjects = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement("select * from [QuizPractice].[dbo].[Subject]");
            } else {
                ps = conn.prepareStatement("select * from [Subject] " + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setS_id(rs.getInt("s_id"));
                s.setName(rs.getString("name"));

                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subjects;
    }

    public ArrayList<Price> getPrice(String condition) {
        ArrayList<Price> prices = new ArrayList<>();
        try {
            conn = DBConnection.open();
            if (condition.isEmpty()) {
                ps = conn.prepareStatement("SELECT [price_id]\n"
                        + "      ,[s_id]\n"
                        + "      ,[price]\n"
                        + "      ,[status]\n"
                        + "      ,[currency_unit]\n"
                        + "      ,[discount]\n"
                        + "      ,[description]\n"
                        + "  FROM [dbo].[PricePackage]");
            } else {
                ps = conn.prepareStatement("SELECT [price_id]\n"
                        + "      ,[s_id]\n"
                        + "      ,[price]\n"
                        + "      ,[status]\n"
                        + "      ,[currency_unit]\n"
                        + "      ,[discount]\n"
                        + "      ,[description]\n"
                        + "  FROM [dbo].[PricePackage]" + condition);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Price p = new Price();
                p.setPrice_id(rs.getInt("price_id"));
                p.setPrice(rs.getInt("price"));
                p.setS_id(rs.getInt("s_id"));
                p.setStatus(rs.getInt("status"));
                p.setCurrency_unit(rs.getString("currency_unit"));
                p.setDiscount(rs.getString("discount"));
                p.setDescription(rs.getString("description"));
                prices.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return prices;
    }
    
    public ArrayList<SubjectCat> getCategoriesAndItsSub(String con1, String con2) {
        ArrayList<SubjectCat> cats = new ArrayList<>();
        try {
            String sql = "SELECT Cat.[cat_id], Cat.[name], Cat.[description], Cat.[image]\n"
                    + "		,COUNT(Sub.[s_id]) AS [num_of_sub]\n"
                    + "         FROM [dbo].[SubjectType] AS Typ\n"
                    + "         JOIN [dbo].[SubjectCategory] AS Cat ON Cat.cat_id = Typ.cat_id\n"
                    + "         JOIN [dbo].[Subject] AS Sub ON Sub.typeID = Typ.typeID\n"
                    + "        WHERE Cat.[status] = 1\n";
            if (con1.isEmpty()) {
                sql = sql + "GROUP BY Cat.[cat_id], Cat.[name], Cat.[description], Cat.[image]";
            } else {
                sql = sql + con1 + "\n"
                        + "GROUP BY Cat.[cat_id], Cat.[name], Cat.[description], Cat.[image]";
            }
            if (!con2.isEmpty()) {
                sql += con2;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                SubjectCat s = new SubjectCat();
                s.setCatId(result.getInt("cat_id"));
                s.setName(result.getString("name"));
                s.setDescription(result.getString("description"));
                s.setImage(result.getString("image"));
                s.setNum_of_sub(result.getInt("num_of_sub"));
                s.setTypes(getTypesAndItsSubject(" AND Cat.[cat_id] = " + s.getCatId(), "User"));
                cats.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cats;
    }

    public ArrayList<SubjectCat> getCategoriesAndItsType(String condition, String role) {
        ArrayList<SubjectCat> cats = new ArrayList<>();
        try {
            String sql = "SELECT Cat.[cat_id], Cat.[name],\n"
                    + "COUNT(Typ.[cat_id]) AS [num_of_subcate] \n"
                    + "FROM [dbo].[SubjectType] AS Typ\n"
                    + "JOIN [dbo].[SubjectCategory] AS Cat ON Cat.cat_id = Typ.cat_id\n"
                    + "WHERE Cat.[status] = 1\n";
            if (condition.isEmpty()) {
                sql = sql + "GROUP BY Cat.[cat_id], Cat.[name]";
            } else {
                sql = sql + condition + "\n"
                        + "GROUP BY Cat.[cat_id], Cat.[name]";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                SubjectCat s = new SubjectCat();
                s.setCatId(result.getInt("cat_id"));
                s.setName(result.getString("name"));
                s.setNum_of_subcate(result.getInt("num_of_subcate"));
                s.setTypes(getTypesAndItsSubject(" AND Cat.[cat_id] = " + s.getCatId(), role));
                cats.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cats;
    }

    public ArrayList<SubjectType> getTypesAndItsSubject(String condition, String role) {
        ArrayList<SubjectType> types = new ArrayList<>();
        try {
            String sql = "SELECT Typ.[typeID], Typ.[name], Cat.[cat_id],\n"
                    + "COUNT(Sub.[s_id]) AS [num_of_sub]\n"
                    + "FROM [dbo].[SubjectType] AS Typ\n"
                    + "LEFT JOIN [dbo].[SubjectCategory] AS Cat ON Cat.cat_id = Typ.cat_id\n"
                    + "LEFT JOIN [dbo].[Subject] AS Sub ON Sub.typeID = Typ.typeID\n"
                    + "WHERE Typ.[status] = 1\n";
            if (condition.isEmpty()) {
                sql = sql + "GROUP BY Typ.[typeID], Typ.[name], Cat.[cat_id]\n";
            } else {
                sql = sql + condition + "\n"
                        + "GROUP BY Typ.[typeID], Typ.[name], Cat.[cat_id]";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                SubjectType s = new SubjectType();
                s.setId(result.getInt("typeID"));
                s.setName(result.getString("name"));
                s.setCat_id(result.getInt("cat_id"));
                s.setNum_of_sub(result.getInt("num_of_sub"));
                s.setSubjects(getOaiSubjects("AND Typ.[typeID] = " + s.getId(), role));
                types.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return types;
    }

    public ArrayList<Subject> getOaiSubjects(String condition, String role) {
        ArrayList<Subject> subjects = new ArrayList<>();
        try {
            String sql = "SELECT	Sub.[name], Sub.[typeID], Sub.[instructor_id], Sub.[description], Sub.[status], \n"
                    + "		Sub.[image], Sub.[organization], Sub.[logo], Sub.[s_id], Sub.[createdTime],\n"
                    + "		Typ.[name],\n"
                    + "		Cate.[cat_id], Cate.[name] AS [cat_name],\n"
                    + "		Pri.[price_id], Pri.[price], Pri.[currency_unit], Pri.[discount],\n"
                    + "		Us.[first_name], Us.[last_name], Us.[role], Us.[avatar]\n"
                    + "FROM [dbo].[Subject] AS Sub\n"
                    + "JOIN [dbo].[SubjectType] AS Typ ON Typ.typeID = Sub.typeID\n"
                    + "JOIN [dbo].[SubjectCategory] AS Cate ON Cate.cat_id = Typ.cat_id\n"
                    + "JOIN [dbo].[PricePackage] AS Pri ON Pri.s_id = Sub.s_id\n"
                    + "JOIN [dbo].[User] AS Us ON Us.u_id = Sub.instructor_id\n"
                    + "WHERE Pri.[status] = 1\n";
            if (role.equals("User")) {
                sql = sql + " AND Sub.[status] = 1\n";
            }
            if (!condition.isEmpty()) {
                sql = sql + condition;
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Subject s = new Subject();
                s.setS_id(result.getInt("s_id"));
                s.setNum_regis_sub(getNumRegisSub(s.getS_id()));
                s.setName(result.getString("name"));
                s.setImage(result.getString("image"));
                s.setInstructor_id(result.getInt("instructor_id"));
                s.setDescription(result.getString("description"));
                s.setLogo(result.getString("logo"));
                s.setOrganization(result.getString("organization"));
                s.setStatus(result.getInt("status"));
                s.setType_id(result.getInt("typeID"));
                s.setPrice_id(result.getInt("price_id"));
                s.setPrice(result.getInt("price"));
                s.setCat_id(result.getInt("cat_id"));
                s.setCat_name(result.getString("cat_name"));
                s.setDiscount(result.getInt("discount"));
                s.setCurrency_unit(result.getString("currency_unit"));
                s.setInstructor_first_name(result.getString("first_name"));
                s.setInstructor_last_name(result.getString("last_name"));
                s.setInstructor_avatar(result.getString("avatar"));
                s.setInstructor_role(result.getString("role"));
                subjects.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return subjects;
    }

    public ArrayList<Subject> getTopRegisSubject(int top) {
        ArrayList<Subject> subs = new ArrayList<>();
        String sql = "SELECT TOP(?)	Pri.[s_id], COUNT(Pri.[s_id]) AS [num_regis_sub],\n"
                + "                Sub.[name] AS [sub_name], Sub.[typeID], Sub.[instructor_id], Sub.[description], \n"
                + "                Sub.[status], Sub.[image], Sub.[organization], Sub.[logo], Typ.[name] AS [type_name],\n"
                + "                Cat.[cat_id], Cat.[name] AS [cat_name], Us.[u_id], Us.[first_name], Us.[last_name], Us.[role], Us.[avatar]\n"
                + "FROM [dbo].[SubjectRegister] AS SubR\n"
                + "JOIN [dbo].[PricePackage] AS Pri ON Pri.price_id = SubR.price_id\n"
                + "JOIN [dbo].[Subject] AS Sub ON Sub.s_id = Pri.s_id\n"
                + "JOIN [dbo].[SubjectType] AS Typ ON Typ.typeID = Sub.typeID\n"
                + "JOIN [dbo].[SubjectCategory] AS Cat ON Cat.cat_id = Typ.cat_id\n"
                + "JOIN [dbo].[User] AS Us ON Us.u_id = Sub.instructor_id\n"
                + "GROUP BY	Pri.[s_id], Sub.[name], Sub.[typeID], Sub.[instructor_id], Sub.[description], \n"
                + "			Sub.[status], Sub.[image], Sub.[organization], Sub.[logo], Typ.[name],\n"
                + "			Cat.[cat_id], Cat.[name], Us.[u_id], Us.[first_name], Us.[last_name], Us.[role], Us.[avatar]\n"
                + "ORDER BY [num_regis_sub] DESC";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, top);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                Subject s = new Subject();
                s.setS_id(result.getInt("s_id"));
                s.setNum_regis_sub(result.getInt("num_regis_sub"));
                s.setName(result.getString("sub_name"));
                s.setType_id(result.getInt("typeID"));
                s.setInstructor_id(result.getInt("instructor_id"));
                s.setDescription(result.getString("description"));
                s.setStatus(result.getInt("status"));
                s.setImage(result.getString("image"));
                s.setOrganization(result.getString("organization"));
                s.setLogo(result.getString("logo"));
                s.setCat_id(result.getInt("cat_id"));
                s.setCat_name(result.getString("cat_name"));
                s.setInstructor_id(result.getInt("u_id"));
                s.setInstructor_first_name(result.getString("first_name"));
                s.setInstructor_last_name(result.getString("last_name"));
                s.setInstructor_role(result.getString("role"));
                s.setInstructor_avatar(result.getString("avatar"));
                subs.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return subs;
    }

    public int getNumRegisSub(int s_id) {
        Subject sub = new Subject();
        String sql = "SELECT Pri.[s_id], COUNT(Pri.[s_id]) AS [num_regis_sub]\n"
                + "FROM [dbo].[SubjectRegister] AS SubR\n"
                + "JOIN [dbo].[PricePackage] AS Pri ON Pri.price_id = SubR.price_id\n"
                + "WHERE pri.[s_id] = ?\n"
                + "GROUP BY Pri.[s_id]";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, s_id);
            ResultSet result = stm.executeQuery();
            while (result.next()) {
                sub.setS_id(result.getInt("s_id"));
                sub.setNum_regis_sub(result.getInt("num_regis_sub"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DBConnection.close(conn, ps, rs);
        }
        return sub.getNum_regis_sub();
    }
}
