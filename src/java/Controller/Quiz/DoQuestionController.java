/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Quiz;

import DAO.QuestionDAO;
import DAO.RegisterDAO;
import DAO.SubjectDAO;
import Entity.QuestionBank;
import Entity.QuizTaken;
import Entity.Quizz;
import Entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jasper.tagplugins.jstl.core.ForEach;

/**
 *
 * @author ADMIN
 */
public class DoQuestionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DoQuestionController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DoQuestionController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String topic_id = request.getParameter("topic_id");
        String subID = request.getParameter("subID");

        // check Random url
        int s_id = 0;
        
            s_id = Integer.parseInt(subID);

            User u = (User) request.getSession().getAttribute("user");
            QuestionDAO questionDAO = new QuestionDAO();
            RegisterDAO RegisterDAO = new RegisterDAO();
            
            // check login
            if (u == null) {
                response.sendRedirect(request.getContextPath() + "/accountcontroller");

            } else {
                int check = RegisterDAO.checkSubjectRegisterByUserId(u.getId(), s_id);

                if (check > 0) {
                    // get random question
                    ArrayList<QuestionBank> questions = questionDAO.getQuestionBanksDoQues(Integer.parseInt(topic_id));

                    request.setAttribute("subject", subID);
                    request.setAttribute("topic", topic_id);
                    request.setAttribute("questions", questions);
                    request.getRequestDispatcher("ViewsDA/doQuestion.jsp").forward(request, response);
                } else {
                    response.sendRedirect(request.getContextPath() + "/home");
                }
            }

        

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String topic = request.getParameter("topic_id");
        QuestionDAO questionDAO = new QuestionDAO();
        PrintWriter out = response.getWriter();

        String time = request.getParameter("time");
        // get question
        ArrayList<QuestionBank> questions = questionDAO.getQuestionBanksByTopic(Integer.parseInt(topic));
        ArrayList<QuizTaken> quizTakes = new ArrayList<>();

        // Caculate final
        int point = 0;
        int quesNum = questions.size();
        for (QuestionBank q : questions) {
            String myOption = request.getParameter("answer" + q.getId());

            QuizTaken qt = new QuizTaken();
            qt.setQuestionBank(q);
            qt.setQuestion(qt.getQuestionBank().getQuestion());
            qt.setQuestion_result(qt.getQuestionBank().getResult());
            qt.setQuestion_explain(qt.getQuestionBank().getDescription());
            qt.setUser_result(myOption.trim());
            qt.setStatus(0);

            if (myOption != null) {
                if (myOption.trim().equals(q.getResult().trim())) {
                    point++;
                    qt.setStatus(1);
                }
            }
            quizTakes.add(qt);
        }

        double grade = ((double) point / quesNum) * 10;

//        User u = (User) request.getSession().getAttribute("user");
        String user_id = request.getParameter("u_id");
        int u_id = Integer.parseInt(user_id);

        // Input value create Quiz
        Quizz q = new Quizz();
        q.setU_id(u_id);
        q.setTopic_id(Integer.parseInt(topic));
        q.setGrade(grade);
        q.setTime(time);

        questionDAO.createQuizz(q);

        // Input value create QuizTaken
        q = questionDAO.getNewQuizzByUser(u_id);

        for (QuizTaken qt : quizTakes) {
            qt.setQ_id(q.getQ_id());
            questionDAO.createQuizzTake(qt);
        }
        
        response.sendRedirect(request.getContextPath()+"/practicelist");
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//out.println("Option = "+ myOption );
//                        out.println("Result = "+ q.getResult());
//                        out.println(" <br/>");
//                        
}
