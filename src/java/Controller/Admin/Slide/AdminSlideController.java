/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Slide;

import DAO.AdminDAO;
import DAO.SlideDAO;
import Entity.Slide;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phamb
 */
public class AdminSlideController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminSlideController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminSlideController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SlideDAO sd = new SlideDAO();
        HttpSession session = request.getSession(true);
        session.setAttribute("slideData", sd.getSlides(0));
        request.getRequestDispatcher("/views/admin/homescreen/slide_list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String slupSta = request.getParameter("supSta");
        AdminDAO aDAO = new AdminDAO();
        // update status của slide nếu có supSta
        if (slupSta != null) {
            int status = Integer.parseInt(request.getParameter("sstatus"));
            int id = Integer.parseInt(request.getParameter("sid"));
            aDAO.updateSlideStatus(id, status);
            System.out.println("update status slide ok");
            // thêm hoặc bớt ngày kết thúc so với ngày hiện tại
            aDAO.changeEnd(id, status);
        } else {
            String slupSho = request.getParameter("supSho");
            //update show thời gian của slide nếu có supShow
            if (slupSho != null) {
                int show = Integer.parseInt(request.getParameter("sshow"));
                int id = Integer.parseInt(request.getParameter("sid"));
                aDAO.updateSlideShow(id, show);
                System.out.println("update show slide ok");
            } else {
                String ns = request.getParameter("ns");
                // thêm slide mới
                if (ns != null) {
                    String nsheading = request.getParameter("nsheading");
                    String nscontent = request.getParameter("nscontent");
                    String nsimage = request.getParameter("nsimage");
                    String nsbegin = request.getParameter("nsbegin");
                    nsbegin = nsbegin + ":00";
                    String nsend = request.getParameter("nsend");
                    nsend = nsend + ":00";
                    String nslink = request.getParameter("nslink");
                    aDAO.addNewSlide(nsheading, nscontent, nsimage, nsbegin, nsend, nslink);
                } else {
                    //update slide detail
                    int id = Integer.parseInt(request.getParameter("sid"));
                    String heading = request.getParameter("sheading");
                    String content = request.getParameter("scontent");
                    String image = request.getParameter("simage");
                    String link = request.getParameter("slink");
                    String begin = request.getParameter("sbegin");
                    begin = begin + ":00";
                    String end = request.getParameter("send");
                    end = end + ":00";
                    System.out.println(begin);
                    aDAO.updateSlideDetail(id, heading, content, image, begin, end, link);
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
