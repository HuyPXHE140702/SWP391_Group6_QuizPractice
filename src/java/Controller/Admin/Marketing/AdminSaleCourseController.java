/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Marketing;

import DAO.AdminDAO;
import DAO.EmailDAO;
import DAO.SubjectDAO;
import Entity.Email;
import Entity.Money;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phamb
 */
public class AdminSaleCourseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminSaleCourseController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminSaleCourseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EmailDAO eDAO = new EmailDAO();
        ArrayList<Email> listMailSent = eDAO.getEmails();
        request.setAttribute("listMailSent", listMailSent);
        SubjectDAO sd = new SubjectDAO();
        //setAttribute for categorys
        List cats = sd.getCategoriesAndItsType("", "Admin");
        request.setAttribute("cats", cats);
        //setAttribute for types
//        List types = sd.getTypesAndItsSubject("");
//        request.setAttribute("types", types);
//        //setAttribute for price
//        List prices = sd.getPrice("");
//        request.setAttribute("prices", prices);
        //setAttribute for totalMoneySubjectRegis
        AdminDAO aDAO = new AdminDAO();
        Money totalMoney = aDAO.getTotalMoneySubjectRegis("");
        request.setAttribute("totalMoney", totalMoney);
//        request.setAttribute("subjectData", sd.getCompleteSubjects());
        request.getRequestDispatcher("/views/admin/marketing/salecourse.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int sub_id = Integer.parseInt(request.getParameter("sub_id"));
        int price = Integer.parseInt(request.getParameter("price"));
        int sale = Integer.parseInt(request.getParameter("sale"));
        AdminDAO aDAO = new AdminDAO();
        aDAO.updateSale(sub_id, price, sale);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
