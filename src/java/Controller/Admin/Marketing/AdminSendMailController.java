/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Marketing;

import DAO.EmailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Phamb
 */
public class AdminSendMailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminSendMailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminSendMailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EmailDAO eDAO = new EmailDAO();
        // Get email id that user want to use that email
        String eid = request.getParameter("eid");
        System.out.println("eid:\t" + eid);
        //Get parameter
        String nsubject = request.getParameter("nsubject");
        String ncontent = request.getParameter("ncontent");
        String ndiscription = request.getParameter("ndiscription");
        System.out.println("Email:\n" + "Subject:\t"
                + nsubject + "\nContent:\t" + ncontent + "\nDescription:\t" + ndiscription);
        // Send and add new mail if user choose new email
        if (eid.equals("newEmail") || eid.isEmpty()) {
            System.out.println("Add email");
                        eDAO.addNewEmail(nsubject, ncontent, ndiscription);
        } else {
            System.out.println("\nUser choose already email");
        }
        // Get all user mail
        ArrayList<String> listMails = eDAO.getEmailOfUser("[u_id] = 44 OR [u_id] = 1045");
        // Start send mail to user
        System.out.println("Send mail");
        try {
            sendmail.SendMail.sendmaillist(listMails, nsubject, ncontent);
        } catch (MessagingException ex) {
            Logger.getLogger(AdminSendMailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
