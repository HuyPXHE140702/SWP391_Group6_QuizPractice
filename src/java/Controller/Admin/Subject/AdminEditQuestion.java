/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Subject;

import DAO.QuestionDAO;
import DAO.SubjectDAO;
import Entity.QuestionBank;
import Entity.Subject;
import Entity.Topic;
import Entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class AdminEditQuestion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            //Admin/Subject/Topic/Question
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminEditQuestion</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminEditQuestion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        User a = (User) request.getSession().getAttribute("user");
        if (a == null) {
            response.sendRedirect(request.getContextPath() + "/accountcontroller");
        } else {
            if (!a.getRole().trim().toLowerCase().equals("admin")) {
                response.sendRedirect(request.getContextPath() + "/home");
            } else {
                String topic_id = request.getParameter("topic_id").trim();
                int t_id = Integer.parseInt(topic_id);

                SubjectDAO SubjectDAO = new SubjectDAO();
                Topic topic = SubjectDAO.getSubjectTopic(t_id, "");

                request.setAttribute("topic", topic);
                request.getRequestDispatcher("/views/admin/subject/allTopicQuestion.jsp").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        response.setContentType("text/html;charset=UTF-8");

        QuestionDAO QuestionDAO = new QuestionDAO();

        String action = request.getParameter("action");

        switch (action) {
            case "add": {
                //get Info
                String topic_id = request.getParameter("topic_id").trim();
                int t_id = Integer.parseInt(topic_id);

                String question = request.getParameter("question").trim();
                String question_result = request.getParameter("question_result").trim();
                String question_description = request.getParameter("question_description").trim();

                ArrayList<String> question_option = new ArrayList<>();

                for (int i = 1; i <= 3; i++) {
                    String option = request.getParameter("question_option" + i).trim();
                    question_option.add(option);
                }

//                System.out.println("" + question + "///" + question_result + "///" + t_id + "///" + question_description);
                // add Quesbank
                QuestionDAO.createQuestionBank(question, question_result, t_id, question_description);

                QuestionBank qb = QuestionDAO.getNewQuestionByTopicId(t_id);
                QuestionDAO.createQuestionChoise(qb.getId(), qb.getResult());
                for (String option : question_option) {
                    QuestionDAO.createQuestionChoise(qb.getId(), option);
                }
                question_option.clear();
                break;
            }
            case "edit": {
                //get info
                String question_id = request.getParameter("question_id").trim();
                int ques_id = Integer.parseInt(question_id);
                String question = request.getParameter("question").trim();
                String question_result = request.getParameter("question_result").trim();
                String question_description = request.getParameter("question_description").trim();

                ArrayList<String> question_option = new ArrayList<>();

                for (int i = 1; i <= 3; i++) {
                    String option = request.getParameter("question_option" + i).trim();
                    question_option.add(option);
                }

                //edit Quesbank
                QuestionDAO.updateQuestionBank(ques_id, question, question_result, question_description);
                QuestionBank qb = QuestionDAO.getQuestionBank(ques_id);
                QuestionDAO.deleteQuestionChoise(ques_id);
                QuestionDAO.createQuestionChoise(qb.getId(), qb.getResult());
                for (String option : question_option) {
                    QuestionDAO.createQuestionChoise(qb.getId(), option);
                }
                break;
            }
            case "delete": {

                String question_id = request.getParameter("question_id");
                int ques_id = Integer.parseInt(question_id);
                QuestionDAO.deleteQuestionChoise(ques_id);
                QuestionDAO.deleteQuestionBank(ques_id);
                break;
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
