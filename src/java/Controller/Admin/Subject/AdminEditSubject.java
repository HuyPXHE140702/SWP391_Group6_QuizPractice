/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Subject;

import DAO.AdminDAO;
import DAO.SubjectDAO;
import Entity.Money;
import Entity.Subject;
import Entity.User;
import com.sun.org.apache.xalan.internal.xsltc.util.IntegerArray;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class AdminEditSubject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminEditSubject</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminEditSubject at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");

//            select * from [Subject] s where s.[status] = 1
//select * from PricePackage
//select * from LessonTopic
//selecT * from Lesson
//select * from QuestionBank
//select * from QuestionChoices	
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        User a = (User) request.getSession().getAttribute("user");
        if (a == null) {
            response.sendRedirect(request.getContextPath() + "/accountcontroller");
        } else {
            if (!a.getRole().trim().toLowerCase().equals("admin")) {
                response.sendRedirect(request.getContextPath() + "/home");
            } else {

                String id = request.getParameter("s_id");
                int s_id = Integer.parseInt(id);
                SubjectDAO SubjectDAO = new SubjectDAO();
                Subject subject = SubjectDAO.getSubject(s_id);

                request.setAttribute("subject", subject);

                //setAttribute for totalMoneySubjectRegis
                AdminDAO aDAO = new AdminDAO();
                Money totalMoney = aDAO.getTotalMoneyBySubject(" and s_id = " + s_id);
                request.setAttribute("totalMoney", totalMoney);

                request.getRequestDispatcher("../../views/admin/subject/allTopic.jsp").forward(request, response);
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            SubjectDAO SubjectDAO = new SubjectDAO();

            switch (action) {
                case "add": {
                    out.print("add + ");

                    String subject_id = request.getParameter("s_id");
                    int s_id = Integer.parseInt(subject_id);

                    String topic_name = request.getParameter("topic_name");
                    String topic_description = request.getParameter("topic_description");
                    String topic_purpose = request.getParameter("topic_purpose");
                    SubjectDAO.createTopicLesson(s_id, topic_name, topic_description, topic_purpose);
                    break;
                }

                case "edit": {
                    out.print("eda");
                    String topic_id = request.getParameter("topic_id");
                    int t_id = Integer.parseInt(topic_id);

                    String topic_name = request.getParameter("topic_name");
                    String topic_description = request.getParameter("topic_description");
                    String topic_purpose = request.getParameter("topic_purpose");
                    SubjectDAO.editTopicLesson(t_id, topic_name, topic_description, topic_purpose);
                    break;

                }

                case "status": {
                    out.print("ss");
                    String topic_id = request.getParameter("topic_id");
                    int t_id = Integer.parseInt(topic_id);

                    int status = 0;
//                    if(request.getParameter("status")==null || request.getParameter("status").isEmpty()){
                    if (request.getParameter("status").equalsIgnoreCase("ON")) {
                        status = 1;
                    }

                    SubjectDAO.editTopicStatus(t_id, status);
                    break;
                }

            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
