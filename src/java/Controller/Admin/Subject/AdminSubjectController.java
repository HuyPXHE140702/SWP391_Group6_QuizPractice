/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Subject;

import DAO.AdminDAO;
import DAO.SubjectDAO;
import Entity.Money;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Phamb
 */
public class AdminSubjectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminSubjectController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminSubjectController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO sd = new SubjectDAO();
        //setAttribute for categorys
        List cats = sd.getSubjectCat("");
        request.setAttribute("cats", cats);
        //setAttribute for types
        List types = sd.getSubjectType("");
        request.setAttribute("types", types);
        //setAttribute for price
        List prices = sd.getPrice("");
        request.setAttribute("prices", prices);
        //setAttribute for totalMoneySubjectRegis
        AdminDAO aDAO = new AdminDAO();
        Money totalMoney = aDAO.getTotalMoneySubjectRegis("");
        request.setAttribute("totalMoney", totalMoney);
        request.setAttribute("subjectData", sd.getOaiSubjects("", "Admin"));
        request.getRequestDispatcher("/views/admin/subject/subject.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String upSta = request.getParameter("sjupSta");
        AdminDAO aDAO = new AdminDAO();
        String newSub = request.getParameter("newSub");
        if (!newSub.equals("newSub")) {
            int id = Integer.parseInt(request.getParameter("sjid"));
            if (upSta != null) {
                int status = Integer.parseInt(request.getParameter("sjstatus"));
                aDAO.updateSubjectStatus(id, status);
            } else {
                String name = request.getParameter("sjname");
                int type = Integer.parseInt(request.getParameter("sjtype"));
                String des = request.getParameter("sjdes");
                String orga = request.getParameter("sjorga");
                int prid = Integer.parseInt(request.getParameter("prid"));
                int prprice = Integer.parseInt(request.getParameter("prprice"));
                aDAO.updateSubjectDetail(id, name, type, des, orga, prid, prprice);
            }
        } else {
            String newSubName = request.getParameter("newSubName");
            String newSubCategory = request.getParameter("newSubCategory");
            String newSubSubCat = request.getParameter("newSubSubCat");
            String newSubImage = request.getParameter("newSubImage");
            String newSubDes = request.getParameter("newSubDes");
            String newSubOrg = request.getParameter("newSubOrg");
            String newSubImageOrg = request.getParameter("newSubOrg");
            String priceString = request.getParameter("newSubPri");
            int instructor_id = 1;
            int typeID = 0;
            try {
                int oldSubCategoryInt = Integer.parseInt(newSubCategory);
                System.out.println("SubCategory old: " + oldSubCategoryInt);
                try {
                    int newSubSubCateInt = Integer.parseInt(newSubSubCat);
                    System.out.println("SubSubCate old: " + newSubSubCateInt);
                    // SubCate cũ, SubSubCate cũ ==> ko cần thêm vào database
                    typeID = newSubSubCateInt;
                } catch (NumberFormatException e) {
                    System.out.println("newSubSubCat cannot be parsed to Integer.");
                    // SubCate cũ, SubSubCate mới ==> thêm SubSubCate vào database
                    aDAO.addNewSubSubCate(oldSubCategoryInt, newSubSubCat);
                    System.out.println("Add Subject SubCategory Successfully!");
                    // lấy ra id của SubSubCate vừa thêm vào database
                    typeID = aDAO.getSubSubCatIdByName(newSubSubCat);
                }
            } catch (NumberFormatException e) {
                System.out.println("newSubCategory cannot be parsed to Integer.");
                // SubCategory mới, SubSubCate mới ==> thêm cả hai vào database
                aDAO.addNewSubCateAndSubSubCate(newSubCategory, newSubSubCat);
                System.out.println("Add Category and SubCategory Successfully!");
                // lấy ra id của SubSubCate vừa thêm vào database
                typeID = aDAO.getSubSubCatIdByName(newSubSubCat);
            }
            aDAO.addNewSubject(newSubName, typeID, instructor_id, newSubImage, newSubDes, newSubOrg, newSubImageOrg);
            System.out.println("Add Subject Successfully!");
            try {
                System.out.println("priceString: [" + priceString + "]");
                int newSubPri = Integer.parseInt(priceString);
                aDAO.addNewPrice(aDAO.getSubIdByName(newSubName), newSubPri);
                System.out.println("Add Price successfully");
            } catch (NumberFormatException e) {
                System.out.println("newSubPri cannot be parsed to Integer.");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
