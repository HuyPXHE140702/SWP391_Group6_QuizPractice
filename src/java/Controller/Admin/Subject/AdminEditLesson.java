/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin.Subject;

import DAO.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class AdminEditLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            ///Admin/Subject/Topic/Lesson
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminEditLesson</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminEditLesson at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
                response.setContentType("text/html;charset=UTF-8");


        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            SubjectDAO subjectDAO = new SubjectDAO();

            switch (action) {
                case "add": {

                    out.print("add + ");

                    String topic_id = request.getParameter("topic_id").trim();
                    int t_id = Integer.parseInt(topic_id);
                    
                    String lesson_name = request.getParameter("lesson_name").trim();
                    int lesson_no = Integer.parseInt(request.getParameter("lesson_no").trim());
                    String lesson_video = request.getParameter("lesson_video").trim();
                    String lesson_image = request.getParameter("lesson_image").trim();
                    String lesson_content = request.getParameter("lesson_content").trim();
                    String lesson_description = request.getParameter("lesson_description").trim();
                    String lesson_references = request.getParameter("lesson_references").trim();
                    String lesson_documents = request.getParameter("lesson_documents").trim();
                    
                    subjectDAO.createLesson(t_id, lesson_name, lesson_no, lesson_video, lesson_image, lesson_content, lesson_description, lesson_references, lesson_documents);
                    break;
                }

                case "edit": {

                    out.print("eda");
                    
                    String lesson_id = request.getParameter("lesson_id").trim();
                    int l_id = Integer.parseInt(lesson_id);
                    
                    String lesson_name = request.getParameter("lesson_name").trim();
                    int lesson_no = Integer.parseInt(request.getParameter("lesson_no").trim());
                    String lesson_video = request.getParameter("lesson_video").trim();
                    String lesson_image = request.getParameter("lesson_image").trim();
                    String lesson_content = request.getParameter("lesson_content").trim();
                    String lesson_description = request.getParameter("lesson_description").trim();
                    String lesson_references = request.getParameter("lesson_references").trim();
                    String lesson_documents = request.getParameter("lesson_documents").trim();

                    subjectDAO.editLessonDetail(l_id, lesson_name, lesson_no, lesson_video, lesson_image, lesson_content, lesson_description, lesson_references, lesson_documents);
                    break;

                }

                case "status": {
                    out.print("ss");
                    
                    String lesson_id = request.getParameter("lesson_id").trim();
                    int l_id = Integer.parseInt(lesson_id);
                    
                    int status = 0;
                    if (request.getParameter("status").trim().equals("0")) {
                        status = 1;
                    }
                    
                    subjectDAO.editLessonStatus(l_id, status);
                    
                    break;
                }

            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
