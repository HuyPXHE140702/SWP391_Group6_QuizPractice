/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.User;

import DAO.UserDAO;
import Entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class AccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            request.getRequestDispatcher("/views/common/login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try ( PrintWriter out = response.getWriter()) {

            boolean rem = "on".equals(request.getParameter("checked"));

            //getting information from login.jsp
            String user = request.getParameter("username");
            String pass = request.getParameter("password");
            if (user != null && pass != null) {

                UserDAO Dao = new UserDAO();

                //check account
                User u = Dao.getUser(new User(user, pass));
                if (u != null) {
                    request.getSession().setAttribute("user", u);

                    //Create cookie for account and password
                    Cookie user_cooky, pass_cooky;

                    //if user click "remeber me"
//                    if (rem) {
//                        //set age for cookies
//                        user_cooky = new Cookie("account", user);
//                        pass_cooky = new Cookie("password", pass);
//                        Cookie checkCookie = new Cookie("check", "check");
//                        user_cooky.setMaxAge(120);
//                        pass_cooky.setMaxAge(120);
//                        response.addCookie(user_cooky);
//                        response.addCookie(pass_cooky);
//                    }

                    if (u.getRole().equalsIgnoreCase("admin")) {
                        response.sendRedirect(request.getContextPath() + "/Admin"); //admin page
                    } else {
                        response.sendRedirect(request.getContextPath() + "/home");//customer page
                    }

                } else {
                    //if the login false then redirect to login page
                    request.setAttribute("mess", "Wrong user or password");
                    request.getRequestDispatcher("/views/common/login.jsp").forward(request, response);
                }

            } else {
                request.setAttribute("mess", "Wrong user or password");
                request.getRequestDispatcher("/views/common/login.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
