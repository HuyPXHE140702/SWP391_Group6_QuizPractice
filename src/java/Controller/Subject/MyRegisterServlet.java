/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Subject;

import DAO.RegisterDAO;
import DAO.SubjectDAO;
import Entity.SubjectCat;
import Entity.SubjectRegister;
import Entity.SubjectType;
import Entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class MyRegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyRegisterServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyRegisterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User u = (User) request.getSession().getAttribute("user");

        if (u != null) {
            SubjectDAO subjectDAO = new SubjectDAO();

            // List ComboBox
            ArrayList<SubjectCat> subjectCategories = subjectDAO.getSubjectCategories("");
            ArrayList<SubjectType> subjectTypes = subjectDAO.getSubjectTypes("");
            request.setAttribute("subjectCategories", subjectCategories);
            request.setAttribute("subjectTypes", subjectTypes);

            // Show Index 
            String st = request.getParameter("st");
            String sc = request.getParameter("sc");
            String search = request.getParameter("search");
            request.setAttribute("st", st);
            request.setAttribute("sc", sc);
            request.setAttribute("search", search);

            // Pagging 
            RegisterDAO registerDAO = new RegisterDAO();

            int totalrow = registerDAO.getCountRegisterByUserId(u.getId(), st, sc, search);
            int pagesize = 9;

            String page = request.getParameter("page");
            if (page == null || page.length() == 0 || page.equals("0")) {
                page = "1";
            }

            int pageindex = Integer.parseInt(page);

            int totalpage ;
            if(totalrow != 0){
                totalpage = ((totalrow % pagesize == 0) ? (totalrow / pagesize) : (totalrow / pagesize + 1));
            }else{
                totalpage = 1;
            }
            
            if (pageindex < 1) {
                    // if Page < 1 => Page = 1 
                response.sendRedirect(request.getContextPath() + "/myregister?page=1&search="+ search+"&sc="+sc+"&st=" + st);
            } else if (pageindex > totalpage) {
                    // if Page higher than Count => Page Count
                response.sendRedirect(request.getContextPath() + "/myregister?page=" + totalpage + "&search="+ search+"&sc="+sc+"&st=" + st);
            } else {
                ArrayList<SubjectRegister> subjectRegiter = registerDAO.getRegisterByUserId(u.getId(), st, sc, search, pagesize, pageindex);
                request.setAttribute("pageindex", pageindex);
                request.setAttribute("totalpage", totalpage);


                request.setAttribute("subjectRegiter", subjectRegiter);
                request.getRequestDispatcher("ViewsDA/myRegistrations.jsp").forward(request, response);
            }
            }else {
            response.sendRedirect(request.getContextPath() + "/home");

        }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
