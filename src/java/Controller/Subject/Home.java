/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Subject;

import DAO.BlogDAO;
import DAO.SlideDAO;
import DAO.SubjectDAO;
import Entity.Blog;
import Entity.Slide;
import Entity.Subject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ADMIN
 */
public class Home extends HttpServlet {

    // get random entity
    public void getRandomEntity(List totalList, List displayList, int n) {
        Random rdNum = new Random();
        int index;
        boolean checkExpertExist;
        while (true) {
            checkExpertExist = false;
            index = rdNum.nextInt(totalList.size());
            displayList.add(totalList.get(index));
            totalList.remove(totalList.get(index));
            if (displayList.size() == n) {
                break;
            }
        }
    }

    public ArrayList<Subject> getRecommenedSubjectTags() {
        /* Display random subject for recommened in homepage*/
        SubjectDAO dao = new SubjectDAO();
        ArrayList<Subject> sjtTotal = dao.getCompleteSubjects(""); // subject source for random
        // Loop for choose random subject (add from sjtTotal into sjtDisplayList)
        return sjtTotal;
    }

    public ArrayList<Subject> getLatestSubjectTags() {
        /* Display random subject for recommened in homepage*/
        SubjectDAO dao = new SubjectDAO();
        List<Subject> sjtTotal = dao.getCompleteSubjects(" order by s_id desc"); // subject source for random
        ArrayList<Subject> sjtDisplayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            sjtDisplayList.add(sjtTotal.get(i));
        }
        return sjtDisplayList;
    }

    // get blog tags in homepage
    public ArrayList<Blog> getBlogTags() {
        BlogDAO bdao = new BlogDAO();
        ArrayList<Blog> blogTotal = bdao.GetBlogs(1);
        return blogTotal;
    }

    // get slide in homepage
    public ArrayList<Slide> getSlides() {
        SlideDAO sdao = new SlideDAO();
        ArrayList<Slide> slides = sdao.getSlides(1);
        return slides;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

        } catch (NumberFormatException e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //slide
        ArrayList<Slide> slides = getSlides();
        request.setAttribute("slides", slides);

//        response.getWriter().print(""+ slides.get(0).getSl_image() );
        request.getRequestDispatcher("/home2").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
