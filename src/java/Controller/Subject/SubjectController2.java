/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Subject;

import DAO.SubjectDAO;
import Entity.Price;
import Entity.Subject;
import Entity.SubjectCat;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Phamb
 */
public class SubjectController2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectController2</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectController2 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO subjectDAO = new SubjectDAO();
        // get all cat and its subject 
        ArrayList<SubjectCat> cats = subjectDAO.getCategoriesAndItsSub("", "ORDER BY [num_of_sub] DESC");
        request.setAttribute("cats", cats);
        // get top subject most registration
        ArrayList<Subject> regisSubs = subjectDAO.getTopRegisSubject(3);
        for (Subject regisSub : regisSubs) {
            ArrayList<Price> p = subjectDAO.getPrice("WHERE [status] = 1 AND [s_id] = " + regisSub.getS_id());
            for (Price price : p) {
                regisSub.setPrice_id(price.getPrice_id());
                regisSub.setPrice(price.getPrice());
                if (price.getDiscount() == null) {
                    price.setDiscount("0");
                }
                regisSub.setDiscount(Integer.parseInt(price.getDiscount()));
                System.out.println("id: " + price.getPrice_id());
                System.out.println("price: " + price.getPrice());
                System.out.println("discount: " + price.getDiscount());
            }
        }
        request.setAttribute("regisSubs", regisSubs);
        request.getRequestDispatcher("/views/subject/subject2.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
